



   

    

    <?php foreach($data as $value){?>

    

    <form action="<?php echo base_url('broserviceadmin/testimoni/update/').$value["testimoni_id"]?>" method="post" enctype="multipart/form-data" >
        
        

        <div class="form-row">
            
            <div class="col-auto">
                <label >Tampilkan</label>
                <select class="form-control mb-2" name='tampilkan'>
                    <option value='true' <?php if($value['tampilkan']==true){echo 'selected';}?>>
                        Ya 
                    </option>
                    <option value='false' <?php if($value['tampilkan']==false){echo 'selected';}?>>
                        Tidak 
                    </option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="<?php echo base_url('broserviceadmin/testimoni/'); ?>" class="btn btn-default">Kembali</a>
        </div>
    </form>


    <?php if(isset($status)){?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-dismissible alert-<?php echo $status['status']?>">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><?php echo $status['message']?></strong>
                
            </div>
        </div>
    </div>
    <?php }?>


    <?php }?>
    
