



   

    

    <?php foreach($datasupersubkategori as $valuesupersubkategori){?>

    

    <form action="<?php echo base_url('broserviceadmin/supersubkategori/update/').$valuesupersubkategori["id"]?>" method="post" enctype="multipart/form-data" >
        
        <div class="form-group">
            <label for="title" class="control-label">Nama</label>
            <input type="text" class="form-control" name="nama" value='<?php echo $valuesupersubkategori['nama']?>'>

                <span class="help-block text-danger"><?php echo form_error('nama'); ?></span>
               
           
        </div>

        <div class="form-row">
            
            <div class="col-auto">
                <label >Kategori</label>
                <select class="form-control mb-2" name='kategori' id='kategori' onchange='getsubkategori()'>
                    
                </select>
            </div>
        </div>

        <div class="form-row">
            
            <div class="col-auto">
                <label >Sub Kategori</label>
                <select class="form-control mb-2" name='subkategori' id='subkategori'>
                    
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="col-auto">
                <label for="gambar">Gambar</label>
                <div class="input-group">
                <input type="file" class="form-control-file" id="gambar" name="gambar" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted">Foto Harus</small>
                    
                </div>
            </div>
        </div>

        <div class="form-row">
            
            <div class="col-auto">
                <label >Tampilkan</label>
                <select class="form-control mb-2" name='tampilkan'>
                    <option value='true' <?php if($valuesupersubkategori['tampilkan']==true){echo 'selected';}?>>
                        Ya 
                    </option>
                    <option value='false' <?php if($valuesupersubkategori['tampilkan']==false){echo 'selected';}?>>
                        Tidak 
                    </option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="<?php echo base_url('broserviceadmin/supersubkategori/'); ?>" class="btn btn-default">Kembali</a>
        </div>
    </form>

    

    <?php if(isset($status)){?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-dismissible alert-<?php echo $status['status']?>">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><?php echo $status['message']?></strong>
                
            </div>
        </div>
    </div>
    <?php }?>


    <?php }?>
    

    <script>

        
        getkategori();
        getsubkategori();
        function getkategori(){
            //script for select kategori   
            $( document ).ready(function() {
                
                $.ajax({
                    url		: '<?php echo base_url('broserviceadmin/supersubkategori/getkategori/'); ?>',
                    type	: 'get',
                    processData: false,
                    contentType: false,
                    dataType: 'html',
                    beforeSend : function(){
                            
                    },
                    success : function(data){
                            
                        var json = $.parseJSON(data);
                
                        
                        for (x in json.data) {

                            var select = document.getElementById("kategori");
                            var option = document.createElement("option");
                            option.text = json.data[x].nama; ;
                            option.value = json.data[x].id; ;

                            select.add(option);
                            
                        }
                        
                        $("#kategori").val(<?php echo $valuesupersubkategori['kategori_id']?>);
                    }
                });

            
            });

        }

        function getsubkategori(){

            //script for select kategori   
            $( document ).ready(function() {
                
                $.ajax({
                    url		: '<?php echo base_url('broserviceadmin/supersubkategori/getsubkategori/'); ?>',
                    type	: 'get',
                    processData: false,
                    contentType: false,
                    dataType: 'html',
                    beforeSend : function(){
                            
                    },
                    success : function(data){
                            
                        var json = $.parseJSON(data);
                
                        

                        var kategori=$('#kategori').val()
                        $('#subkategori').find('option').remove();
                        for (x in json.data) {

                            if(json.data[x].kategori_id==kategori){

                                

                                var select = document.getElementById("subkategori");
                                var option = document.createElement("option");
                                option.text = json.data[x].nama; ;
                                option.value = json.data[x].id; ;

                                select.add(option);

                            }
                            
                        }
                        
                        $("#subkategori").val(<?php echo $valuesupersubkategori['subkategori_id']?>);
                       
            
                    }
                });

            
            });

        }
    </script>

    