



   

    
    <form action="<?php echo base_url('broserviceadmin/supersubkategori/store'); ?>" method="post" enctype="multipart/form-data" >
        
        <div class="form-group">
            <label for="title" class="control-label">Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="Nama Kategori">

                <span class="help-block text-danger"><?php echo form_error('nama'); ?></span>
               
           
        </div>

        <div class="form-row">
            
            <div class="col-md-4">
                <label >Kategori</label>
                <select class="form-control mb-2" name='kategori' id='kategori' onchange='getsubkategori()'>
                    
                </select>
            </div>
        </div>

        <div class="form-row">
            
            <div class="col-md-4">
                <label >Sub Kategori</label>
                <select class="form-control mb-2" name='subkategori' id='subkategori' onchange='getsubkategori()'>
                    
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="col-auto">
                <label for="gambar">Gambar</label>
                <div class="input-group">
                <input type="file" class="form-control-file" id="gambar" name="gambar" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted">Foto Harus</small>
                    
                </div>
            </div>
        </div>

        <div class="form-row">
            
            <div class="col-auto">
                <label >Tampilkan</label>
                <select class="form-control mb-2" name='tampilkan'>
                    <option value='true'>
                        Ya 
                    </option>
                    <option value='false'>
                        Tidak 
                    </option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="<?php echo base_url('broserviceadmin/supersubkategori/'); ?>" class="btn btn-default">Kembali</a>
        </div>
    </form>


    <?php if(isset($status)){?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-dismissible alert-<?php echo $status['status']?>">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><?php echo $status['message']?></strong>
                
            </div>
        </div>
    </div>
    <?php }?>


    <script>

        
        getkategori();
        getsubkategori();
        function getkategori(){
             //script for select kategori   
            $( document ).ready(function() {
                
                $.ajax({
                    url		: '<?php echo base_url('broserviceadmin/supersubkategori/getkategori/'); ?>',
                    type	: 'get',
                    processData: false,
                    contentType: false,
                    dataType: 'html',
                    beforeSend : function(){
                            
                    },
                    success : function(data){
                            
                        var json = $.parseJSON(data);
                
                        
                        for (x in json.data) {

                            var select = document.getElementById("kategori");
                            var option = document.createElement("option");
                            option.text = json.data[x].nama; ;
                            option.value = json.data[x].id; ;

                            select.add(option);
                            
                        }
            
                    }
                });

            
            });

        }
        
        function getsubkategori(){

            //script for select kategori   
            $( document ).ready(function() {
                
                $.ajax({
                    url		: '<?php echo base_url('broserviceadmin/supersubkategori/getsubkategori/'); ?>',
                    type	: 'get',
                    processData: false,
                    contentType: false,
                    dataType: 'html',
                    beforeSend : function(){
                            
                    },
                    success : function(data){
                            
                        var json = $.parseJSON(data);
                
                        

                        var kategori=$('#kategori').val()
                        $('#subkategori').find('option').remove();
                        for (x in json.data) {

                            if(json.data[x].kategori_id==kategori){

                                

                                var select = document.getElementById("subkategori");
                                var option = document.createElement("option");
                                option.text = json.data[x].nama; ;
                                option.value = json.data[x].id; ;

                                select.add(option);

                            }
                            
                        }
            
                    }
                });

            
            });

        }
    </script>
    
