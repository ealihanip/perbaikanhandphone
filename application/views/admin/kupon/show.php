<div class='row'>

    <div class='col-lg-12'>

        <div class='card bg-light'>

            <div class='card-header'>
                <p class='h5'>Data Kupon</p>
            </div>
            <div class='card-body'>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Nama:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $data['nama']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Minimal Transaksi:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $data['min_transaksi']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-12'>
                        <p>Deskripsi:</p>
                    </div>
                    <div class='col-lg-12'>
                        <p><?php echo $data['deskripsi']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-12'>
                        <p>Gambar:</p>
                    </div>
                    <div class='col-lg-12'>

                        <img src="<?php echo base_url('assets/uploads/kupon/'.$data['gambar']) ?>" width='400px' height='400px'>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
