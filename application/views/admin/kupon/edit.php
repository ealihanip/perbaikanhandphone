



   

    

    <?php foreach($data as $value){?>

    

    <form action="<?php echo base_url('broserviceadmin/kupon/update/').$value["id"]?>" method="post" enctype="multipart/form-data" >
        
        <div class="form-group">
            <label for="title" class="control-label">Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="Nama Kategori" value='<?php echo $value['nama']?>'>

                <span class="help-block text-danger"><?php echo form_error('nama'); ?></span>
        </div>


        <div class="form-group">
            <label for="mintransaksi" class="control-label">Minimal Transaksi</label>
            <input type="text" class="form-control" name="mintransaksi" placeholder="Minimal Transaksi" value='<?php echo $value['min_transaksi']?>'>

                <span class="help-block text-danger"><?php echo form_error('mintransaksi'); ?></span>
        </div>


        <div class="form-group">
            <label for="deskripsi">Deskripsi</label>
            <textarea class="form-control" rows="5" id="deskripsi" name='deskripsi'><?php echo $value['deskripsi']?></textarea>
        </div>


        <div class="form-row">
            <div class="col-auto">
                <label for="gambar">Gambar</label>
                <div class="input-group">
                <input type="file" class="form-control-file" id="gambar" name="gambar" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted">Foto Harus</small>
                    
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="<?php echo base_url('broserviceadmin/kupon/'); ?>" class="btn btn-default">Kembali</a>
        </div>
    </form>


    <?php if(isset($status)){?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-dismissible alert-<?php echo $status['status']?>">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><?php echo $status['message']?></strong>
                
            </div>
        </div>
    </div>
    <?php }?>


    <?php }?>
    
