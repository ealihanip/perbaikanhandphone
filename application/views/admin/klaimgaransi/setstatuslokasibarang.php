
<form action="<?php echo base_url('broserviceadmin/klaim-garansi/show/update-status-lokasi-barang/'.$kode); ?>" method="post" enctype="multipart/form-data" >

<?php if($jenis_layanan==false){?> 

    <div class="form-row">
        
        <div class="col-auto">
            <label >Status Lokasi Barang</label>
            <select class="form-control mb-2" name='status' id='status'>
                <?php foreach($status_lokasi as $value){?>

                    <option value='<?php echo $value['id']?>' <?php if($value['id']==$antarjemput['status']){echo 'selected';}?>>
                        <?php echo $value['nama']?>
                    </option>

                <?php }?>
            </select>
        </div>

    </div>

<?php }?>       


<?php if($jenis_layanan==true){?>       
    <div class="form-row">
        
        <div class="col-auto">
            <label >Status Lokasi Barang</label>
            <select class="form-control mb-2" name='status' id='status' onchange='showinputresi()'>
                <?php foreach($status_lokasi as $value){?>

                    <option value='<?php echo $value['id']?>' <?php if($value['id']==$kirimonline['status']){echo 'selected';}?>>
                        <?php echo $value['nama']?>
                    </option>

                <?php }?>
            </select>
        </div>

    </div>

    
    <div id='inputresi' class="d-none">
        <div class="form-group">
            <label for="kurir">Kurir</label>
            <select class="form-control" id="kurir" name='kurir'>
                <option>JNE</option>
                <option>TIKI</option>
                <option>JNT</option>
            </select>
        </div>

        <div class="form-group">
            <label for="resi">No Resi</label>
            <input type="text" class="form-control" id="resi" placeholder="No Resi" name='resi'>
        </div>

    </div>
    <?php }?>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Simpan</button>
        <a href="<?php echo base_url('broserviceadmin/klaim-garansi/show/'.$kode); ?>" class="btn btn-default">Kembali</a>
    </div>
</form>


    <?php if(isset($status)){?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-dismissible alert-<?php echo $status['status']?>">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><?php echo $status['message']?></strong>
                
            </div>
        </div>
    </div>
    <?php }?>





<script>

    

    function showinputresi(){

        $(document).ready(function() {
            
         
            if($('#status').val()==3){

                $( "#inputresi" ).removeClass("d-none");

            }
            

            
        });   


    }

    

      

</script>

    