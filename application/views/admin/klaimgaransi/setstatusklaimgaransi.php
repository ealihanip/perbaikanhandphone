
<form action="<?php echo base_url('broserviceadmin/klaim-garansi/update-status-klaim-garansi/'.$data['kode']); ?>" method="post" enctype="multipart/form-data" >
        
        
        <div class="form-row">
            
            <div class="col-auto">
                <label >Status Klaim Garansi</label>
                <select class="form-control mb-2" name='status'>
                    <option value='false' <?php if($data['status_klaim_garansi']==false){echo 'selected';}?>>
                        Belum Diterima 
                    </option>
                    <option value='true'<?php if($data['status_klaim_garansi']==true){echo 'selected';}?>>
                        Diterima 
                    </option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="<?php echo base_url('broserviceadmin/klaim-garansi/'); ?>" class="btn btn-default">Kembali</a>
        </div>
    </form>


    <?php if(isset($status)){?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-dismissible alert-<?php echo $status['status']?>">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><?php echo $status['message']?></strong>
                
            </div>
        </div>
    </div>
    <?php }?>

    