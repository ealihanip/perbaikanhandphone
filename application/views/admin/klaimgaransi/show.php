<div class='row'>

    <div class='col-lg-12'>

        <div class='card bg-light'>

            <div class='card-header'>
                <p class='h5'>Data Costumer</p>
            </div>
            <div class='card-body'>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Nama:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $user['first_name']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>No Handphone:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $user['phone']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Alamat:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $user['address']?></p>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class='card bg-light'>

            <div class='card-header'>
                <p class='h5'>Data Klaim Garansi</p>
            </div>
            <div class='card-body'>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Kode Klaim Garansi:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $klaimgaransi['kode_klaim_garansi']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Tanggal Booking:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo date('d-m-Y',strtotime($klaimgaransi['tanggal_klaim_garansi']))?></p>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Status Perbaikan:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $klaimgaransi['nama_status_perbaikan']?></p>
                    </div>
                </div>

                <?php if($klaimgaransi['status_perbaikan']==true){?>
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Tanggal Selesai Perbaikan:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo date('d-m-Y',strtotime($klaimgaransi['tanggal_selesai_perbaikan']))?></p>
                    </div>
                </div>
                <?php }?>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Status Booking:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $klaimgaransi['nama_status_klaim_garansi']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Jenis Layanan:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $klaimgaransi['nama_jenis_layanan']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Kode Perbaikan:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $klaimgaransi['kode_perbaikan']?></p>
                        <a class='btn btn-dark btn-sm' href='<?php echo base_url('broserviceadmin/perbaikan/show/'.$klaimgaransi['kode_perbaikan'])?>'><div class='text-white'>Detail Perbaikan</div></a>
                    </div>
                </div>
               
                
            </div>
        </div>
        
        <hr>
        <?php if($klaimgaransi['jenis_layanan']==false){?>
        <div class='card bg-light'>

            <div class='card-header'>
                <p class='h5'>Data Antar Jemput</p>
            </div>
            <div class='card-body'>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Tanggal:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo date('d-m-Y',strtotime($antarjemput['tanggal']))?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Jam:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $antarjemput['jam']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Alamat:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $antarjemput['alamat']?></p>
                    </div>

                    <a class='btn btn-dark btn-sm' href='<?php echo base_url('broserviceadmin/klaim-garansi/show/set-status-lokasi-barang/'.$klaimgaransi['kode'])?>'><div class='text-white'>Ubah Status Lokasi Barang</div></a>
                </div>
                
            </div>
        </div>
        <?php }?>

        <?php if($klaimgaransi['jenis_layanan']==true){?>
        <div class='card bg-light'>

            <div class='card-header'>
                <p class='h5'>Data Kirim Online</p>
            </div>
            <div class='card-body'>

                <div class='row'>

                    <div class='col-lg-12'>
                        <p class='h5'>Pengiriman Dari Konsumen:</p>
                        <hr>
                    </div>
                    
                </div>

                <div class='row'>

                    <div class='col-lg-2'>
                        <p>Kurir:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $kirimonline['kurir']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Resi:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $kirimonline['resi']?></p>
                        
                    </div>
                    
                </div>

                <?php if($kirimonline['status']==3){?>
                    
                    <div class='row'>

                    <div class='col-lg-12'>
                        <p class='h5'>Pengiriman Dari Broservice:</p>
                        <hr>
                    </div>
                    
                </div>

                <div class='row'>

                    <div class='col-lg-2'>
                        <p>Kurir:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $kirimonline['kurirkembali']?></p>
                    </div>
                </div>
                
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Resi:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $kirimonline['resikembali']?></p>
                    </div>
                </div>

                <?php }?>


                <div class='row'>
                    
                    <div class='col-lg-12'>
                        <a class='btn btn-dark btn-sm' href='<?php echo base_url('broserviceadmin/klaim-garansi/show/set-status-lokasi-barang/'.$klaimgaransi['kode'])?>'><div class='text-white'>Ubah Status Lokasi Barang</div></a>
                    </div>

                </div>

                
                
            </div>
        </div>
                            
        
        <?php }?>
        
    </div>

</div>