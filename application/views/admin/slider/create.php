



   

    
    <form action="<?php echo base_url('broserviceadmin/slider/store'); ?>" method="post" enctype="multipart/form-data" >
        
        <div class="form-row">
            <div class="col-auto">
                <label for="gambar">Gambar</label>
                <div class="input-group">
                <input type="file" class="form-control-file" id="gambar" name="gambar" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted">Foto Harus</small>
                    
                </div>
            </div>
        </div>

        

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="<?php echo base_url('broserviceadmin/slider/'); ?>" class="btn btn-default">Kembali</a>
        </div>
    </form>


    <?php if(isset($status)){?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-dismissible alert-<?php echo $status['status']?>">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><?php echo $status['message']?></strong>
                
            </div>
        </div>
    </div>
    <?php }?>
    
