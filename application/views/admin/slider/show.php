

@extends('layouts.default')

@section('title', 'Guru')

@section('content')
    <div class='row'>
        <div class='col'>

            <h4>{{ $guru->id_guru }}</h4>
            <p>{{ $guru->nama_guru }}</p>
            <a href="{{ route('guru.index') }}" class="btn btn-default">Kembali</a>
        </div>
        
    </div>
    

    

@endsection