    <div class='row'>
        <div class='col'>
            <table class="table table-bordered" id="tabel">
                <thead>
                    <tr>
                        
                        <th>id</th>
                        <th>Gambar</th>
                        <th>Aksi</th>
                        
                    </tr>
                </thead>
            </table>

  
        </div>
        
    </div>



<script>
    $(document).ready( function () {
        $('#tabel').DataTable({
            bLengthChange: false,
            info: false,
            pageLength: 10,
            oLanguage: {
                sSearch: "cari"
            },						
            processing: true, //Feature control the processing indicator.
            serverSide: true, //Feature control DataTables' server-side processing mode.
            order: [], //Initial no order.
            // Load data for the table's content from an Ajax source
            ajax: {
                url: '<?php echo base_url('broserviceadmin/slider/getdata/'); ?>',
                type: "post"
            },
            columns: [
                {data:'id'},
                {data:'gambar_slider'},
                {data:'action', orderable: false, searchable: false}
            ]
        });
    });

</script>



