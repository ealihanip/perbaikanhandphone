



<?php foreach($data as $value){?>

    
    <form action="<?php echo base_url('broserviceadmin/pengaturan/update'); ?>" method="post" enctype="multipart/form-data" >
        
        <?php if($value['id']==1){?>
        <div class="form-group">
            <label for="durasiakhirgarasi" class="control-label">Durasi Akhir Garansi</label>
            <input type="text" class="form-control" name="durasiakhirgarasi" value='<?php echo $value['nilai']?>'>

                <span class="help-block text-danger"><?php echo form_error('durasiakhirgarasi'); ?></span>
        </div>
        <?php }?>
       

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            
        </div>
    </form>


    <?php if(isset($status)){?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-dismissible alert-<?php echo $status['status']?>">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><?php echo $status['message']?></strong>
                
            </div>
        </div>
    </div>
    <?php }?>

<?php }?>
    
