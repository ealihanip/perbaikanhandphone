
<html>
<head>

    <title>BROSERVICE</title>
    

    <style>

        table,td{

            font-size:20px;

        };

    </style>
</head>


<body>

<small><?php echo date('d-m-Y')?></small>


<h1 align='center'>BROSERVICE.COM</h1>

<hr>
<table>
    <tr>
        <td>
            Kode Perbaikan
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $perbaikan['kode']?>
        </td>
    </tr>
    <tr>
        <td>
            Status Perbaikan
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $perbaikan['nama_status_perbaikan']?>
        </td>
    </tr>
    <tr>
        <td>
            Tanggal selesai Perbaikan
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo date('d-m-Y',strtotime($perbaikan['tanggal_selesai_perbaikan']))?>
        </td>
    </tr>
    <tr>
        <td>
            Akhir Waktu Klaim Garansi
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo date('d-m-Y',strtotime($akhir_garansi))?>
        </td>
    </tr>
    </table>
    <hr>
    <hr>
    <table>
    <tr>
        <td>
            Nama Costumer
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $user['first_name']?>
        </td>
    </tr>

    <tr>
        <td>
            No Handphone
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $user['phone']?>
        </td>
    </tr>  
        
    <tr>
        <td>
            Jenis
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $perbaikan['nama_jenis']?>
        </td>
    </tr>

    <tr>
        <td>
            Merk
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $perbaikan['nama_brand']?>
        </td>
    </tr>

    <tr>
        <td>
            Model
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $perbaikan['nama_model']?>
        </td>
    </tr>


</table>
<hr>

<p>Daftar Kerusakan :</p>

<table>
    <?php foreach($daftarkerusakan as $value){?>
    <tr>
        <td style='width:200px;'>
            <?php echo $value['nama_kerusakan']?>  
        </td>
        <td style='width:100px;'>
            <?php echo $value['harga']-(($value['harga']* $value['diskon'])/100)?> 
        </td>
        
        <?php if($value['diskon']!=false){?>
        <td>
            (diskon <?php echo $value['diskon']?>% dari <?php echo $value['harga']?> ) 
        </td>

        <?php }?>

    </tr>

    <?php }?>

    <tr>
        <td>
            
        </td>
        <td>
            <hr> 
        </td>

    </tr>

    <tr>
        <td>
            
        </td>
        <td>
            <?php echo preg_replace("/,/", '', $perbaikan['total_harga'])?> 
        </td>

    </tr>
    

</table>


<hr>

</body>


</html>