    
    
    <div class='row'>
        <div class='col'>
            <!-- <div class='card'>

                <div class='card-header'>
                    <p>Filter Data</p>
                </div>

                <div class='card-body'>
                    <form action="<?php echo base_url('broserviceadmin/perbaikan/getdata/'); ?>" method="post">
                        <div class="form-group">
                            <label for="statusbooking">Status Booking</label>
                            <select class="form-control" id="statusbooking" name='statusbooking'>
                            <option value=''>--</option>
                            <option value='0'>Belum Diterima</option>
                            <option value='1'>Sudah Diterima</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="statuspembayaran">Status Pembayaran</label>
                            <select class="form-control" id="statuspembayaran" name='statuspembayaran'>
                            <option value=''>--</option>
                            <option value='0'>Belum Dibayar</option>
                            <option value='1'>Sudah Dibayar</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="statusperbaikan">Status Perbaikan</label>
                            <select class="form-control" id="statusperbaikan" name='statusperbaikan'>
                            <option value=''>--</option>
                            <option value='0'>Belum Selesai</option>
                            <option value='1'>Sudah Selesai</option>
                            </select>
                        </div>
                        
                        

                        <div class="form-group">
                            <input type='submit' class='btn btn-primary btn-sm' value='Filter'>
                        </div>
                    </form>

                    
                    
                </div>


                
            </div> -->

  
        </div>
        
    </div>
    
    
    <div class='row'>
        <div class='col'>
            <table class="table display nowrap" id="tabel">
                <thead>
                    <tr>
                    <th><center>Action</center></th>
                        <th>Kode</th>
                        <th>Jenis Layanan</th>
                        <th>Tanggal Booking</th>
                        <th>Status Booking</th>
                        <th>Status Perbaikan</th>
                        <th>Status Pembayaran</th>
                        <th>Nama Konsumen</th>
                        <th>No Handphone</th>
                        <th>Total Harga</th>
                        
                        
                    </tr>
                </thead>
            </table>

  
        </div>
        
    </div>



<script>

    $(document).ready( function () {

        
        var table =$('#tabel').DataTable({
            
        
            dom: 'Bfrtip',
            buttons: [
                'excel', 'print',
               
            ],
            
            responsive: true,
            scrollX: true,
            bLengthChange: false,
            info: false,
            pageLength: 10,
            oLanguage: {
                sSearch: "Cari"
            },						
            processing: true, //Feature control the processing indicator.
            serverSide: true, //Feature control DataTables' server-side processing mode.
            order: [], //Initial no order.
            // Load data for the table's content from an Ajax source
            ajax: {
                url: '<?php echo base_url('broserviceadmin/perbaikan/getdata/'); ?>',
                type: "post"
            },
            columns: [
                {data:'action', orderable: false, searchable: false},
                {data:'kode' , orderable: true, searchable: true},
                {data:'nama_jenis_layanan', orderable: false, searchable: true},
                {data:'tanggal_booking', orderable: true, searchable: true},
                {data:'nama_status_booking', orderable: true, searchable: true},
                {data:'nama_status_perbaikan', orderable: true, searchable: true},
                {data:'nama_status_pembayaran', orderable: true, searchable: true},
                {data:'first_name', orderable: false, searchable: true},
                {data:'phone' ,orderable: false, searchable: false},
                {data:'total_harga', orderable: false, searchable: false},
                
            ]
        });

        

        
    });


    
    

    
</script>



