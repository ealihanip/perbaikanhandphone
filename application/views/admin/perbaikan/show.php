<div class='row'>

    <div class='col-lg-12'>

        <div class='card bg-light'>

            <div class='card-header'>
                <p class='h5'>Data Costumer</p>
            </div>
            <div class='card-body'>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Nama:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $user['first_name']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>No Handphone:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $user['phone']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Alamat:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $user['address']?></p>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class='card bg-light'>

            <div class='card-header'>
                <p class='h5'>Data Perbaikan</p>
            </div>
            <div class='card-body'>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Kode:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $perbaikan['kode']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Tanggal Booking:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo date('d-m-Y',strtotime($perbaikan['tanggal_booking']))?></p>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Status Perbaikan:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $perbaikan['nama_status_perbaikan']?></p>
                    </div>
                </div>

                <?php if($perbaikan['status_perbaikan']==true){?>
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Tanggal Selesai Perbaikan:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo date('d-m-Y',strtotime($perbaikan['tanggal_selesai_perbaikan']))?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Tanggal Akhir Garansi:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo date('d-m-Y',strtotime($akhir_garansi))?></p>
                    </div>
                </div>

                <?php }?>
                

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Status Booking:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $perbaikan['nama_status_booking']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Jenis Layanan:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $perbaikan['nama_jenis_layanan']?></p>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Status Pembayaran:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $perbaikan['nama_status_pembayaran']?></p>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Total Harga:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $perbaikan['total_harga']?></p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class='card bg-light'>

            <div class='card-header'>
                <p class='h5'>Data Kerusakan</p>
            </div>
            <div class='card-body'>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Jenis:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $perbaikan['nama_jenis']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Brand:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $perbaikan['nama_brand']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Model:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $perbaikan['nama_model']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-12'>
                        <p>Kerusakan</p>
                    </div>
                    <div class='col-lg-12'>

                        <?php $no=1; foreach($daftarkerusakan as $value){?>
                            <p><?php echo $no?>. <?php echo $value['nama_kerusakan']?></p>
                        <?php $no++; }?>
                    </div>
                </div>
                
            </div>
        </div>
        <hr>
        <?php if($perbaikan['jenis_layanan']==false){?>
        <div class='card bg-light'>

            <div class='card-header'>
                <p class='h5'>Data Antar Jemput</p>
            </div>
            <div class='card-body'>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Tanggal:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo date('d-m-Y',strtotime($antarjemput['tanggal']))?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Jam:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $antarjemput['jam']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Alamat:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $antarjemput['alamat']?></p>
                    </div>
                </div>

                <div class='row'>
                    
                    <div class='col-lg-12'>
                        <a class='btn btn-dark btn-sm' href='<?php echo base_url('broserviceadmin/perbaikan/show/set-status-lokasi-barang/'.$perbaikan['kode'])?>'><div class='text-white'>Ubah Status Lokasi Barang</div></a>
                    </div>

                </div>
                
            </div>
        </div>
        <?php }?>

        <?php if($perbaikan['jenis_layanan']==true){?>
        <div class='card bg-light'>

            <div class='card-header'>
                <p class='h5'>Data Kirim Online</p>
            </div>
            <div class='card-body'>

                <div class='row'>

                    <div class='col-lg-12'>
                        <p class='h5'>Pengiriman Dari Konsumen:</p>
                        <hr>
                    </div>
                    
                </div>

                <div class='row'>

                    <div class='col-lg-2'>
                        <p>Kurir:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $kirimonline['kurir']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Resi:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $kirimonline['resi']?></p>
                        
                    </div>
                    
                </div>

                <?php if($kirimonline['status']==3){?>
                    
                    <div class='row'>

                    <div class='col-lg-12'>
                        <p class='h5'>Pengiriman Dari Broservice:</p>
                        <hr>
                    </div>
                    
                </div>

                <div class='row'>

                    <div class='col-lg-2'>
                        <p>Kurir:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $kirimonline['kurirkembali']?></p>
                    </div>
                </div>
                
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Resi:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $kirimonline['resikembali']?></p>
                    </div>
                </div>

                <?php }?>


                <div class='row'>
                    
                    <div class='col-lg-12'>
                        <a class='btn btn-dark btn-sm' href='<?php echo base_url('broserviceadmin/perbaikan/show/set-status-lokasi-barang/'.$perbaikan['kode'])?>'><div class='text-white'>Ubah Status Lokasi Barang</div></a>
                    </div>

                </div>

                
                
            </div>
        </div>
                            
        <div class='card bg-light'>

            <div class='card-header'>
                <p class='h5'>Data Komfirmasi Pembayaran</p>
            </div>
            <div class='card-body'>
                <?php foreach($komfirmasipembayaran as $value){?>            
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Bank:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $value['bank']?></p>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>No Rekening:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $value['norek']?></p>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Nama:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $value['nama']?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Tanggal Transfer:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo date('d-m-Y',strtotime($value['tanggal']))?></p>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Bukti Transfer</p>
                    </div>
                    <div class='col-lg-10'>
                        <img src="<?php echo base_url('assets/uploads/komfirmasipembayaran/'.$value['foto'])?>">
                    </div>
                </div>

                
                <?php }?> 
                
                
            </div>
        </div>


        <div class='card bg-light'>

            <div class='card-header'>
                <p class='h5'>Kupon</p>
            </div>
            <div class='card-body'>
                <?php foreach($kupon as $value){?>

                <?php if($value['min_transaksi']<=preg_replace('/\D/', '', $perbaikan['total_harga'])){?>

                    <div class='row'>
                    <div class='col-lg-2'>
                        <p>Status Kupon:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p class='text-primary'>Memenuhi Syarat Total Transaksi</p>
                    </div>

                    </div>

                     

                <?php }else{?>


                    <div class='row'>
                        <div class='col-lg-2'>
                            <p>Status Kupon:</p>
                        </div>
                        <div class='col-lg-10'>
                            <p class='text-danger'>Tidak Memenuhi Syarat Total Transaksi</p>
                        </div>

                        
                    </div>

                <?php }?>

                
                                        
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Nama:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $value['nama']?></p>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-lg-2'>
                        <p>Deskripsi:</p>
                    </div>
                    <div class='col-lg-10'>
                        <p><?php echo $value['deskripsi']?></p>
                    </div>
                </div>

                
                <?php }?> 
                
                
            </div>
        </div>

        <?php }?>
        
    </div>

</div>