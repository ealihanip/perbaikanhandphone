<div class='row'>


    <div class='col-md-12'>

        <div class='row'>

            <div class='col-lg-2'>
                <p class='h5'>Kode Perbaikan</p> 
            </div>
            <div class='col-lg-10'>
                <p class='h5'><?php echo $data->kode?></p>
                
            </div>

        </div>

        <div class='row'>

            <div class='col-lg-2'>
                <p class='h5'>Nama Costumer</p> 
            </div>
            <div class='col-lg-10'>
                <p class='h5'><?php echo $data->first_name?> <?php echo $data->last_name?></p>
                
            </div>

        </div>

        <div class='row'>

            <div class='col-lg-2'>
                <p class='h5'>No Handphone</p> 
            </div>
            <div class='col-lg-10'>
                <p class='h5'><?php echo $data->phone?></p>
                
            </div>

        </div>

        <div class='row'>

            <div class='col-lg-2'>
                <p class='h5'>Alamat</p> 
            </div>
            <div class='col-lg-10'>
                <p class='h5'><?php echo $data->address?></p>
                
            </div>

        </div>
        
    </div>

    <div class='col-lg-12'>
        <hr>
    </div>
    

    <div class='col-lg-12'>
        <p class='h5'>Detail Perbaikan</p>
    </div>

    <div class='col-lg-12'>
        <div class='row'>

            <div class='col-lg-2'>
                <p class='h5'>Jenis</p> 
            </div>
            <div class='col-lg-10'>
                <p class='h5'><?php echo $data->nama_jenis?></p>
                
            </div>

        </div>
        <div class='row'>

            <div class='col-lg-2'>
                <p class='h5'>Brand</p> 
            </div>
            <div class='col-lg-10'>
                <p class='h5'><?php echo $data->nama_brand?></p>
                
            </div>

        </div>
        <div class='row'>

            <div class='col-lg-2'>
                <p class='h5'>Model</p> 
            </div>
            <div class='col-lg-10'>
                <p class='h5'><?php echo $data->nama_model?></p>
                
            </div>

        </div>
        <div class='row'>

            <div class='col-lg-2'>
                <p class='h5'>Kerusakan</p> 
            </div>
            <div class='col-lg-10'>
                <p class='h5'><?php echo $data->nama_kerusakan?></p>
                
            </div>

        </div>
    </div>

    <div class='col-lg-12'>
        <hr>
    </div>

    <div class='col-lg-12'>
        <p class='h5'>Detail Antar Jemput</p>
    </div>

    <div class='col-lg-12'>
        <div class='row'>

            <div class='col-lg-2'>
                <p class='h5'>Tanggal</p> 
            </div>
            <div class='col-lg-10'>
                <p class='h5'><?php echo $data->tanggal?></p>
                
            </div>

        </div>
        <div class='row'>

            <div class='col-lg-2'>
                <p class='h5'>Jam</p> 
            </div>
            <div class='col-lg-10'>
                <p class='h5'><?php echo $data->jam?></p>
                
            </div>

        </div>
        <div class='row'>

            <div class='col-lg-2'>
                <p class='h5'>Alamat Penjemputan</p> 
            </div>
            <div class='col-lg-10'>
                <p class='h5'><?php echo $data->alamat?></p>
                
            </div>
           

        </div>
        
    </div>

    


</div>