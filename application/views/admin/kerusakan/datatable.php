    <div class='row'>
        <div class='col'>
            <table class="table table-bordered" id="tabel">
                <thead>
                    <tr>
                        <th>Kerusakan</th>
                        <th>Super Sub Kategori</th>
                        <th>Sub Kategori</th>
                        <th>Kategori</th>
                        <th>Harga</th>
                        <th>Diskon (%)</th>
                        <th>Action</th>
                        
                    </tr>
                </thead>
            </table>

  
        </div>
        
    </div>



<script>
    $(document).ready( function () {
        $('#tabel').DataTable({
            bLengthChange: false,
            info: false,
            pageLength: 10,
            oLanguage: {
                sSearch: "cari"
            },						
            processing: true, //Feature control the processing indicator.
            serverSide: true, //Feature control DataTables' server-side processing mode.
            order: [], //Initial no order.
            // Load data for the table's content from an Ajax source
            ajax: {
                url: '<?php echo base_url('broserviceadmin/kerusakan/getdata/'); ?>',
                type: "post"
            },
            columns: [
                {data:'nama_kerusakan'},
                {data:'nama_supersubkategori'},
                {data:'nama_subkategori'},
                {data:'nama_kategori'},
                {data:'harga'},
                {data:'diskon'},
                {data:'action', orderable: false, searchable: false}
            ]
        });
    });

</script>



