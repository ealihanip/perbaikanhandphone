



   

    
    <form action="<?php echo base_url('broserviceadmin/kerusakan/store'); ?>" method="post" enctype="multipart/form-data" >
        
        <div class="form-group">
            <label for="title" class="control-label">Nama Kerusakan</label>
            <input type="text" class="form-control" name="nama" placeholder="Nama Kerusakan">

                <span class="help-block text-danger"><?php echo form_error('nama'); ?></span>
               
           
        </div>

        <div class="form-row">
            
            <div class="col-md-4">
                <label >Kategori</label>
                <select class="form-control mb-2" name='kategori' id='kategori' onchange='getsubkategori();getsupersubkategori()'>
                <option>Pilih</option>
                </select>
            </div>
        </div>

        <div class="form-row">
            
            <div class="col-md-4">
                <label >Sub Kategori</label>
                <select class="form-control mb-2" name='subkategori' id='subkategori' onchange='getsupersubkategori()'>
                <option>Pilih</option>
                </select>
            </div>
        </div>

        <div class="form-row">
            
            <div class="col-md-4">
                <label >Super Sub Kategori</label>
                <select class="form-control mb-2" name='supersubkategori' id='supersubkategori'>
                    
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="title" class="control-label">Harga</label>
            <input type="text" class="form-control" name="harga" placeholder="harga">

                <span class="help-block text-danger"><?php echo form_error('harga'); ?></span>
        </div>

        <div class="form-group">
            <label for="title" class="control-label">Deskripsi</label>
            <textarea class="form-control" rows="5" id="deskripsi" name='deskripsi'></textarea>

                <span class="help-block text-danger"><?php echo form_error('deskripsi'); ?></span>
        </div>

        <div class="form-group">
            <label for="diskon" class="control-label">Diskon</label>
            <div class="row">
                <div class="col-lg-4">

                    <div class='input-group'>
                        
                        <input type="text" class="form-control" name="diskon" placeholder="diskon">
                        <div class="input-group-append">
                            <span class="input-group-text">%</span>
                        </div>
                        
                    </div>

                    <small>Default 0%</small>
                    <span class="help-block text-danger"><?php echo form_error('diskon'); ?></span>
                
                </div>
            </div>
            
                
        </div>

        <div class="form-row">
            <div class="col-auto">
                <label for="gambar">Gambar</label>
                <div class="input-group">
                <input type="file" class="form-control-file" id="gambar" name="gambar" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted">Foto Harus</small>
                    
                </div>
            </div>
        </div>

        

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="<?php echo base_url('broserviceadmin/kerusakan/'); ?>" class="btn btn-default">Kembali</a>
        </div>
    </form>


    <?php if(isset($status)){?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-dismissible alert-<?php echo $status['status']?>">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><?php echo $status['message']?></strong>
                
            </div>
        </div>
    </div>
    <?php }?>


    <script>

        
        getkategori();
        getsubkategori();
        getsupersubkategori();
        
        
        function getkategori(){
             //script for select kategori   
           
                
                $.ajax({
                    url		: '<?php echo base_url('broserviceadmin/kerusakan/getkategori/'); ?>',
                    type	: 'post',
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: 'html',
                    beforeSend : function(){
                            
                    },
                    success : function(data){
                            
                        var json = $.parseJSON(data);
                
                        
                        for (x in json.data) {

                            var select = document.getElementById("kategori");
                            var option = document.createElement("option");
                            option.text = json.data[x].nama; 
                            option.value = json.data[x].id; 

                            select.add(option);
                            
                        }
            
                    }
                });

            
           

        }
        
        function getsubkategori(){

                $('#subkategori').find('option').remove();
                
                $.ajax({
                    url		: '<?php echo base_url('broserviceadmin/kerusakan/getsubkategori/'); ?>',
                    type	: 'post',
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: 'html',
                    beforeSend : function(){
                            
                    },
                    success : function(data){
                            
                        var json = $.parseJSON(data);
                
                        

                        var kategori=$('#kategori').val()


                        var select = document.getElementById("subkategori");
                        var option = document.createElement("option");
                        
                        option.text = 'Pilih' 
                        option.value = ''; 

                        select.add(option);
                        for (x in json.data) {

                            if(json.data[x].kategori_id==kategori){

                                

                                var select = document.getElementById("subkategori");
                                var option = document.createElement("option");
                                
                                option.text = json.data[x].nama; 
                                option.value = json.data[x].id; 

                                select.add(option);

                            }
                            
                        }
            
                    }
                });

            
           

        }

        function getsupersubkategori(){

       
            $('#supersubkategori').find('option').remove();

            $.ajax({
                url		: '<?php echo base_url('broserviceadmin/kerusakan/getsupersubkategori/'); ?>',
                type	: 'post',
                processData: false,
                contentType: false,
                dataType: 'html',
                beforeSend : function(){
                        
                },
                success : function(data){
                        
                    var json = $.parseJSON(data);

                    var subkategori=$('#subkategori').val()
                    
                    for (x in json.data) {

                        if(json.data[x].subkategori_id==subkategori){

                            var select = document.getElementById("supersubkategori");
                            var option = document.createElement("option");
                            option.text = json.data[x].nama; 
                            option.value = json.data[x].id; 

                            select.add(option);

                        }
                        
                    }

                }
            });



        }
    </script>
    
    