    <div class='row'>
        <div class='col'>
            <table class="table table-bordered" id="tabel">
                <thead>
                    <tr>
                        
                        <th>Nama</th>
                        <th>Gambar</th>
                        <th>Tampilkan</th>
                        <th>Action</th>
                        
                    </tr>
                </thead>
            </table>

  
        </div>
        
    </div>



<script>
    $(document).ready( function () {
        $('#tabel').DataTable({
            bLengthChange: false,
            info: false,
            pageLength: 10,
            oLanguage: {
                sSearch: "cari"
            },						
            processing: true, //Feature control the processing indicator.
            serverSide: true, //Feature control DataTables' server-side processing mode.
            order: [], //Initial no order.
            // Load data for the table's content from an Ajax source
            ajax: {
                url: '<?php echo base_url('broserviceadmin/kategori/getdata/'); ?>',
                type: "post"
            },
            columns: [
                {data:'nama_kategori'},
                {data:'gambar_kategori'},
                {data:'tampilkan_kategori'},
                {data:'action', orderable: false, searchable: false}
            ]
        });
    });

</script>



