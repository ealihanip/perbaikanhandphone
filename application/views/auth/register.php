


<div class='container-fluid'>


    <div class='row'>
        


        <div class='col' style='min-height:500px'>
            <div class='card'>

                <div class='card-header'>
                    <p class='h3 text-center'>Pendaftaran</p>
                </div>
                <div class='card-body'>
                <?php if(isset($message)){?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $message;?>
                </div>
                <?php }?>
                        <form action="<?php echo base_url();?>auth/register" method="post" accept-charset="utf-8">


                    <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" class="form-control" name="first_name" value="" id="first_name">
                    </div>

                    <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input type="text" class="form-control" name="last_name" value="" id="last_name">
                    </div>

                    <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" name="email" value="" id="email">
                    </div>

                    <div class="form-group">
                    <label for="phone">No Handphone</label>
                    <input type="text" class="form-control" name="phone" value="" id="phone">
                    </div>

                    <div class="form-group">
                    <label for="address">alamat</label>

                    <textarea class="form-control" rows="5" name="address" value="" id="address"></textarea>
                    
                    </div>

                    <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" value="" id="password">
                    </div>

                    <div class="form-group">
                    <label for="password_confirm">Komfirmasi Password</label>
                    <input type="password" class="form-control" name="password_confirm" value="" id="password_confirm">
                    </div>
                    
                    
                    
                    <button type="submit" class="btn btn-primary" name="submit" value="Create User">Daftar</button>
                    
                    
                    </div>

                </form>


                </div>

            </div>
            
            
            

        </div>
    </div>
</div>

<br>

