





<div class='container-fluid'>
  

  <div class='row'>

    <div class='col'>
    <?php if(isset($message)){?>
    <div class="alert alert-primary" role="alert">
      <?php echo $message;?>
    </div>
    <?php }?>

        <div class='card'>
          <div class='card-body'>
            <form action="<?php echo base_url();?>auth/login" method="post" accept-charset="utf-8">


            <div class="form-group">
              <label for="identity">Email address</label>
              <input type="email" class="form-control" id="identity" aria-describedby="emailHelp" placeholder="Enter email" name="identity">
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" placeholder="Password" name="password">
            </div>
            <div class="form-group">
            <div class="form-check">
              <input type="checkbox" class="form-check-input" name="remember" value="1"  id="remember">
              <label class="form-check-label" for="exampleCheck1">Remember Me</label>
            </div>
            </div>
            <div class="form-group">
              <a class='text-dark' href="<?php echo base_url('auth/register')?>">Tidak Punya Akun Daftar Disini</a>
            </div>
            <div class="form-group">
              <a class='text-dark' href="<?php echo base_url('auth/forgot_password')?>">Lupa Password</a>
            </div>
            
            <button type="submit" class="btn btn-dark btn-lg" name="submit" value="Login">Login</button>
            </form>
          </div>
        </div>

    </div>

  </div>


  </div>








