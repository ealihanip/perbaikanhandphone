


<div class='container-fluid'>
    <?php if(isset($status)){?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-dismissible alert-<?php echo $status['status']?>">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><?php echo $status['message']?></strong>
                
            </div>
        </div>
    </div>
    <?php }?>

    <div class='row'>
        
        <div class='col' style='min-height:500px'>
        
            <div class='card'>

                <div class='card-body'>

                    
                        <?php echo $data['message']?>
                        <form action="<?php echo base_url('auth/edit_profile/'.$id);?>" method="post" accept-charset="utf-8">


                    <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" class="form-control" name="first_name" value="<?php echo $data['user']->first_name?>" id="first_name">
                    </div>

                    <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input type="text" class="form-control" name="last_name" value="<?php echo $data['user']->last_name?>" id="last_name">
                    </div>

                    <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" name="email" value="<?php echo $data['user']->email?>" id="email">
                    </div>

                    <div class="form-group">
                    <label for="phone">No Handphone</label>
                    <input type="text" class="form-control" name="phone" value="<?php echo $data['user']->phone?>" id="phone">
                    </div>

                    <div class="form-group">
                    <label for="address">alamat</label>

                    <textarea class="form-control" rows="5" name="address" id="address"><?php echo $data['user']->address?></textarea>
                    
                    </div>

                    <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" value="" id="password">
                    </div>

                    <div class="form-group">
                    <label for="password_confirm">Komfirmasi Password</label>
                    <input type="password" class="form-control" name="password_confirm" value="" id="password_confirm">
                    </div>
                    
                    
                    
                    <button type="submit" class="btn btn-primary" name="submit" value="Update User">Update</button>
                    
                    
                    </div>

                </form>


                </div>

            </div>
            
            
            

        </div>
    </div>
</div>

<br>

