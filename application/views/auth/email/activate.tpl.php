

<html>
    <head>
        <style type="text/css">
            
            body{

                background-color:#e3e5e8;
            }

            .container{

                width:80%;
                min-height:400px;
                margin:0 auto;
                background-color:white;
                padding-top:20px;
                padding-bottom:20px;
                padding-left:20px;
                padding-right:20px;
            }

            .header{

                width:100%;
                height:auto;
                background-color:black;
                

            }
            .header-content{

                width:95%;
                margin:0 auto;
                padding-top:10px;
                padding-bottom:10px;
                padding-left:10px;
                padding-right:10px;
                
                color:white;
            }

            .body{

                width:100%;
                height:auto;
                

            }

            .body-content{

                width:90%;
                margin:0 auto;
                padding-top:20px;
                padding-bottom:20px;
                padding-left:20px;
                padding-right:20px;
            }

            
            
        </style>
    </head>


    <body>
        <div class='container'>

            <div class='header'>
                <div class='header-content'>
                    <h2>BROSERVICE</h2>
                </div>
            </div>

            <div class='body'>
                <div class='body-content'>
				<h1> Aktifasi Email Untuk  <?php echo $identity?></h1>
				<p>Silahkan Klik Link <?php echo anchor('auth/activate/'. $id .'/'. $activation, lang('email_activate_link'));?></p>

                </div>
            </div>
        
        </div>
    </body>
</html>


