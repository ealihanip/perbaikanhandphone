

<div class='container-fluid'>
  

  <div class='row'>

    <div class='col'>
    
    
    <?php if(isset($message)){?>
    <div class="alert alert-primary" role="alert">
      <?php echo $message;?>
    </div>
    <?php }?>

        <div class='card'>
          <div class='card-body'>
            <form action="<?php echo base_url();?>auth/forgot_password" method="post" accept-charset="utf-8">


            <div class="form-group">
              <label for="identity">Email address</label>
              <input type="email" class="form-control" id="identity" aria-describedby="emailHelp" placeholder="Enter email" name="identity">
            </div>
            
            <button type="submit" class="btn btn-dark btn-lg" name="submit" value="Login">Reset Password</button>
            </form>
          </div>
        </div>

    </div>

  </div>


  </div>
