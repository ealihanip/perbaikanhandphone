

<br>
<footer class='bg-dark text-white'>
<br>

<div class='container-fluid'>
        <div class='row'>
            
            <div class='col-lg-3'>
                <h6><b>Tentang Kami</b></h6>
                <p><b>Broservice</b> adalah <i>perusahaan yang bergerak di bidang jasa perbaikan Gadget.</i><br><br>
                <b>Broservice</b><i> memiliki teknisi-teknisi dan bermitra dengan teknisi yang berpengalaman.</i><br><br>
                <b>Broservice</b><i> melayani antar Jemput gratis, atau perbaikan di tempat.</i></p>
                <i>*Layanan antar jemput khusus wil.kuningan & sekitarnya. </i>

            </div>
            <br>

            <div class='col-lg-3'>

                <h6><b>Cara kerja kami</b></h6>
                <li>Kamu dapat Estimasi Biaya</li>
                <li>Jadwalkan Perbaikan Kamu</li>
                <li>Gadget kamu di Perbaiki</li>
                <li>Kami antarkan kembali</li>
                <br>
                <h6><b>Pembayaran</b></h6>
                <li>Bayar jika Gadget selesai diperbaiki </li>
                <li>Pembayaran via COD atau Transfer</li>

            </div>
            <br>

             <div class='col-lg-2'>

                <h6><b>Cari Kami di</b></h6>
                <li><i class="fa fa-instagram"></i><a href="http://instagram.com/broservice.id/"> Broservice.id</a>
                <li><i class="fa fa-whatsapp"></i><a href="https://api.whatsapp.com/send?phone=6287724456289&text=Halo%20Bro%20saya%20memiliki%20pertanyaan%20mengenai%20kerusakan%20gadget"> WhatsApp</a>
        

            </div>

            <br>

            <div class='col-lg-4'>

               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.737494107977!2d108.48917881520882!3d-6.921951994998665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6f17111f834235%3A0x466fc6c11fbd2dcc!2sJl.+Raya+Desa+Nanggerang%2C+Kabupaten+Kuningan%2C+Jawa+Barat!5e0!3m2!1sid!2sid!4v1522199917507" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
	
                
            </div>
        
        </div>


</div> 
    


    </footer >

        
        
        
        
        <!-- Popper 3 -->
        <script src="<?php echo base_url();?>assets/node_modules/popper.js/dist/umd/popper.min.js"></script>
        
        <!-- Bootstrap 4 -->
        <script src="<?php echo base_url();?>assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>


        <!-- owl-->
        <script src="<?php echo base_url();?>assets/node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
        
                
        <!-- datepicker-->
        <script src="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/datepicker/bootstrap-datepicker.js"></script>
        

        <!-- moment-->
        <script src="<?php echo base_url();?>assets/node_modules/moment/moment.js"></script>
        
    
        <!-- datepicker material-->
        <script src="<?php echo base_url();?>assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
        
        <!-- DataTable -->
        <script src="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/datatables/jquery.dataTables.js"></script>

        
        <!-- DataTable -->
        <script src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
        
        <!-- DataTable -->
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        
        
        <script>
            $('#supportservice').owlCarousel({
                loop:true,
                items:8,
                margin:10,
                autoHeight:true
            });

            $('#slider').owlCarousel({
                loop:true,
                items:1,
                margin:10,
                autoHeight:true
            });


        </script>

    </body>



</html>