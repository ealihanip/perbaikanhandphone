<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">BROSERVICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url('home')?>">Home <span class="sr-only">(current)</span></a>
        </li>

        
        </ul>

        <?php if($this->ion_auth->logged_in()){?>
        

        <!-- Right nav -->
        <ul class="nav navbar-nav navbar-right">
            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Hi, <?php $user = $this->ion_auth->user()->row();
                        echo $user->first_name;?>
                </a>

                <?php if($this->ion_auth->is_admin()){?>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?php echo base_url('broserviceadmin/perbaikan/')?>">Halaman Admin</a>
                    <div class="dropdown-divider"></div>

                </div>

                <?php }else{?>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?php echo base_url('auth/edit_profile/'.$user->id)?>">Edit Profile</a>
                    <a class="dropdown-item" href="<?php echo base_url('perbaikan/histori-perbaikan')?>">Histori Perbaikan</a>
                    <a class="dropdown-item" href="<?php echo base_url('klaim-garansi/histori-klaim-garansi')?>">Histori Klaim Garansi</a>
                    <div class="dropdown-divider"></div>
                    
                </div>
                
                <?php }?>
            </li>

            <li class="nav-item active">
                <a class="nav-link" href="<?php echo base_url();?>auth/logout">Logout</span></a>
            </li>
            
        </ul>
        <?php }else{?>

        <ul class="navbar-nav justify-content-end">
            
            <li class="nav-item">
                <a class="nav-link active" href="<?php echo base_url();?>auth/login">Login</a>
            </li>
            
        </ul>

        <?php }?>

    </div>
    </nav>

    <br>