<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BROSERVICE.COM</title>
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon-32x32.png">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/admin-lte/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  
  <!-- lightbox -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/lightbox2/dist/css/lightbox.css">  
 
  <!-- Data Table -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/datatables/dataTables.bootstrap4.css">
  
  <!-- Data Table -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/datatables.net-dt/css/jquery.dataTables.min.css">
  
  <!-- Bootstrap Editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


  <!-- export data table -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <!-- export data table -->
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
  

 
  



  <!-- jQuery -->
  <script src="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/jquery/jquery.min.js"></script> 

</head>
<body class="hold-transition sidebar-mini">
