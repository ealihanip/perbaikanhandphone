<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>BROSERVICE.COM</title>
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon-32x32.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap 4 -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/bootstrap/dist/css/bootstrap.min.css">


        <!-- OWL caurosel -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/owl.carousel/dist/assets/owl.carousel.min.css">
        

        <!-- datepicker -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/admin-lte/plugins/datepicker/datepicker3.css">
        

        <!-- bootstrap material datetimepicker -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css">
        
       
        <!-- Data Table -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/datatables.net-dt/css/jquery.dataTables.min.css">
        
        <!-- Data Table -->
        <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css">
    
        <!-- Data Table -->
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
        
        <!-- Jquery 3 -->
        <script src="<?php echo base_url();?>assets/node_modules/jquery/dist/jquery.min.js"></script>

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">




        
        
    </head>

        

    <body class='bg-light'>

    
 

    
