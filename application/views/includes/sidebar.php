<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      
        <span class="brand-text font-weight-light">BROSERVICE ADMIN</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
            <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa fa-th"></i>
              <p>
                Perbaikan
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url('broserviceadmin/perbaikan/'); ?>" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Olah Data Perbaikan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url('broserviceadmin/perbaikan/diterima'); ?>" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Data Perbaikan Diterima</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="<?php echo base_url('broserviceadmin/perbaikan/selesai'); ?>" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Data Perbaikan Selesai</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="<?php echo base_url('broserviceadmin/perbaikan/ditolak'); ?>" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Data Perbaikan Ditolak</p>
                </a>
              </li>

              
              
            </ul>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url('broserviceadmin/klaim-garansi/'); ?>" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Klaim Garansi
                
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="<?php echo base_url('broserviceadmin/slider/'); ?>" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Slider
                
              </p>
            </a>
          </li>
          
          
          <li class="nav-item">
            <a href="<?php echo base_url('broserviceadmin/kerusakan/'); ?>" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Kerusakan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('broserviceadmin/kategori/'); ?>" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Kategori
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('broserviceadmin/subkategori/'); ?>" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Sub Kategori
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('broserviceadmin/supersubkategori/'); ?>" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Super Sub Kategori
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url('broserviceadmin/kupon/'); ?>" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Kupon
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url('broserviceadmin/testimoni/'); ?>" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Testimoni
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url('auth'); ?>" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                User Management
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('broserviceadmin/pengaturan/'); ?>" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Pengaturan
                
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="<?php echo base_url('auth/logout/'); ?>" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Logout
              </p>
            </a>
          </li>

          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  