<br>

<div class='container-fluid bg-dark'>
    <br>
        <div class='row'>
            <div class='col'>

                <p class='h5 text-center text-white'>Testimoni Pelanggan</p>

            </div>
        </div>
    <br>
        <div class='row'>
        
            <div class='col'>
                <?php foreach($testimoni as $value){?>
                    <?php if($value['tampilkan']==true){?>
                    
                        <div class="card">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item smaall text-primary"><?php echo $value['tanggal_testimoni']?></li>
                                <li class="list-group-item h5"><?php echo $value['first_name']?></li>
                                <li class="list-group-item small">Perbaikan: <?php echo $value['nama_jenis']?> <?php echo $value['nama_brand']?> <?php echo $value['nama_model']?></li>
                                <li class="list-group-item small"><?php echo $value['testimoni']?></li>
                                
                            </ul>
                        </div>
                        <br>
                    <?php }?>
                <?php }?>
               


            </div>

            
        </div>
        <br>
</div>