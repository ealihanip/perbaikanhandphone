
<div class='container-fluid'>

<div class='container'>
    
    <div class='row'>

        <div class='col'>

            <div class='card'>
                <div class='card-header bg-white'>
                    
                </div>
                <div class='card-body'>
                    <p class='text-center h4'>BATALKAN KLAIM GARANSI</p>
                    <p class='text-center'>Data Klaim Garansi Akan Di Hapus Secara Permanen</p>
                    <p class='text-center'>Apakah Anda Yakin?</p>

                    <center>
                        <a class='btn btn-danger' href="<?php echo base_url('klaim-garansi/batalkan-klaim-garansi/destroy/'.$kode)?>">Batalkan Sekarang !</a>
                        <a class='btn btn-dark' href='<?php echo base_url('klaim-garansi/histori-klaim-garansi/')?>'>Kembali</a>
                    </center>
                    
                </div>

                <div class='card-footer bg-white'>
                      
                </div>

            </div>
            



        </div>
    </div>



</div>

</div>

<br>
