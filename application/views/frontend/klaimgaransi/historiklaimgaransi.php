


<div class='container-fluid'>

<p class='h5'>Perbaikan Saat Ini</p>
    <div class='row'>
    <?php foreach($data as $klaimgaransi){?>
        <div class='col-lg-12'>
            
            <!-- <div class='card'>

                <div class='card-body'>
                    <div class='row'>
                        <div class='col-lg-12'>
                        <p class='h6'>Booking</p>
                        </div>
                    </div>
                </div>

            </div> -->
        
            <div class='card'>
                <div class='card-header bg-white'>
                    <div class='row'>

                        <div class=col-lg-2>
                            <p>kode: <?php echo $klaimgaransi['kode_klaim_garansi']?></p>
                        </div>
                        <div class=col-lg-2>
                            <p>Tanggal: <?php echo date('d-m-Y',strtotime($klaimgaransi['tanggal_klaim_garansi']))?></p>
                        </div>
                        <div class=col-lg-4>
                            <?php if($klaimgaransi['status_klaim_garansi']==false ){?>
                                <p><a class='btn btn-dark btn-sm text-white' href='<?php echo base_url('klaim-garansi/batalkan-klaim-garansi/'.$klaimgaransi['kode_klaim_garansi'])?>'>Batalkan Klaim Garansi</a></p>
                            <?php }?>
                            <?php if($klaimgaransi['status_perbaikan']==true ){?>
                               
                               
                            <?php }?>
                        </div>
                        <div class=col-lg-4>
                            
                        </div>
                    </div>
                    
                    
                </div>
                <div class='card-body'>
                    <div class='row'>
                        <div class='col-lg-4'>
                            <p class='h6'>Klaim Garansi Untuk: <?php echo $klaimgaransi['kode_perbaikan']?></p>
                          
                        </div>
                        <?php if($klaimgaransi['jenis_layanan']==false){?>
                        <div class='col-lg-3'>
                            <p class='h6'>Jenis Layanan: Antar Jemput</p>
                            <?php foreach($klaimgaransi['antar_jemput'] as $antarjemput){?>
                            <hr>
                            <p>Status : <?php echo $antarjemput['nama_status']?> </p>
                            <hr>
                            <p>Tanggal Jemput : <?php echo date('d-m-Y',strtotime($antarjemput['tanggal']))?></p>
                            <p>Jam Jemput : <?php echo $antarjemput['jam']?></p>
                            <p>Alamat Jemput : <?php echo $antarjemput['alamat']?> </p>
                            <?php if($antarjemput['status']==1){?>
                            <p><a class='btn btn-dark btn-sm text-white' href='<?php echo base_url('klaim-garansi/ubah-jadwal-antar-jemput/'.$klaimgaransi['kode_klaim_garansi'])?>'>Ubah Jadwal</a></p>
                            <?php }?>
                            <?php }?>
                            <hr>
                        </div>
                        <?php }else{?>
                        <div class='col-lg-4'>
                            <p class='h6'>Jenis Layanan: Kirim Online</p>
                            <?php foreach($klaimgaransi['kirim_online'] as $kirimonline){?>
                            <hr>
                            <p>Status : <?php echo $kirimonline['nama_status']?> </p>
                            <hr>
                            <p class='h6'>Pengiriman Dari Konsumen</p>
                            <p>Kurir : <?php echo $kirimonline['kurir']?></p>
                            <p>Resi : <?php echo $kirimonline['resi']?></p>
                            <?php if($kirimonline['status']==1){?>
                                <p><a class='btn btn-dark btn-sm text-white' href='<?php echo base_url('klaim-garansi/kirim-online/input-resi/'.$klaimgaransi['kode_klaim_garansi'])?>'>Input/Ubah Resi</a></p>
                            <?php }?>
                            <hr>
                            <?php if($kirimonline['status']==3){?>
                            <p class='h6'>Pengiriman Dari Broservice</p>

                            <p>Kurir : <?php echo $kirimonline['kurirkembali']?></p>
                            <p>Resi : <?php echo $kirimonline['resikembali']?></p>
                            
                            <?php }?>
                            <?php }?>
                            
                            <hr>
                        </div>

                        <?php }?>
                       
                        <div class='col-lg-4'>
                            
                            
                            <p class='h6'>Status Perbaikan : <?php echo $klaimgaransi['nama_status_perbaikan']?></p>
                            <p class='h6'>Status Klaim Garansi : <?php echo $klaimgaransi['nama_status_klaim_garansi']?></p>
                            <hr>

                        </div>
                    </div>

                    <div class='card-footer bg-white'>
                        <?php if($klaimgaransi['status_klaim_garansi']==false ){?>
                            <small class='text-danger'>*Booking Tidak Bisa di Batalkan Atau Di Tambah Kerusakannya Jika Sudah Di Terima Oleh Admin</small>
                        <?php }?>

                        <?php if($klaimgaransi['status_perbaikan']==true && $klaimgaransi['status_transaksi']!=4 ){?>
                            <a class='btn btn-dark text-white' href='<?php echo base_url('klaim-garansi/komfirmasi-barang-diterima/'.$klaimgaransi['kode_klaim_garansi'])?>'>Komfirmasi Barang Di Terima</a>
                        <?php }?>
                    </div>
                </div>

            </div>
            <hr>
        </div>
    <?php }?> 

    </div>
    
    <div class='row'>

        <div class='col-lg-12'>
            <!-- <div class='card'>

                <div class='card-header bg-white'>
                    <p class='h4'>Riwayat Transaksi Anda</p>
 
                </div>

                <div class='card-body'>

                </div>

            </div> -->
            
        </div>
        
    </div>

</div>