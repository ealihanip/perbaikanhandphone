
<div class='container-fluid'>

<div class='container'>
    
    <div class='row'>

        <div class='col'>

            <div class='card'>
                <div class='card-header bg-white'>
                    <p class='text-center h4'>Layanan Antar Jemput Barang</p>
                    <p class='text-center'>Kami Datang Ke Lokasi Anda Dengan Biaya Gratis Jika Lokasi Masih Satu Daerah Dengan Kami</p>
                </div>
                <div class='card-body'>
                    <form action="<?php echo base_url('klaim-garansi/antar-jemput/store/'.$kode); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-row">
                                <div class='col-12'>
                                    <label for="tanggal">Tanggal Dan Jam</label>
                                </div>
                                <div class='col'>
                                    <input type="text" class="form-control" id="tanggal" name="tanggal">
                                    <span class="help-block text-danger"><?php echo form_error('tanggal'); ?></span>
                                </div>

                                <div class='col'>
                                    
                                    <select class="form-control" id="jam" name='jam'>
                                        <option>11:00</option>
                                        <option>12:00</option>
                                        <option>13:00</option>
                                        <option>14:00</option>
                                        <option>15:00</option>
                                        <option>16:00</option>
                                        <option>17:00</option>
                                    </select>
                                </div>
                        </div>

                        


                        
                        <div class="form-group">
                            <label for="alamat">Alamat Penjemputan</label>
                            <textarea class="form-control" id="alamat" name="alamat" rows="3"></textarea>
                            <span class="help-block text-danger"><?php echo form_error('alamat'); ?></span>
                        </div>


                        
                    
                </div>

                <div class='card-footer bg-white'>
                        <p class='text-center'><input type='submit' class='btn btn-dark btn-lg' value='Buat Janji'></p>
                    </form>
                </div>

            </div>
            



        </div>
    </div>



</div>

</div>

<br>



<script>

$(document).ready( function () {

     //Date picker

    

    $('#tanggal').bootstrapMaterialDatePicker({ 
         
        weekStart : 0, 
        time: false,
        minDate : new Date() 
    });

     
});

</script>