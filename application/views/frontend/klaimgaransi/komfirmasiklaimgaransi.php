
<div class='container-fluid'>

<div class='container'>
    
    <div class='row'>

        <div class='col'>

            <div class='card'>
                <div class='card-header bg-white'>
                    <p class='text-center h4'>Apakah Anda Yakin Untuk Melakukan Klaim Garansi</p>
                </div>
                <div class='card-body'>
                    
                    <p class='text-center'>
                        
                        <a class='btn btn-dark bt-lg text-white' href='<?php echo base_url('klaim-garansi/create/'.$kode)?>'>Ya Saya Yakin</a>
                        <a class='btn btn-dark bt-lg text-white' href='<?php echo base_url('perbaikan/histori-perbaikan')?>'>Kembali</a>
                
                    </p>
                </div>

                <div class='card-footer bg-white'>
                        
                </div>

            </div>
            



        </div>
    </div>



</div>

</div>

<br>



<script>

$(document).ready( function () {

     //Date picker

    $('#jam').bootstrapMaterialDatePicker
    ({
        date: false,
        shortTime: false,
        format: 'HH:mm'
    });

    $('#tanggal').bootstrapMaterialDatePicker({ 
         
        weekStart : 0, 
        time: false
    });

     
});

</script>