




   




<div class='container-fluid'>

    <div class='row'>

        
        <?php foreach($data as $value){?>
        
        <div class='col-md-12'>
            <hr>
            <p class='h5'>Kerusakan: <?php echo $value['nama']?> </p>		
            <hr>  
        </div>
        
        
        <div class='col-lg-4'>
            <img src='<?php echo base_url();?>assets/uploads/<?php echo $this->session->pilih?>/<?php echo $value['gambar']?>' class='rounded mx-auto d-block' title='Layar / LCD' alt='Layar / LCD' width='300' height='300'>
                    
        
        </div>
    
        <div class='col-lg-4'>
            <div class='card'>
                <div class='card-body'>
                    <?php echo $value['deskripsi']?>
                </div>
            </div>	
        
        </div>


        <div class='col-lg-4'>
        <?php if(isset($this->session->kupon)){?>
            <?php if($this->session->kupon){?>
        
            <?php }else{?>
        <div class='card'>

            <div class='card-body'>
                <p class='h5 text-center'>Punya Kode Kupon</p>
            </div>
            
            <div class='card-body'>

                <form action="<?php echo base_url('perbaikan/add-kupon'); ?>" method="post" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <textarea class="form-control" id="kupon" name="kupon" rows="2"></textarea>
                        <span class="help-block text-danger"><?php echo form_error('kupon'); ?></span>
                    </div>
                    <input type='hidden' name='url' value='<?php echo $_SERVER["REQUEST_URI"]?>'>
                    <div class="form-group">
                        <p class='text-center'><input type='submit' class='btn btn-dark btn-lg' value='Masukan Kode'></p>
                    </div>
                </form>
            </div>
        </div>
        <hr>
            <?php }?>
        
        <?php }else{?>
            
        <div class='card'>

            <div class='card-body'>
                <p class='h5 text-center'>Punya Kode Kupon</p>
            </div>
            
            <div class='card-body'>

                <form action="<?php echo base_url('perbaikan/add-kupon'); ?>" method="post" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <textarea class="form-control" id="kupon" name="kupon" rows="2"></textarea>
                        <span class="help-block text-danger"><?php echo form_error('kupon'); ?></span>
                    </div>
                    <input type='hidden' name='url' value='<?php echo $_SERVER["REQUEST_URI"]?>'>
                    <div class="form-group">
                        <p class='text-center'><input type='submit' class='btn btn-dark btn-lg' value='Masukan Kode'></p>
                    </div>
                </form>
            </div>
        </div>
        <hr>

        <?php }?>
        

        
        <div class='card'>


            <div class='card-body'>

                <p class='h4'>Biaya Perbaikan</p>
                <?php if($value['diskon']!=false){?>
                    <p class='h5'><strike class='text-danger'>Rp.<?php echo $value['harga']?>,-</strike> Diskon <?php echo $value['diskon'].'%'?> Menjadi</p>
                    <p class='h5'>Rp.<?php $harga=($value['harga']-(($value['harga']*$value['diskon']/100))); echo $harga; ?>,-</p>
                <?php }else{?>
                    <p class='h5'>Rp.<?php echo $value['harga']?>,-</p>
                <?php }?>
                <p class='h4'>Biaya Antar Jemput</p>
                <p class='h5'>Gratis</p>
                

                
                <label for="chkPassport">
                    <input type="checkbox" id="chkPassport" />
                    Saya sudah membaca dan menyetujui syarat dan ketentuan Terms & Conditions
                </label>
                <br />
                Pesan sekarang:
                <a href="<?php echo base_url();?>perbaikan/pilihlayanan/">
                    <button type="button" id="booking" disabled="disabled"  name="button" class="btn btn-danger">Buat Janji</button>
                </a>   
            </div>
        </div>
            
                
        </div>
    </div>
</div>
    <?php }?>


<br>

<script>
    $(function () {
        $("#chkPassport").click(function () {
            if ($(this).is(":checked")) {
                $("#booking").removeAttr("disabled");
                $("#booking").removeClass("btn btn-danger");
                $("#booking").attr("class", "btn btn-primary");

            } else {
                $("#booking").attr("disabled", "disabled");
                $("#booking").removeClass("btn btn-primary");
                $("#booking").attr("class", "btn btn-danger");
            }
        });
    });
</script>



<?php if(isset($kupon_ditemukan)){?>
    <?php if($kupon_ditemukan==true){?>

        <!-- Modal -->
        <div class="modal fade" id="modalkupon" tabindex="-1" role="dialog" aria-labelledby="modalkupon" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                
            </div>
            <div class="modal-body">

                <?php if($harga>=$kupon['min_transaksi']){?>

                    <p class='h5'>Anda Akan Menggunakan Kupon <?php echo $kupon['nama']?></p>
                    
                    <form action="<?php echo base_url('perbaikan/store-kupon'); ?>" method="post" enctype="multipart/form-data">
                    
                    <input type='hidden' name='kupon' value='<?php echo $kupon['nama']?>'>
                    <input type='submit' class='btn btn-primary btn-sm' value='Ya Gunakan Kupon Ini'>
                    </form>
                <?php }else{ ?>
                    <p class='h5'>Minimal Harga Kerusakan Untuk Kupon Ini Adalah <?php echo $kupon['min_transaksi']?></p>
                    
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>
        <script>

    $(document).ready( function () {
        $('#modalkupon').modal('show')
    });
</script>

<?php }else{?>

        <!-- Modal -->
        <div class="modal fade" id="modalkupon" tabindex="-1" role="dialog" aria-labelledby="modalkupon" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"></h5>
               
            </div>
            <div class="modal-body">
                OOPS Kupon Tidak Ditemukan
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>

<script>
    $(document).ready( function () {
        $('#modalkupon').modal('show')
    });
</script>
<?php }?>
<?php }?>





















