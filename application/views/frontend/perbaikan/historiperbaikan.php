


<div class='container-fluid'>

<p class='h5'>Perbaikan Saat Ini</p>
    <div class='row'>
    <?php foreach($data as $perbaikan){?>
        <div class='col-lg-12'>
            
            <!-- <div class='card'>

                <div class='card-body'>
                    <div class='row'>
                        <div class='col-lg-12'>
                        <p class='h6'>Booking</p>
                        </div>
                    </div>
                </div>

            </div> -->
        
            <div class='card'>
                <div class='card-header bg-white'>
                    <div class='row'>

                        <div class=col-lg-2>
                            <p>kode: <?php echo $perbaikan['kode']?></p>
                        </div>
                        <div class=col-lg-2>
                            <p>Tanggal: <?php echo date('d-m-Y',strtotime($perbaikan['tanggal_booking']))?></p>
                        </div>
                        <div class=col-lg-4>
                            <?php if($perbaikan['status_booking']==false ){?>
                                <a class='btn btn-dark btn-sm text-white' href='<?php echo base_url('perbaikan/batalkan-perbaikan/'.$perbaikan['kode'])?>'>Batalkan Booking</a></p>
                            <?php }?>
                            <?php if($perbaikan['status_perbaikan']==true ){?>
                                
                                
                                <?php if($perbaikan['status_perbaikan']==true){?>
                                    <p><a class='btn btn-dark btn-sm text-white' href='<?php echo base_url('perbaikan/cetak-kwitansi/'.$perbaikan['kode'])?>'>Cetak Kwitansi</a>
                                    <?php 
                                        $range = date_range($perbaikan['tanggal_selesai_perbaikan'], date("Y-m-d"));
                                        
                                        $selisihtanggal=0;
                                        foreach ($range as $date)
                                        {
                                            $selisihtanggal++;
                                        }

                                        if($selisihtanggal<31){

                                    
                                    ?>

                                        
                                
                                    <?php }?>
                                    </p>
                                <?php }?>
                            <?php }?>
                        </div>
                        <div class=col-lg-4>
                            <?php if($perbaikan['kupon']!=null ){?>
                                
                                <?php if($perbaikan['kupon']->min_transaksi<=preg_replace('/\D/', '', $perbaikan['total_harga'])){?>
                                
                                    <p>KUPON: <?php echo $perbaikan['kupon']->nama ?> <a class='btn btn-dark btn-sm text-white' href='<?php echo base_url('perbaikan/detail-hadiah/'.$perbaikan['kupon']->nama)?>'>Lihat Detail Hadiah</a></p>
                            
                                <?php }else{?>
                                    
                                    <p class='small'>KUPON: Tidak Aktif Karena Kurangnya Total Transaksi minimal transaksi adalah Rp.<?php echo $perbaikan['kupon']->min_transaksi?>,-</p>
                            
                                <?php }?>
                                    
                                    
                                
                            <?php }?>
                        </div>
                    </div>
                    
                    
                </div>
                <div class='card-body'>
                    <div class='row'>
                        <div class='col-lg-3'>
                            <p class='h6'>Perbaikan: <?php echo $perbaikan['nama_jenis']?> <?php echo $perbaikan['nama_brand']?> <?php echo $perbaikan['nama_model']?></p>
                            <p>Kerusakan :</p>
                            <?php $no=1; foreach($perbaikan['daftar_kerusakan'] as $daftarkerusakan){?>
                            <p><?php echo $no?>. <?php echo $daftarkerusakan['nama_kerusakan']?></p>
                            
                            <?php $no++; }?>

                            <?php if($perbaikan['status_booking']==false ){?>
                            <p><a class='btn btn-dark btn-sm text-white' href='<?php echo base_url('perbaikan/tambah-kerusakan/'.$perbaikan['kode'])?>'>Tambah Kerusakan</a></p>
                            <?php  }?>
                            <hr>
                        </div>
                        <?php if($perbaikan['jenis_layanan']==false){?>
                        <div class='col-lg-3'>
                            <p class='h6'>Jenis Layanan: Antar Jemput</p>
                            
                            <?php foreach($perbaikan['antar_jemput'] as $antarjemput){?>
                            <hr>
                            <p>Status : <?php echo $antarjemput['nama_status']?> </p>
                            <hr>
                            <p>Tanggal Jemput : <?php echo date('d-m-Y',strtotime($antarjemput['tanggal']))?></p>
                            <p>Jam Jemput : <?php echo $antarjemput['jam']?></p>
                            <p>Alamat Jemput : <?php echo $antarjemput['alamat']?> </p>
                            <?php if($antarjemput['status']==1){?>
                            <p><a class='btn btn-dark btn-sm text-white' href='<?php echo base_url('perbaikan/ubah-jadwal-antar-jemput/'.$perbaikan['kode'])?>'>Ubah Jadwal</a></p>
                            <?php }?>
                            
                            <?php }?>
                            <hr>
                        </div>
                        <?php }else{?>
                        <div class='col-lg-3'>
                            <p class='h6'>Jenis Layanan: Kirim Online</p>
                            <?php foreach($perbaikan['kirim_online'] as $kirimonline){?>
                            <hr>
                            <p>Status : <?php echo $kirimonline['nama_status']?> </p>
                            <hr>
                            <p class='h6'>Pengiriman Dari Konsumen</p>
                            <p>Kurir : <?php echo $kirimonline['kurir']?></p>
                            <p>Resi : <?php echo $kirimonline['resi']?></p>
                            <?php if($kirimonline['status']==1){?>
                                <p><a class='btn btn-dark btn-sm text-white' href='<?php echo base_url('perbaikan/kirim-online/input-resi/'.$perbaikan['kode'])?>'>Input/Ubah Resi</a></p>
                            <?php }?>
                            <hr>
                            <?php if($kirimonline['status']==3){?>
                            <p class='h6'>Pengiriman Dari Broservice</p>

                            <p>Kurir : <?php echo $kirimonline['kurirkembali']?></p>
                            <p>Resi : <?php echo $kirimonline['resikembali']?></p>
                            
                            <?php }?>
                            <?php }?>
                            
                            <hr>
                        </div>

                        <?php }?>
                        <div class='col-lg-3'>

                        <?php $hargatotal=0; foreach($perbaikan['daftar_kerusakan'] as $daftarkerusakan){?>
                        <?php $hargatotal=$hargatotal+$daftarkerusakan['harga']; }?>   
                            <p>Harga Total : Rp.<?php echo preg_replace('/\D/', '', $perbaikan['total_harga'])?>,-</p>


                            
                            <p>Pembayaran : <?php echo $perbaikan['nama_status_pembayaran']?></p>
                            
                           
                            
                        
                            <hr>
                            
                        </div>
                        <div class='col-lg-3'>
                            
                        <?php if($perbaikan['status_transaksi']!=8){ ?>   
                            <p class='h6'>Status Perbaikan : <?php echo $perbaikan['nama_status_perbaikan']?></p>
                            <p class='h6'>Status Booking : <?php echo $perbaikan['nama_status_booking']?></p>
                            <hr>

                            <?php if($perbaikan['status_perbaikan']==true && $perbaikan['jenis_layanan']==true && $perbaikan['status_pembayaran']==false ){?>
                            <div class="alert alert-secondary" role="alert">
                                <div class="alert alert-info" role="alert">
                                <small class='text-danger'>
                                    Perbaikan Telah Selesai Harap Melakukan Pembayaran dan Komfirmasi Pembayaran 
                                    Untuk Melihat Info Pembayaran Perbaikan Ini Bisa Menekan Tombol Info Pembayaran Di Bawah Ini.
                                </small>
                                </div>
                                <hr>
                                <p><a class='btn btn-dark btn-sm text-white' href="<?php echo base_url('perbaikan/info-pembayaran/'.$perbaikan['kode'])?>"> Info Pembayaran</a></p>
                                <p><a class='btn btn-dark btn-sm text-white' href="<?php echo base_url('perbaikan/komfirmasi-pembayaran/'.$perbaikan['kode'])?>">Komfirmasi Pembayaran</a></p>
                            </div>
                            <?php }?>

                            <hr>
                            <?php if($perbaikan['status_perbaikan']==true){?>
                            <p class='small'>Tanggal Akhir Klaim Garansi : <?php echo date('d-m-Y',strtotime($perbaikan['tanggal_akhir_garansi']))?></p>
                            <a class='btn btn-dark btn-sm text-white' href='<?php echo base_url('klaim-garansi/komfirmasi-klaim-garansi/'.$perbaikan['kode'])?>'>Klaim Garansi</a>
                            <?php }?>
                        </div>

                        <?php }else{?>

                            <p class='h6 text-danger'>Status : Ditolak Karena Barang Tidak Bisa Di Perbaiki</p>
                            
                        
                        <?php }?>
                           
                    </div>

                    <div class='card-footer bg-white'>
                        <?php if($perbaikan['status_booking']==false ){?>
                            <small class='text-danger'>*Booking Tidak Bisa di Batalkan Atau Di Tambah Kerusakannya Jika Sudah Di Terima Oleh Admin</small>

                            
                        <?php }?>
                        <?php if($perbaikan['status_perbaikan']==true && $perbaikan['status_transaksi']!=4 ){?>
                            <a class='btn btn-dark text-white' href='<?php echo base_url('perbaikan/komfirmasi-barang-diterima/'.$perbaikan['kode'])?>'>Komfirmasi Barang Di Terima</a>
                        <?php }?>
                    </div>
                </div>

            </div>
            <hr>
        </div>
    <?php }?> 

    </div>
    
    <div class='row'>

        <div class='col-lg-12'>
            <!-- <div class='card'>

                <div class='card-header bg-white'>
                    <p class='h4'>Riwayat Transaksi Anda</p>
 
                </div>

                <div class='card-body'>

                </div>

            </div> -->
            
        </div>
        
    </div>

</div>