
<div class='container-fluid'>

<div class='container'>
    
    <div class='row'>

        <div class='col'>

            <div class='card'>
                <div class='card-header bg-white'>
                    <p class='text-center h4'>Tambah Kerusakan</p>
                    <p class=''>Kode Booking : <?php echo $kode;?></p>
                </div>
                <div class='card-body'>
                    <form action="<?php echo base_url('perbaikan/tambah-kerusakan/store/'.$kode); ?>" method="post" enctype="multipart/form-data">
                        
                    <div class="form-group">
                        <label for="kerusakan">Example select</label>
                        <select class="form-control" id="kerusakan" name="kerusakan">
                            <?php foreach($data as $value){?>
                                <option Value="<?php echo $value['id']?>"><?php echo $value['nama']?></option>
                            <?php }?>
                        </select>
                    </div>
                        
                    
                </div>

                <div class='card-footer bg-white'>
                        <p class='text-center'><input type='submit' class='btn btn-dark btn-lg' value='Tambah'></p>
                        <a class='btn btn-dark' href='<?php echo base_url('perbaikan/histori-perbaikan/')?>'>Kembali</a>
                    </form>
                </div>

            </div>
            



        </div>
    </div>



</div>

</div>

<br>



<script>

$(document).ready( function () {

     //Date picker

    $('#jam').bootstrapMaterialDatePicker
    ({
        date: false,
        shortTime: false,
        format: 'HH:mm'
    });

    $('#tanggal').bootstrapMaterialDatePicker({ 
         
        weekStart : 0, 
        time: false,
        minDate : new Date() 
    });

     
});

</script>