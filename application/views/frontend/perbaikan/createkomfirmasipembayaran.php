
<div class='container-fluid'>

<div class='container'>
    
    <div class='row'>

        <div class='col'>

            <div class='card'>
                <div class='card-header bg-white'>
                    <p class='text-center h4'>Komfirmasi Transfer Pembayaran</p>
                    
                </div>
                <div class='card-body'>
                    <form action="<?php echo base_url('perbaikan/komfirmasi-pembayaran/store/'.$kode); ?>" method="post" enctype="multipart/form-data">
                        
                        <div class="form-group">
                            <label for="bank">Nama Bank</label>
                            <input type="text" class="form-control" id="bank" name="bank">
                            <span class="help-block text-danger"><?php echo form_error('bank'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="norek">No Rekening</label>
                            <input type="text" class="form-control" id="norek" name="norek">
                            <span class="help-block text-danger"><?php echo form_error('norek'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="nama">Nama Pemilik Rekening</label>
                            <input type="text" class="form-control" id="nama" name="nama">
                            <span class="help-block text-danger"><?php echo form_error('nama'); ?></span>
                        </div>
                        
                        <div class="form-group">
                            <label for="tanggal">Tanggal Transfer</label>
                            <input type="text" class="form-control" id="tanggal" name="tanggal">
                            <span class="help-block text-danger"><?php echo form_error('tanggal'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <textarea class="form-control" id="keterangan" name="keterangan" rows="3"></textarea>
                            <span class="help-block text-danger"><?php echo form_error('keterangan'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="foto">Foto Bukti Transfer</label>
                            <input type="file" class="form-control-file" id="foto" name='foto'>
                            <span class="help-block text-danger"><?php echo form_error('foto'); ?></span>
                        </div>
                    
                </div>

                <div class='card-footer bg-white'>
                        <p class='text-center'><input type='submit' class='btn btn-dark btn-lg' value='Kirim'></p>
                    </form>
                </div>

            </div>
            



        </div>
    </div>



</div>

</div>

<br>



<script>

$(document).ready( function () {

     //Date picker

    $('#jam').bootstrapMaterialDatePicker
    ({
        date: false,
        shortTime: false,
        format: 'HH:mm'
    });

    $('#tanggal').bootstrapMaterialDatePicker({ 
         
        weekStart : 0, 
        time: false
    });

     
});

</script>