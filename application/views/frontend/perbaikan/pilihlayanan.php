<div class='container-fluid'>

    <div class='container'>

        <?php foreach($data as $value){?>
        <div class='row justify-content-md-center'>


            <div class='col-lg-8'>

                <div class="card">
                    <div class="card-header bg-white">
                        <h4 class="card-title text-center">Antar Jemput Ke Lokasi</h4>
                    </div>
                    <div class="card-body">
                        <p>Kami Akan Datang Menjemput Ke Lokasi Anda, jika perbaikan telah selesai kami akan antar kembali barang milik anda ke lokasi anda</p>
                        
                        <small class='text-danger'>*Hanya Berlaku Di Sekitar Kuningan Dan Sekitarnya</small>
                        </div>

                    
                    <div class="card-footer bg-white">
                    
                        <h4 class="card-title text-center"><a href="<?php echo base_url('perbaikan/antar-jemput/')?>" class="btn btn-dark">Pilih Layanan Ini</a>
                    </h4>
                    </div>
                </div>
                
            </div>
        </div>
        <hr>
        <div class='row justify-content-md-center'>
            <div class='col-lg-8'>

                <div class="card">
                    <div class="card-header bg-white">
                        <h4 class="card-title text-center">Kirim Via Ke Lokasi Kami</h4>
                    </div>
                    <div class="card-body">
                        
                        <p >Kirim ke alamat kami, kemudian setelah barang selesai diperbaiki kami akan kirim kembali ke anda</p>
                        </div>
                    <div class="card-footer bg-white">
                        <h4 class="card-title text-center"><a href="<?php echo base_url('perbaikan/kirim-online/')?>" class="btn btn-dark">Pilih Layanan Ini</a>
                    </h4>
                    </div>
                </div>
                
            </div>
        </div>
        <?php }?>

    </div>
</div>