<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//kupon route
$route['broserviceadmin/kupon'] = 'admin/kupon/index';
$route['broserviceadmin/kupon/get'] = 'admin/kupon/get/';
$route['broserviceadmin/kupon/getdata'] = 'admin/kupon/getdata/';
$route['broserviceadmin/kupon/getdata/(:any)'] = 'admin/kupon/getdata/';
$route['broserviceadmin/kupon/create'] = 'admin/kupon/create';
$route['broserviceadmin/kupon/store'] = 'admin/kupon/store';
$route['broserviceadmin/kupon/show/(:any)'] = 'admin/kupon/show/$1';
$route['broserviceadmin/kupon/edit/(:any)'] = 'admin/kupon/edit/$1';
$route['broserviceadmin/kupon/update/(:any)'] = 'admin/kupon/update/$1';
$route['broserviceadmin/kupon/destroy/(:any)'] = 'admin/kupon/destroy/$1';

//slider route
$route['broserviceadmin/slider'] = 'admin/slider/index';
$route['broserviceadmin/slider/get'] = 'admin/slider/get/';
$route['broserviceadmin/slider/getdata'] = 'admin/slider/getdata/';
$route['broserviceadmin/slider/getdata/(:any)'] = 'admin/slider/getdata/';
$route['broserviceadmin/slider/create'] = 'admin/slider/create';
$route['broserviceadmin/slider/store'] = 'admin/slider/store';
$route['broserviceadmin/slider/show'] = 'admin/slider/show';
$route['broserviceadmin/slider/edit/(:any)'] = 'admin/slider/edit/$1';
$route['broserviceadmin/slider/update/(:any)'] = 'admin/slider/update/$1';
$route['broserviceadmin/slider/destroy/(:any)'] = 'admin/slider/destroy/$1';

//testimoni route
$route['broserviceadmin/testimoni'] = 'admin/testimoni/index';
$route['broserviceadmin/testimoni/get'] = 'admin/testimoni/get/';
$route['broserviceadmin/testimoni/getdata'] = 'admin/testimoni/getdata/';
$route['broserviceadmin/testimoni/getdata/(:any)'] = 'admin/testimoni/getdata/';
$route['broserviceadmin/testimoni/create'] = 'admin/testimoni/create';
$route['broserviceadmin/testimoni/store'] = 'admin/testimoni/store';
$route['broserviceadmin/testimoni/show/(:any)'] = 'admin/testimoni/show/$1';
$route['broserviceadmin/testimoni/settampilkan/(:any)'] = 'admin/testimoni/settampilkan/$1';
$route['broserviceadmin/testimoni/update/(:any)'] = 'admin/testimoni/update/$1';
$route['broserviceadmin/testimoni/destroy/(:any)'] = 'admin/testimoni/destroy/$1';

//kategori route
$route['broserviceadmin/kategori'] = 'admin/kategori/index';
$route['broserviceadmin/kategori/get'] = 'admin/kategori/get/';
$route['broserviceadmin/kategori/getdata'] = 'admin/kategori/getdata/';
$route['broserviceadmin/kategori/getdata/(:any)'] = 'admin/kategori/getdata/';
$route['broserviceadmin/kategori/create'] = 'admin/kategori/create';
$route['broserviceadmin/kategori/store'] = 'admin/kategori/store';
$route['broserviceadmin/kategori/show'] = 'admin/kategori/show';
$route['broserviceadmin/kategori/edit/(:any)'] = 'admin/kategori/edit/$1';
$route['broserviceadmin/kategori/update/(:any)'] = 'admin/kategori/update/$1';
$route['broserviceadmin/kategori/destroy/(:any)'] = 'admin/kategori/destroy/$1';

//subkategori route
$route['broserviceadmin/subkategori'] = 'admin/subkategori/index';
$route['broserviceadmin/subkategori/get'] = 'admin/subkategori/get/';
$route['broserviceadmin/subkategori/getkategori'] = 'admin/subkategori/getkategori/';
$route['broserviceadmin/subkategori/getdata'] = 'admin/subkategori/getdata/';
$route['broserviceadmin/subkategori/getdata/(:any)'] = 'admin/subkategori/getdata/';
$route['broserviceadmin/subkategori/create'] = 'admin/subkategori/create';
$route['broserviceadmin/subkategori/store'] = 'admin/subkategori/store';
$route['broserviceadmin/subkategori/show'] = 'admin/subkategori/show';
$route['broserviceadmin/subkategori/edit/(:any)'] = 'admin/subkategori/edit/$1';
$route['broserviceadmin/subkategori/update/(:any)'] = 'admin/subkategori/update/$1';
$route['broserviceadmin/subkategori/destroy/(:any)'] = 'admin/subkategori/destroy/$1';


//supersubkategori route
$route['broserviceadmin/supersubkategori'] = 'admin/supersubkategori/index';
$route['broserviceadmin/supersubkategori/get'] = 'admin/supersubkategori/get/';
$route['broserviceadmin/supersubkategori/getkategori'] = 'admin/supersubkategori/getkategori/';
$route['broserviceadmin/supersubkategori/getsubkategori'] = 'admin/supersubkategori/getsubkategori/';
$route['broserviceadmin/supersubkategori/getdata'] = 'admin/supersubkategori/getdata/';
$route['broserviceadmin/supersubkategori/getdata/(:any)'] = 'admin/supersubkategori/getdata/';
$route['broserviceadmin/supersubkategori/create'] = 'admin/supersubkategori/create';
$route['broserviceadmin/supersubkategori/store'] = 'admin/supersubkategori/store';
$route['broserviceadmin/supersubkategori/show'] = 'admin/supersubkategori/show';
$route['broserviceadmin/supersubkategori/edit/(:any)'] = 'admin/supersubkategori/edit/$1';
$route['broserviceadmin/supersubkategori/update/(:any)'] = 'admin/supersubkategori/update/$1';
$route['broserviceadmin/supersubkategori/destroy/(:any)'] = 'admin/supersubkategori/destroy/$1';

//kerusakan route
$route['broserviceadmin/kerusakan'] = 'admin/kerusakan/index';
$route['broserviceadmin/kerusakan/get'] = 'admin/kerusakan/get/';
$route['broserviceadmin/kerusakan/getkategori'] = 'admin/kerusakan/getkategori/';
$route['broserviceadmin/kerusakan/getsubkategori'] = 'admin/kerusakan/getsubkategori/';
$route['broserviceadmin/kerusakan/getsupersubkategori'] = 'admin/kerusakan/getsupersubkategori/';
$route['broserviceadmin/kerusakan/getdata'] = 'admin/kerusakan/getdata/';
$route['broserviceadmin/kerusakan/getdata/(:any)'] = 'admin/kerusakan/getdata/';
$route['broserviceadmin/kerusakan/create'] = 'admin/kerusakan/create';
$route['broserviceadmin/kerusakan/store'] = 'admin/kerusakan/store';
$route['broserviceadmin/kerusakan/show'] = 'admin/kerusakan/show';
$route['broserviceadmin/kerusakan/edit/(:any)'] = 'admin/kerusakan/edit/$1';
$route['broserviceadmin/kerusakan/update/(:any)'] = 'admin/kerusakan/update/$1';
$route['broserviceadmin/kerusakan/destroy/(:any)'] = 'admin/kerusakan/destroy/$1';


//perbaikan route
$route['broserviceadmin/perbaikan'] = 'admin/perbaikan/index';
$route['broserviceadmin/perbaikan/set-status-booking/(:any)'] = 'admin/perbaikan/setstatusbooking/$1';
$route['broserviceadmin/perbaikan/create-tolak-perbaikan/(:any)'] = 'admin/perbaikan/createtolakperbaikan/$1';
$route['broserviceadmin/perbaikan/store-tolak-perbaikan/(:any)'] = 'admin/perbaikan/storetolakperbaikan/$1';
$route['broserviceadmin/perbaikan/set-status-pembayaran/(:any)'] = 'admin/perbaikan/setstatuspembayaran/$1';
$route['broserviceadmin/perbaikan/set-status-perbaikan/(:any)'] = 'admin/perbaikan/setstatusperbaikan/$1';
$route['broserviceadmin/perbaikan/update-status-booking/(:any)'] = 'admin/perbaikan/updatestatusbooking/$1';
$route['broserviceadmin/perbaikan/update-status-pembayaran/(:any)'] = 'admin/perbaikan/updatestatuspembayaran/$1';
$route['broserviceadmin/perbaikan/update-status-perbaikan/(:any)'] = 'admin/perbaikan/updatestatusperbaikan/$1';
$route['broserviceadmin/perbaikan/show/set-status-lokasi-barang/(:any)'] = 'admin/perbaikan/setstatuslokasibarang/$1';
$route['broserviceadmin/perbaikan/show/update-status-lokasi-barang/(:any)'] = 'admin/perbaikan/updatestatuslokasibarang/$1';
$route['broserviceadmin/perbaikan/get'] = 'admin/perbaikan/get/';
$route['broserviceadmin/perbaikan/getkategori'] = 'admin/perbaikan/getkategori/';
$route['broserviceadmin/perbaikan/getsubkategori'] = 'admin/perbaikan/getsubkategori/';
$route['broserviceadmin/perbaikan/getsupersubkategori'] = 'admin/perbaikan/getsupersubkategori/';
$route['broserviceadmin/perbaikan/getdata'] = 'admin/perbaikan/getdata/';
$route['broserviceadmin/perbaikan/getdata/'] = 'admin/perbaikan/getdata/';
$route['broserviceadmin/perbaikan/getdata/dataditerima'] = 'admin/perbaikan/getdataditerima/';
$route['broserviceadmin/perbaikan/getdata/dataselesai'] = 'admin/perbaikan/getdataselesai/';
$route['broserviceadmin/perbaikan/getdata/dataditolak'] = 'admin/perbaikan/getdataditolak/';
$route['broserviceadmin/perbaikan/diterima'] = 'admin/perbaikan/diterima/';
$route['broserviceadmin/perbaikan/ditolak'] = 'admin/perbaikan/ditolak/';
$route['broserviceadmin/perbaikan/selesai'] = 'admin/perbaikan/selesai/';
$route['broserviceadmin/perbaikan/cetak-kwitansi/(:any)'] = 'admin/perbaikan/cetakkwitansi/$1';

$route['broserviceadmin/perbaikan/create'] = 'admin/perbaikan/create';
$route['broserviceadmin/perbaikan/store'] = 'admin/perbaikan/store';
$route['broserviceadmin/perbaikan/show/(:any)'] = 'admin/perbaikan/show/$1';
$route['broserviceadmin/perbaikan/edit/(:any)'] = 'admin/perbaikan/edit/$1';
$route['broserviceadmin/perbaikan/update/(:any)'] = 'admin/perbaikan/update/$1';
$route['broserviceadmin/perbaikan/destroy/(:any)'] = 'admin/perbaikan/destroy/$1';

//klaim-garansi route
$route['broserviceadmin/klaim-garansi'] = 'admin/klaim_garansi/index';
$route['broserviceadmin/klaim-garansi/set-status-klaim-garansi/(:any)'] = 'admin/klaim_garansi/setstatusklaimgaransi/$1';
$route['broserviceadmin/klaim-garansi/set-status-perbaikan/(:any)'] = 'admin/klaim_garansi/setstatusperbaikan/$1';
$route['broserviceadmin/klaim-garansi/update-status-klaim-garansi/(:any)'] = 'admin/klaim_garansi/updatestatusklaimgaransi/$1';
$route['broserviceadmin/klaim-garansi/update-status-perbaikan/(:any)'] = 'admin/klaim_garansi/updatestatusperbaikan/$1';
$route['broserviceadmin/klaim-garansi/show/set-status-lokasi-barang/(:any)'] = 'admin/klaim_garansi/setstatuslokasibarang/$1';
$route['broserviceadmin/klaim-garansi/show/update-status-lokasi-barang/(:any)'] = 'admin/klaim_garansi/updatestatuslokasibarang/$1';
$route['broserviceadmin/klaim-garansi/get'] = 'admin/klaim_garansi/get/';
$route['broserviceadmin/klaim-garansi/getkategori'] = 'admin/klaim_garansi/getkategori/';
$route['broserviceadmin/klaim-garansi/getsubkategori'] = 'admin/klaim_garansi/getsubkategori/';
$route['broserviceadmin/klaim-garansi/getsupersubkategori'] = 'admin/klaim_garansi/getsupersubkategori/';
$route['broserviceadmin/klaim-garansi/getdata'] = 'admin/klaim_garansi/getdata/';
$route['broserviceadmin/klaim-garansi/getdata/'] = 'admin/klaim_garansi/getdata/';
$route['broserviceadmin/klaim-garansi/getdata/dataditerima'] = 'admin/klaim_garansi/getdataditerima/';
$route['broserviceadmin/klaim-garansi/getdata/dataselesai'] = 'admin/klaim_garansi/getdataselesai/';
$route['broserviceadmin/klaim-garansi/diterima'] = 'admin/klaim_garansi/diterima/';
$route['broserviceadmin/klaim-garansi/selesai'] = 'admin/klaim_garansi/selesai/';
$route['broserviceadmin/klaim-garansi/cetak-kwitansi/(:any)'] = 'admin/klaim_garansi/cetakkwitansi/$1';

$route['broserviceadmin/klaim-garansi/create'] = 'admin/klaim_garansi/create';
$route['broserviceadmin/klaim-garansi/store'] = 'admin/klaim_garansi/store';
$route['broserviceadmin/klaim-garansi/show/(:any)'] = 'admin/klaim_garansi/show/$1';
$route['broserviceadmin/klaim-garansi/edit/(:any)'] = 'admin/klaim_garansi/edit/$1';
$route['broserviceadmin/klaim-garansi/update/(:any)'] = 'admin/klaim_garansi/update/$1';
$route['broserviceadmin/klaim-garansi/destroy/(:any)'] = 'admin/klaim_garansi/destroy/$1';


//Config route
$route['broserviceadmin/pengaturan'] = 'admin/pengaturan/index';
$route['broserviceadmin/pengaturan/create'] = 'admin/pengaturan/create';
$route['broserviceadmin/pengaturan/store'] = 'admin/pengaturan/store';
$route['broserviceadmin/pengaturan/show/(:any)'] = 'admin/pengaturan/show/$1';
$route['broserviceadmin/pengaturan/edit/(:any)'] = 'admin/pengaturan/edit/$1';
$route['broserviceadmin/pengaturan/update'] = 'admin/pengaturan/update';
$route['broserviceadmin/pengaturan/destroy/(:any)'] = 'admin/pengaturan/destroy/$1';

//Home route
$route['home'] = 'frontend/home/index';


//Perbaikan route
$route['perbaikan/testimoni/create/(:any)'] = 'frontend/perbaikan/createtestimoni/$1';
$route['perbaikan/testimoni/store/(:any)'] = 'frontend/perbaikan/storetestimoni/$1';
$route['perbaikan/komfirmasi-barang-diterima/(:any)'] = 'frontend/perbaikan/komfirmasibarangditerima/$1';
$route['perbaikan/komfirmasi-barang-diterima/update/(:any)'] = 'frontend/perbaikan/updatekomfirmasibarangditerima/$1';
$route['perbaikan/histori-perbaikan'] = 'frontend/perbaikan/historiperbaikan/';
$route['perbaikan/detail-hadiah/(:any)'] = 'frontend/perbaikan/detailhadiah/$1';
$route['perbaikan/add-kupon'] = 'frontend/perbaikan/addkupon/';
$route['perbaikan/store-kupon'] = 'frontend/perbaikan/storekupon/';
$route['perbaikan/histori-perbaikan'] = 'frontend/perbaikan/historiperbaikan/';
$route['perbaikan/cetak-kwitansi/(:any)'] = 'frontend/perbaikan/cetakkwitansi/$1';
$route['perbaikan/komfirmasi-pembayaran/(:any)'] = 'frontend/perbaikan/createkomfirmasipembayaran/$1';
$route['perbaikan/komfirmasi-pembayaran/store/(:any)'] = 'frontend/perbaikan/storekomfirmasipembayaran/$1';
$route['perbaikan/info-pembayaran/(:any)'] = 'frontend/perbaikan/infopembayaran/$1';
$route['perbaikan/antar-jemput'] = 'frontend/perbaikan/createantarjemput/';
$route['perbaikan/antar-jemput/store'] = 'frontend/perbaikan/storeantarjemput/';
$route['perbaikan/ubah-jadwal-antar-jemput/(:any)'] = 'frontend/perbaikan/editantarjemput/$1';
$route['perbaikan/ubah-jadwal-antar-jemput/update/(:any)'] = 'frontend/perbaikan/updateantarjemput/$1';
$route['perbaikan/kirim-online'] = 'frontend/perbaikan/storekirimonline/';
$route['perbaikan/kirim-online/input-resi/(:any)'] = 'frontend/perbaikan/createresi/$1';
$route['perbaikan/kirim-online/update-resi/(:any)'] = 'frontend/perbaikan/updateresi/$1';
$route['perbaikan/pilihlayanan'] = 'frontend/perbaikan/pilihlayanan/';
$route['perbaikan/tambah-kerusakan/(:any)'] = 'frontend/perbaikan/createkerusakan/$1';
$route['perbaikan/tambah-kerusakan/store/(:any)'] = 'frontend/perbaikan/storekerusakan/$1';
$route['perbaikan/batalkan-perbaikan/(:any)'] = 'frontend/perbaikan/batalkanperbaikan/$1';
$route['perbaikan/batalkan-perbaikan/destroy/(:any)'] = 'frontend/perbaikan/destroy/$1';
$route['perbaikan'] = 'frontend/perbaikan/index/';
$route['perbaikan/(:any)'] = 'frontend/perbaikan/index/';
$route['perbaikan/(:any)/(:any)'] = 'frontend/perbaikan/index/';
$route['perbaikan/(:any)/(:any)/(:any)'] = 'frontend/perbaikan/index/';
$route['perbaikan/(:any)/(:any)/(:any)/(:any)'] = 'frontend/perbaikan/detailkerusakan/';
$route['perbaikan/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'frontend/perbaikan/detailkerusakan/';



//Klaim Garansi route

$route['klaim-garansi/histori-klaim-garansi'] = 'frontend/klaim_garansi/historiklaimgaransi/';
$route['klaim-garansi/komfirmasi-klaim-garansi/(:any)'] = 'frontend/klaim_garansi/komfirmasiklaimgaransi/$1';
$route['klaim-garansi/create/(:any)'] = 'frontend/klaim_garansi/create/$1';
$route['klaim-garansi/antar-jemput/(:any)'] = 'frontend/klaim_garansi/createantarjemput/$1';
$route['klaim-garansi/antar-jemput/store/(:any)'] = 'frontend/klaim_garansi/storeantarjemput/$1';
$route['klaim-garansi/ubah-jadwal-antar-jemput/(:any)'] = 'frontend/klaim_garansi/editantarjemput/$1';
$route['klaim-garansi/ubah-jadwal-antar-jemput/update/(:any)'] = 'frontend/klaim_garansi/updateantarjemput/$1';

// $route['klaim-garansi/kirim-online/(:any)'] = 'frontend/klaim_garansi/createkirimonline/$1';
$route['klaim-garansi/kirim-online/store/(:any)'] = 'frontend/klaim_garansi/storekirimonline/$1';
$route['klaim-garansi/kirim-online/input-resi/(:any)'] = 'frontend/klaim_garansi/createresi/$1';
$route['klaim-garansi/kirim-online/update-resi/(:any)'] = 'frontend/klaim_garansi/updateresi/$1';
$route['klaim-garansi/komfirmasi-barang-diterima/(:any)'] = 'frontend/klaim_garansi/komfirmasibarangditerima/$1';
$route['klaim-garansi/komfirmasi-barang-diterima/update/(:any)'] = 'frontend/klaim_garansi/updatekomfirmasibarangditerima/$1';
$route['klaim-garansi/batalkan-klaim-garansi/(:any)'] = 'frontend/klaim_garansi/batalkanklaimgaransi/$1';
$route['klaim-garansi/batalkan-klaim-garansi/destroy/(:any)'] = 'frontend/klaim_garansi/destroy/$1';





$route['default_controller'] = 'welcome';
// $route['404_override'] = '';
// $route['translate_uri_dashes'] = FALSE;



