<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengaturan_model extends CI_Model {
	
	

	function get($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('pengaturan');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
		
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('pengaturan');
		
	}
	
	function getdata() {
		
		$this->load->library('datatables');
       	$this->datatables->select('id,gambar,nama as nama_pengaturan, min_transaksi');
		
		$this->datatables->add_column("action"
		,'

		<a href="show/$1" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
		<a href="edit/$1" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
		<a href="destroy/$1" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>'
		
		, 'id');
		
        $this->datatables->from('pengaturan');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function store($data) {
		
		$this->db->insert('pengaturan',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}
		
	function destroy($where) {
		
		$this->db->where($where);
		$this->db->delete('pengaturan');
		
		return;
		
	}
	
	
	
}