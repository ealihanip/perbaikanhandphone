<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kategori_model extends CI_Model {
	
	

	function get($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('kategori');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
		
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('kategori');
		
	}
	
	function getdata() {
		
		$this->load->library('datatables');
       	$this->datatables->select('id,gambar,nama as nama_kategori,if(kategori.tampilkan=true,"Ya","Tidak") as tampilkan_kategori');
		$this->datatables->add_column("gambar_kategori"
		,'
		
		<a href="'.base_url().'assets/uploads/kategori/$1" data-lightbox="image-1" data-title="My caption">
		Klik untuk melihat</a>'
		
		, 'gambar');
		$this->datatables->add_column("action"
		,'
		
		<a href="edit/$1" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
		<a href="destroy/$1" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>'
		
		, 'id');
		
        $this->datatables->from('kategori');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function store($data) {
		
		$this->db->insert('kategori',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}
		
	function destroy($where) {
		
		$this->db->where($where);
		$this->db->delete('kategori');
		
		return;
		
	}
	
	
	
}