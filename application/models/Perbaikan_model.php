<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Perbaikan_model extends CI_Model {
	
	

	function getdata() {
			
		$this->load->library('datatables');
		
		$this->datatables->select("
			perbaikan.id,
			users.first_name,
			users.phone,
			perbaikan.kode,
			DATE_FORMAT(perbaikan.tanggal_booking,'%d-%m-%Y') as tanggal_booking,
			supersubkategori.nama as nama_model,
			subkategori.nama as nama_brand,
			kategori.nama as nama_jenis,
			if(perbaikan.status_pembayaran=false,'Belum Di Bayar','Sudah Lunas') as nama_status_pembayaran,
			if(perbaikan.status_booking=false,'Belum Diterima','Sudah Diterima') as nama_status_booking,
			if(perbaikan.status_perbaikan=false,'Belum Selesai','Sudah Selesai') as nama_status_perbaikan,
			if(perbaikan.jenis_layanan=false,'Antar Jemput','Kirim Online') as nama_jenis_layanan,
			(
				select format(sum(kerusakan.harga-((kerusakan.harga*daftar_kerusakan.diskon)/100)),0) from daftar_kerusakan join kerusakan on daftar_kerusakan.kerusakan_id=kerusakan.id 
			where perbaikan_id=perbaikan.id
			
			) as total_harga  
		
		
		");
		$this->datatables->add_column("action"
		,'
		
		<a href="show/$1" class="btn btn-primary btn-sm"><i class="fa fa-edit">detail</i></a>
		<a href="cetak-kwitansi/$1" class="btn btn-primary btn-sm"><i class="fa fa-edit">Cetak Kwitansi</i></a>
		<a href="set-status-booking/$1" class="btn btn-warning btn-sm"><i class="fa fa-edit">Set Status Booking</i></a>
		<a href="set-status-perbaikan/$1" class="btn btn-warning btn-sm"><i class="fa fa-edit">Set Status Perbaikan</i></a>
		<a href="set-status-pembayaran/$1" class="btn btn-warning btn-sm"><i class="fa fa-edit">Set Status Pembayaran</i></a>
		<a href="create-tolak-perbaikan/$1" class="btn btn-danger btn-sm"><i class="fa fa-edit">Tolak Perbaikan</i></a>
		
		'
		
		, 'kode');
		
		$this->datatables->join('users', 'users.id = perbaikan.users_id');
		$this->datatables->join('supersubkategori', 'supersubkategori.id = perbaikan.supersubkategori_id');
		$this->datatables->join('subkategori', 'subkategori.id = supersubkategori.subkategori_id');
		$this->datatables->join('kategori', 'kategori.id = subkategori.kategori_id');
		
		$this->datatables->from('perbaikan');

		$this->datatables->where('perbaikan.status_transaksi != "8"');
		
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}

	function getdataditerima() {
		
		$where=array('perbaikan.status_booking'=>true);
		
		
		$this->load->library('datatables');
		
		$this->datatables->select("
			perbaikan.id,
			users.first_name,
			users.phone,
			perbaikan.kode,
			DATE_FORMAT(perbaikan.tanggal_booking,'%d-%m-%Y') as tanggal_booking,
			supersubkategori.nama as nama_model,
			subkategori.nama as nama_brand,
			kategori.nama as nama_jenis,
			if(perbaikan.status_pembayaran=false,'Belum Di Bayar','Sudah Lunas') as nama_status_pembayaran,
			if(perbaikan.status_booking=false,'Belum Diterima','Sudah Diterima') as nama_status_booking,
			if(perbaikan.status_perbaikan=false,'Belum Selesai','Sudah Selesai') as nama_status_perbaikan,
			if(perbaikan.jenis_layanan=false,'Antar Jemput','Kirim Online') as nama_jenis_layanan,
			(
				select format(sum(kerusakan.harga-((kerusakan.harga*daftar_kerusakan.diskon)/100)),0) from daftar_kerusakan join kerusakan on daftar_kerusakan.kerusakan_id=kerusakan.id 
			where perbaikan_id=perbaikan.id
			
			) as total_harga  
		
		
		");
		
		$this->datatables->join('users', 'users.id = perbaikan.users_id');
		$this->datatables->join('supersubkategori', 'supersubkategori.id = perbaikan.supersubkategori_id');
		$this->datatables->join('subkategori', 'subkategori.id = supersubkategori.subkategori_id');
		$this->datatables->join('kategori', 'kategori.id = subkategori.kategori_id');
		$this->datatables->from('perbaikan');
		$this->datatables->where($where);
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}

	function getdataditolak() {
		
		$where=array('perbaikan.status_transaksi'=>8);
		
		$this->load->library('datatables');
		
		$this->datatables->select("
			perbaikan.id,
			users.first_name,
			users.phone,
			perbaikan.kode,
			DATE_FORMAT(perbaikan.tanggal_booking,'%d-%m-%Y') as tanggal_booking,
			supersubkategori.nama as nama_model,
			subkategori.nama as nama_brand,
			kategori.nama as nama_jenis,
			if(perbaikan.status_pembayaran=false,'Belum Di Bayar','Sudah Lunas') as nama_status_pembayaran,
			if(perbaikan.status_booking=false,'Belum Diterima','Sudah Diterima') as nama_status_booking,
			if(perbaikan.status_perbaikan=false,'Belum Selesai','Sudah Selesai') as nama_status_perbaikan,
			if(perbaikan.jenis_layanan=false,'Antar Jemput','Kirim Online') as nama_jenis_layanan,
			(
				select format(sum(kerusakan.harga-((kerusakan.harga*daftar_kerusakan.diskon)/100)),0) from daftar_kerusakan join kerusakan on daftar_kerusakan.kerusakan_id=kerusakan.id 
			where perbaikan_id=perbaikan.id
			
			) as total_harga  
		
		
		");
		
		$this->datatables->join('users', 'users.id = perbaikan.users_id');
		$this->datatables->join('supersubkategori', 'supersubkategori.id = perbaikan.supersubkategori_id');
		$this->datatables->join('subkategori', 'subkategori.id = supersubkategori.subkategori_id');
		$this->datatables->join('kategori', 'kategori.id = subkategori.kategori_id');
		$this->datatables->from('perbaikan');
		$this->datatables->where($where);
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}

	function getdataselesai() {
		
		$where=array('perbaikan.status_perbaikan'=>true);
		
		$this->load->library('datatables');
		
		$this->datatables->select("
			perbaikan.id,
			users.first_name,
			users.phone,
			perbaikan.kode,
			DATE_FORMAT(perbaikan.tanggal_booking,'%d-%m-%Y') as tanggal_booking,
			supersubkategori.nama as nama_model,
			subkategori.nama as nama_brand,
			kategori.nama as nama_jenis,
			if(perbaikan.status_pembayaran=false,'Belum Di Bayar','Sudah Lunas') as nama_status_pembayaran,
			if(perbaikan.status_booking=false,'Belum Diterima','Sudah Diterima') as nama_status_booking,
			if(perbaikan.status_perbaikan=false,'Belum Selesai','Sudah Selesai') as nama_status_perbaikan,
			if(perbaikan.jenis_layanan=false,'Antar Jemput','Kirim Online') as nama_jenis_layanan,
			(
				select format(sum(kerusakan.harga-((kerusakan.harga*daftar_kerusakan.diskon)/100)),0) from daftar_kerusakan join kerusakan on daftar_kerusakan.kerusakan_id=kerusakan.id 
			where perbaikan_id=perbaikan.id
			
			) as total_harga  
		
		
		");
		
		$this->datatables->join('users', 'users.id = perbaikan.users_id');
		$this->datatables->join('supersubkategori', 'supersubkategori.id = perbaikan.supersubkategori_id');
		$this->datatables->join('subkategori', 'subkategori.id = supersubkategori.subkategori_id');
		$this->datatables->join('kategori', 'kategori.id = subkategori.kategori_id');
		$this->datatables->from('perbaikan');
		$this->datatables->where($where);
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}

	function storeperbaikan($data) {
		
		$this->db->insert('perbaikan',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}

	function storedaftarkerusakan($data) {
		
		$this->db->insert('daftar_kerusakan',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}

	function storeantarjemput($data) {
		
		$this->db->insert('antar_jemput',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}

	function storekirimonline($data) {
		
		$this->db->insert('kirim_online',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}
	
	function get($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		$this->db->select("
			perbaikan.*,
			DATE_FORMAT(perbaikan.tanggal_booking,'%d-%m-%Y') as tanggal_booking,
			DATE_FORMAT(perbaikan.tanggal_selesai_perbaikan,'%d-%m-%Y') as tanggal_selesai_perbaikan,
			supersubkategori.nama as nama_model,
			subkategori.nama as nama_brand,
			kategori.nama as nama_jenis,
			if(perbaikan.jenis_layanan=false,'Antar Jemput','Kirim Online') as nama_jenis_layanan,
			if(perbaikan.status_pembayaran=false,'Belum Di Bayar','Lunas') as nama_status_pembayaran,
			if(perbaikan.status_booking=false,'Belum Diterima','Diterima') as nama_status_booking,
			if(perbaikan.status_perbaikan=false,'Belum Selesai','Selesai') as nama_status_perbaikan,
			(
			select format(sum(kerusakan.harga-((kerusakan.harga*daftar_kerusakan.diskon)/100)),0) from daftar_kerusakan join kerusakan on daftar_kerusakan.kerusakan_id=kerusakan.id 
			where perbaikan_id=perbaikan.id
			
			) as total_harga
			
		");
		
		$this->db->join('supersubkategori', 'supersubkategori.id = perbaikan.supersubkategori_id');
		$this->db->join('subkategori', 'subkategori.id = supersubkategori.subkategori_id');
		$this->db->join('kategori', 'kategori.id = subkategori.kategori_id');
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('perbaikan');
		
		return $query;
		$query->free_result();
		
	}

	function getuser($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('users');
		
		
		return $query;
		$query->free_result();
		
	}

	function getkupon($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('kupon');
		
		
		return $query;
		$query->free_result();
		
	}
	
	function getdaftarkerusakan($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		$this->db->select('daftar_kerusakan.id,kerusakan.nama as nama_kerusakan, kerusakan.harga,kerusakan.diskon');
		$this->db->join('kerusakan', 'kerusakan.id = daftar_kerusakan.kerusakan_id');
		$query = $this->db->get('daftar_kerusakan');
		
		
		return $query;
		$query->free_result();
		
	}

	function getstatuslokasibarang($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('status_lokasi_barang');
		
		
		return $query;
		$query->free_result();
		
	}

	function getsubkategori($where=""){
		
		if($where){
			
			$this->db->where($where);
		}
		
		$this->db->select('
			subkategori.*
			
		');
		
		$this->db->join('kategori', 'kategori.id = subkategori.kategori_id');
		$this->db->where($where);
		$this->db->from('subkategori');
		$query = $this->db->get();
		
		
		return $query;
		$query->free_result();
		
	}

	function getsupersubkategori($where=''){

		if($where){
			
			$this->db->where($where);
		}
		
		$this->db->select('
			
			supersubkategori.*
			
		');
		
		$this->db->join('subkategori', 'subkategori.id = supersubkategori.subkategori_id');
		
		$this->db->from('supersubkategori');
		$query = $this->db->get();
		
		
		return $query;
		$query->free_result();
		
	}

	function getkerusakan($where=''){

		if($where){
			
			$this->db->where($where);
		}

		
		$this->db->select('
			kerusakan.*
			
		');
		
		$this->db->join('supersubkategori', 'supersubkategori.id = kerusakan.supersubkategori_id');
		
		$this->db->from('kerusakan');
		$query = $this->db->get();
		
		
		return $query;
		$query->free_result();
		
	}

	function getmaxkodeperbaikan($like){

		
		$this->db->select_max('kode');
		$this->db->like($like);
		$query = $this->db->get('perbaikan');

		return $query;
	}
	
	function getantarjemput($where){
	
		$this->db->select('
			antar_jemput.*,
			DATE_FORMAT(antar_jemput.tanggal,"%d-%m-%Y") as tanggal,
			status_lokasi_barang.nama as nama_status
			
		');
		$this->db->where($where);
		$this->db->join('status_lokasi_barang', 'status_lokasi_barang.id = antar_jemput.status');
		$this->db->from('antar_jemput');
		$query = $this->db->get();
		
		
		return $query;
		$query->free_result();
		
	}

	function getkirimonline($where){
		
		$this->db->select('
			kirim_online.*,

			status_lokasi_barang.nama as nama_status
			
		');
		$this->db->where($where);
		$this->db->from('kirim_online');
		$this->db->join('status_lokasi_barang', 'status_lokasi_barang.id = kirim_online.status');
		$query = $this->db->get();
		
		
		return $query;
		$query->free_result();
		
	}
	
	public function updatestatus($where,$data){

		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('perbaikan'); // gives UPDATE mytable SET field = field+1 WHERE id = 2

	}

	public function updatestatuslokasibarangantarjemput($where,$data){

		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('antar_jemput'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}
	public function updatestatuslokasibarangkirimonline($where,$data){

		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('kirim_online'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}

	public function updatekirimonline($where,$data){

		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('kirim_online'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	
		return true;
	}

	public function updateantarjemput($where,$data){

		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('antar_jemput'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	
		return true;
	}

	function storekomfirmasipembayaran($data) {
		
		$this->db->insert('komfirmasi_pembayaran',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}

	function updatekomfirmasipembayaran($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('komfirmasi_pembayaran'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	
		return true;
		
	}

	function getkomfirmasipembayaran($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('komfirmasi_pembayaran');
		
		
		return $query;
		$query->free_result();
		
	}


	function storetestimoni($data) {
		
		$this->db->insert('testimoni',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}

	function destroy($where) {
		
		$this->db->where($where);
		$this->db->delete('perbaikan');
		
		return;
		
	}
	
	
}