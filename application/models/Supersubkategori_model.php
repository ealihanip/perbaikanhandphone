<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Supersubkategori_model extends CI_Model {
	
	

	function get($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('supersubkategori');
		
		
		return $query;
		$query->free_result();
		
	}
	

	function getkategorijoin($where){
		
		$this->db->select('
			supersubkategori.*,
			kategori.id as kategori_id
			
		
		');

		$this->db->join('subkategori', 'subkategori.id = supersubkategori.subkategori_id');
		$this->db->join('kategori', 'kategori.id = subkategori.kategori_id');
		$this->db->where($where);
		$this->db->from('supersubkategori');
		$query = $this->db->get();
		
		
		return $query;
		$query->free_result();
		
	}
	
	
		
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('supersubkategori');
		
	}
	
	function getdata() {
		
		$this->load->library('datatables');
		$this->datatables->select('
			subkategori.id as subkategori_id,
			subkategori.nama as nama_subkategori,
		   	kategori.id as kategori_id,
			kategori.nama as nama_kategori,
		   	supersubkategori.id,
		   	supersubkategori.gambar,
		   	supersubkategori.nama as nama_supersubkategori,
		   	if(supersubkategori.
		  	 tampilkan=true,"Ya","Tidak") as tampilkan_supersubkategori'
		
		);
		$this->datatables->add_column("gambar_supersubkategori"
		,'
		
		<a href="'.base_url().'assets/uploads/supersubkategori/$1" data-lightbox="image-1" data-title="My caption">
		Klik untuk melihat</a>'
		
		, 'gambar');
		$this->datatables->add_column("action"
		,'
		
		<a href="edit/$1" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
		<a href="destroy/$1" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>'
		
		, 'id');
		$this->datatables->join('subkategori', 'subkategori.id = supersubkategori.subkategori_id');
		$this->datatables->join('kategori', 'kategori.id = subkategori.kategori_id');
        $this->datatables->from('supersubkategori');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function store($data) {
		
		$this->db->insert('supersubkategori',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}
		
	function destroy($where) {
		
		$this->db->where($where);
		$this->db->delete('supersubkategori');
		
		return;
		
	}
	
	
	
}