<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Slider_model extends CI_Model {
	
	

	function get($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('slider');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
		
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('slider');
		
	}
	
	function getdata() {
		
		$this->load->library('datatables');
       	$this->datatables->select('id,gambar');
		$this->datatables->add_column("gambar_slider"
		,'
		
		<a href="'.base_url().'assets/uploads/slider/$1" data-lightbox="image-1" data-title="My caption">
		Klik untuk melihat</a>'
		
		, 'gambar');
		$this->datatables->add_column("action"
		,'
		
		
		<a href="destroy/$1" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>'
		
		, 'id');
		
        $this->datatables->from('slider');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function store($data) {
		
		$this->db->insert('slider',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}
		
	function destroy($where) {
		
		$this->db->where($where);
		$this->db->delete('slider');
		
		return;
		
	}
	
	
	
}