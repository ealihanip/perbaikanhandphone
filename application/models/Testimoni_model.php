<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Testimoni_model extends CI_Model {
	
	

	function get($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		$this->db->select('
		testimoni.id as testimoni_id,
		DATE_FORMAT(testimoni.tanggal,"%d-%m-%Y") as tanggal_testimoni,
		testimoni.tampilkan,
		testimoni.testimoni,
		users.first_name,
		users.phone,perbaikan.kode as kode_perbaikan,perbaikan_id,
		if(testimoni.tampilkan=true,"Ya","Tidak") as tampilkan_testimoni,
		supersubkategori.nama as nama_model,
		subkategori.nama as nama_brand,
		kategori.nama as nama_jenis'
		
		);
		$this->db->join('perbaikan', 'perbaikan.id = testimoni.perbaikan_id');
		$this->db->join('users', 'users.id = perbaikan.users_id');
		$this->db->join('supersubkategori', 'supersubkategori.id = perbaikan.supersubkategori_id');
		$this->db->join('subkategori', 'subkategori.id = supersubkategori.subkategori_id');
		$this->db->join('kategori', 'kategori.id = subkategori.kategori_id');
		$query = $this->db->get('testimoni');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
		
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('testimoni');
		
	}
	
	function getdata() {
		
		$this->load->library('datatables');
       	$this->datatables->select('testimoni.id as testimoni_id,testimoni.tanggal as tanggal_testimoni, users.first_name,users.phone,perbaikan.kode as kode_perbaikan,perbaikan_id,testimoni,if(testimoni.tampilkan=true,"Ya","Tidak") as tampilkan_testimoni');
		
		$this->datatables->add_column("action"
		,'
		
		<a href="show/$1" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i>Lihat Testimoni</a>
		<a href="settampilkan/$1" class="btn btn-warning btn-sm"><i class="fa fa-eye"></i>Set Tampilkan</a>
		
		
		', 'testimoni_id');
		
		$this->datatables->join('perbaikan', 'perbaikan.id = testimoni.perbaikan_id');
		$this->datatables->join('users', 'users.id = perbaikan.users_id');
        $this->datatables->from('testimoni');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function store($data) {
		
		$this->db->insert('testimoni',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}
		
	function destroy($where) {
		
		$this->db->where($where);
		$this->db->delete('testimoni');
		
		return;
		
	}
	
	
	
}