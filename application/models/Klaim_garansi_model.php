<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Klaim_garansi_model extends CI_Model {
	
	

	function getdata() {
			
		$this->load->library('datatables');
		
		$this->datatables->select("
			klaim_garansi.*,
			klaim_garansi.kode as kode_klaim_garansi,
			perbaikan.kode as kode_perbaikan,
			DATE_FORMAT(klaim_garansi.tanggal_klaim_garansi,'%d-%m-%Y') as tanggal_klaim_garansi,
			
			if(klaim_garansi.jenis_layanan=false,'Antar Jemput','Kirim Online') as nama_jenis_layanan,
			if(klaim_garansi.status_klaim_garansi=false,'Belum Diterima','Sudah Diterima') as nama_status_klaim_garansi,
			if(klaim_garansi.status_perbaikan=false,'Belum Selesai','Sudah Selesai') as nama_status_perbaikan,
			
			
		
		
		");
		$this->datatables->join('perbaikan', 'perbaikan.id = klaim_garansi.perbaikan_id');
		$this->datatables->add_column("action"
		,'
		
		<a href="show/$1" class="btn btn-primary btn-sm"><i class="fa fa-edit">detail</i></a>
		
		
		<a href="set-status-klaim-garansi/$1" class="btn btn-warning btn-sm"><i class="fa fa-edit">Set Status Klaim Garansi</i></a>
		<a href="set-status-perbaikan/$1" class="btn btn-warning btn-sm"><i class="fa fa-edit">Set Status Perbaikan</i></a>
		
		'
		
		, 'kode_klaim_garansi');
		
		
		$this->datatables->from('klaim_garansi');

        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}

	function getdataditerima() {
		
		$where=array('klaim_garansi.status_booking'=>true);
		
		$this->load->library('datatables');
		
		$this->datatables->select("
			klaim_garansi.id,
			users.first_name,
			users.phone,
			klaim_garansi.kode,
			DATE_FORMAT(klaim_garansi.tanggal_booking,'%d-%m-%Y') as tanggal_booking,
			
			supersubkategori.nama as nama_model,
			subkategori.nama as nama_brand,
			kategori.nama as nama_jenis,
			if(klaim_garansi.status_pembayaran=false,'Belum Di Bayar','Sudah Lunas') as nama_status_pembayaran,
			if(klaim_garansi.status_booking=false,'Belum Diterima','Sudah Diterima') as nama_status_booking,
			if(klaim_garansi.status_klaim_garansi=false,'Belum Selesai','Sudah Selesai') as nama_status_klaim_garansi,
			if(klaim_garansi.jenis_layanan=false,'Antar Jemput','Kirim Online') as nama_jenis_layanan,
			(
				select format(sum(kerusakan.harga-((kerusakan.harga*daftar_kerusakan.diskon)/100)),0) from daftar_kerusakan join kerusakan on daftar_kerusakan.kerusakan_id=kerusakan.id 
			where klaim_garansi_id=klaim_garansi.id
			
			) as total_harga  
		
		
		");
		
		$this->datatables->join('users', 'users.id = klaim_garansi.users_id');
		$this->datatables->join('supersubkategori', 'supersubkategori.id = klaim_garansi.supersubkategori_id');
		$this->datatables->join('subkategori', 'subkategori.id = supersubkategori.subkategori_id');
		$this->datatables->join('kategori', 'kategori.id = subkategori.kategori_id');
		$this->datatables->from('klaim_garansi');
		$this->datatables->where($where);
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}

	function getdataselesai() {
		
		$where=array('klaim_garansi.status_klaim_garansi'=>true);
		
		$this->load->library('datatables');
		
		$this->datatables->select("
			klaim_garansi.id,
			users.first_name,
			users.phone,
			klaim_garansi.kode,
			DATE_FORMAT(klaim_garansi.tanggal_booking,'%d-%m-%Y') as tanggal_booking,
			supersubkategori.nama as nama_model,
			subkategori.nama as nama_brand,
			kategori.nama as nama_jenis,
			if(klaim_garansi.status_pembayaran=false,'Belum Di Bayar','Sudah Lunas') as nama_status_pembayaran,
			if(klaim_garansi.status_booking=false,'Belum Diterima','Sudah Diterima') as nama_status_booking,
			if(klaim_garansi.status_klaim_garansi=false,'Belum Selesai','Sudah Selesai') as nama_status_klaim_garansi,
			if(klaim_garansi.jenis_layanan=false,'Antar Jemput','Kirim Online') as nama_jenis_layanan,
			(
				select format(sum(kerusakan.harga-((kerusakan.harga*daftar_kerusakan.diskon)/100)),0) from daftar_kerusakan join kerusakan on daftar_kerusakan.kerusakan_id=kerusakan.id 
			where klaim_garansi_id=klaim_garansi.id
			
			) as total_harga  
		
		
		");
		
		$this->datatables->join('users', 'users.id = klaim_garansi.users_id');
		$this->datatables->join('supersubkategori', 'supersubkategori.id = klaim_garansi.supersubkategori_id');
		$this->datatables->join('subkategori', 'subkategori.id = supersubkategori.subkategori_id');
		$this->datatables->join('kategori', 'kategori.id = subkategori.kategori_id');
		$this->datatables->from('klaim_garansi');
		$this->datatables->where($where);
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}

	function storeklaimgaransi($data) {
		
		$this->db->insert('klaim_garansi',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}

	function storedaftarkerusakan($data) {
		
		$this->db->insert('daftar_kerusakan',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}

	function storeantarjemput($data) {
		
		$this->db->insert('kg_antar_jemput',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}

	function storekirimonline($data) {
		
		$this->db->insert('kg_kirim_online',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}
	
	function get($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		$this->db->select("
			klaim_garansi.*,
			klaim_garansi.kode as kode_klaim_garansi,
			perbaikan.kode as kode_perbaikan,
			DATE_FORMAT(klaim_garansi.tanggal_klaim_garansi,'%d-%m-%Y') as tanggal_klaim_garansi,
			DATE_FORMAT(klaim_garansi.tanggal_selesai_perbaikan,'%d-%m-%Y') as tanggal_selesai_perbaikan,
			if(klaim_garansi.jenis_layanan=false,'Antar Jemput','Kirim Online') as nama_jenis_layanan,
		
			if(klaim_garansi.status_klaim_garansi=false,'Belum Diterima','Diterima') as nama_status_klaim_garansi,
			if(klaim_garansi.status_perbaikan=false,'Belum Selesai','Selesai') as nama_status_perbaikan,
		");

		
		
		// $this->db->join('kategori', 'kategori.id = subkategori.kategori_id');
		$this->db->join('perbaikan', 'perbaikan.id = klaim_garansi.perbaikan_id');
		
		$query = $this->db->get('klaim_garansi');
		
		return $query;
		$query->free_result();
		
	}

	function getuser($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('users');
		
		
		return $query;
		$query->free_result();
		
	}

	function getkupon($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('kupon');
		
		
		return $query;
		$query->free_result();
		
	}
	
	function getdaftarkerusakan($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		$this->db->select('daftar_kerusakan.id,kerusakan.nama as nama_kerusakan, kerusakan.harga,kerusakan.diskon');
		$this->db->join('kerusakan', 'kerusakan.id = daftar_kerusakan.kerusakan_id');
		$query = $this->db->get('daftar_kerusakan');
		
		
		return $query;
		$query->free_result();
		
	}

	function getstatuslokasibarang($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('status_lokasi_barang');
		
		
		return $query;
		$query->free_result();
		
	}

	function getsubkategori($where=""){
		
		if($where){
			
			$this->db->where($where);
		}
		
		$this->db->select('
			subkategori.*
			
		');
		
		$this->db->join('kategori', 'kategori.id = subkategori.kategori_id');
		$this->db->where($where);
		$this->db->from('subkategori');
		$query = $this->db->get();
		
		
		return $query;
		$query->free_result();
		
	}

	function getsupersubkategori($where=''){

		if($where){
			
			$this->db->where($where);
		}
		
		$this->db->select('
			
			supersubkategori.*
			
		');
		
		$this->db->join('subkategori', 'subkategori.id = supersubkategori.subkategori_id');
		
		$this->db->from('supersubkategori');
		$query = $this->db->get();
		
		
		return $query;
		$query->free_result();
		
	}

	function getkerusakan($where=''){

		if($where){
			
			$this->db->where($where);
		}

		
		$this->db->select('
			kerusakan.*
			
		');
		
		$this->db->join('supersubkategori', 'supersubkategori.id = kerusakan.supersubkategori_id');
		
		$this->db->from('kerusakan');
		$query = $this->db->get();
		
		
		return $query;
		$query->free_result();
		
	}

	function getmaxkodeklaimgaransi($like){

		
		$this->db->select_max('kode');
		$this->db->like($like);
		$query = $this->db->get('klaim_garansi');

		return $query;
	}
	
	function getantarjemput($where){
	
		$this->db->select('
			kg_antar_jemput.*,
			DATE_FORMAT(kg_antar_jemput.tanggal,"%d-%m-%Y") as tanggal,
			status_lokasi_barang.nama as nama_status
			
		');
		$this->db->where($where);
		$this->db->from('kg_antar_jemput');
		$this->db->join('status_lokasi_barang', 'status_lokasi_barang.id = kg_antar_jemput.status');
		$query = $this->db->get();
		
		
		return $query;
		$query->free_result();
		
	}

	function getkirimonline($where){
		
		$this->db->select('
			kg_kirim_online.*,
			status_lokasi_barang.nama as nama_status
			
		');
		$this->db->where($where);
		$this->db->from('kg_kirim_online');
		$this->db->join('status_lokasi_barang', 'status_lokasi_barang.id = kg_kirim_online.status');
		$query = $this->db->get();
		
		
		return $query;
		$query->free_result();
		
	}
	
	public function updatestatus($where,$data){

		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('klaim_garansi'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}
	
	public function updatestatuslokasibarangkirimonline($where,$data){

		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('kg_kirim_online'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}

	public function updatestatuslokasibarangantarjemput($where,$data){

		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('kg_antar_jemput'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}

	public function updatekirimonline($where,$data){

		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('kg_kirim_online'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	
		return true;
	}

	public function updateantarjemput($where,$data){

		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('kg_antar_jemput'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	
		return true;
	}

	function storekomfirmasipembayaran($data) {
		
		$this->db->insert('komfirmasi_pembayaran',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}

	function updatekomfirmasipembayaran($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('komfirmasi_pembayaran'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	
		return true;
		
	}

	function getkomfirmasipembayaran($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('komfirmasi_pembayaran');
		
		
		return $query;
		$query->free_result();
		
	}

	function destroy($where) {
		
		$this->db->where($where);
		$this->db->delete('klaim_garansi');
		
		return;
		
	}
	
	
}