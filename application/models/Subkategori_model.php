<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Subkategori_model extends CI_Model {
	
	

	function get($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('subkategori');
		
		
		return $query;
		$query->free_result();
		
	}
	

	function getsubkategori() {
		
		
		$query = $this->db->get('subkategori');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
		
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('subkategori');
		
	}
	
	function getdata() {
		
		$this->load->library('datatables');
		$this->datatables->select('
		   	kategori.id as kategori_id,
			kategori.nama as nama_kategori,
		   	subkategori.id,
		   	subkategori.gambar,
		   	subkategori.nama as nama_subkategori,
		   	if(subkategori.
		  	 tampilkan=true,"Ya","Tidak") as tampilkan_subkategori'
		
		);
		$this->datatables->add_column("gambar_subkategori"
		,'
		
		<a href="'.base_url().'assets/uploads/subkategori/$1" data-lightbox="image-1" data-title="My caption">
		Klik untuk melihat</a>'
		
		, 'gambar');
		$this->datatables->add_column("action"
		,'
		
		<a href="edit/$1" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
		<a href="destroy/$1" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>'
		
		, 'id');
		$this->datatables->join('kategori', 'kategori.id = subkategori.kategori_id');
        $this->datatables->from('subkategori');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function store($data) {
		
		$this->db->insert('subkategori',$data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;
		
	}
		
	function destroy($where) {
		
		$this->db->where($where);
		$this->db->delete('subkategori');
		
		return;
		
	}
	
	
	
}