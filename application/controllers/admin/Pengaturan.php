<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
	{
		parent::__construct();

		$this->load->library('ion_auth');

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$this->load->model('pengaturan_model');

		$newdata = array(
			'menu'  => 'Pengaturan'
		);
		
		$this->session->set_userdata($newdata);
	}

	
	
	public function index()
	{


		$data['data']=$this->pengaturan_model->get()->result_array();
		
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/pengaturan/index',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
	}

	public function update()
	{

		

		$this->load->library('form_validation');
		$this->form_validation->set_rules('durasiakhirgarasi', 'Akhir Garansi', 'required|numeric', 'Akhir Garansi Salah');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			
			$data['status']=array(

				'status'=>'error',
				'message'=>'Kesalahan input',

			);

		}
		else
		{

			
			

			//update durasi akhir garansi
			$where=array(

				'id'=>1
	
			);
			
			$update=array(

				'nilai'=>$this->input->post('durasiakhirgarasi')

			);
			

			
			$update=$this->pengaturan_model->update($where,$update);
			//end update durasi akhir garansi
			
			$data['status']=array(

				'status'=>'success',
				'message'=>'Data Berhasil Di Update',

			);
		}

		$data['data']=$this->pengaturan_model->get()->result_array();
		
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/pengaturan/index',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
		
		
	}


	

}
