<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
	{
		parent::__construct();

		$this->load->library('ion_auth');

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$this->load->model('testimoni_model');

		$newdata = array(
			'menu'  => 'Testimoni'
		);
		
		$this->session->set_userdata($newdata);
	}

	public function index()
	{
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/testimoni/index');
		$this->load->view('admin/testimoni/datatable');
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
	}

	public function create()
	{
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/testimoni/create');
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
	}

	public function get()
	{

		$data['data']=$this->testimoni_model->get()->result_array();
		echo json_encode($data);
	}

	public function getdata()
	{
		echo $this->testimoni_model->getdata();
	}

	public function settampilkan($id)
	{	

		$where = array(

			'testimoni.id' => $id
			
		);
		
		$data['data']=$this->testimoni_model->get($where)->result_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/testimoni/settampilkan',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');

		

	}
	
	public function show($id)
	{	

		$where = array(

			'testimoni.id' => $id
			
		);
		
		$data['data']=$this->testimoni_model->get($where)->row_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/testimoni/show',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');

		

	}

	public function update($id)
	{

		$data['status']=array(

			'status'=>'success',
			'message'=>'Data berhasil disimpan'

		);

		//memberi nilai true or false
		if($this->input->post('tampilkan')=='true'){

			$tampilkan=true;

		}else{
			$tampilkan=false;
		}

		$where=array(

			'testimoni.id'=>$id

		);

		$update=array(

			'tampilkan'=>$tampilkan
		);

		$data['data']=$this->testimoni_model->update($where,$update);
		$data['data']=$this->testimoni_model->get($where)->result_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/testimoni/settampilkan',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
		
	}
	

	public function destroy($id)
	{
		$where = array(

			'id' => $id
			
		);
		
		$data=$this->testimoni_model->get($where)->result_array();
		

		$path='assets/uploads/testimoni/';

		foreach($data as $value){
			$nama_gambar=$value['gambar'];
		}
		
		
		if(file_exists($path.$nama_gambar)){

			unlink($path.$nama_gambar);

		}


		$this->testimoni_model->destroy($where);
		
		redirect('/broserviceadmin/testimoni/', 'refresh');

	}

}
