<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klaim_garansi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
	{
		parent::__construct();

		$this->load->library('ion_auth');

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$this->load->model('klaim_garansi_model');

		$newdata = array(
			'menu'  => 'Klaim Garansi'
		);
		
		$this->session->set_userdata($newdata);
	}

	public function index()
	{
		
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/klaimgaransi/index');
		$this->load->view('admin/klaimgaransi/datatable');
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
	}

	public function diterima()
	{
		
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/klaimgaransi/index');
		$this->load->view('admin/klaimgaransi/datatablebookingditerima');
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
	}

	public function selesai()
	{
		
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/klaimgaransi/index');
		$this->load->view('admin/klaimgaransi/datatableperbaikanselesai');
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
	}

	public function cetakkwitansi($kode)
	{
		
		
		$where = array(

			'klaim_garansi.kode' => $kode
			
		);
		
		$klaimgaransi=$this->klaim_garansi_model->get($where)->row();

		$data['klaimgaransi']=$this->klaim_garansi_model->get($where)->row_array();

		$where = array(

			'id' => $klaimgaransi->users_id
			
		);
		$data['user']=$this->klaim_garansi_model->getuser($where)->row_array();
		
		
		$where = array(

			'perbaikan_id' => $klaimgaransi->id
			
		);
		$data['daftarkerusakan']=$this->klaim_garansi_model->getdaftarkerusakan($where)->result_array();
		$data['kirimonline']=$this->klaim_garansi_model->getkirimonline($where)->row_array();
		$data['antarjemput']=$this->klaim_garansi_model->getantarjemput($where)->row_array();
		$data['komfirmasipembayaran']=$this->klaim_garansi_model->getkomfirmasipembayaran($where)->result_array();
		
		$this->load->library('pdf');
		$title_page = 'Kwitansi';
		$html=$this->load->view('admin/klaimgaransi/cetakkwitansi',$data,true);
		$this->pdf->pdf_create($html,$title_page,'legal','portrait',"false");
		

		// $this->load->view('admin/klaimgaransi/cetakkwitansi',$data);
		 
	}

	public function show($kode){

		$where = array(

			'klaim_garansi.kode' => $kode
			
		);
		
		$klaimgaransi=$this->klaim_garansi_model->get($where)->row();

		$data['klaimgaransi']=$this->klaim_garansi_model->get($where)->row_array();

		$where = array(

			'id' => $klaimgaransi->users_id
			
		);
		$data['user']=$this->klaim_garansi_model->getuser($where)->row_array();
		
		
		$where = array(

			'klaim_garansi_id' => $klaimgaransi->id
			
		);
		
		$data['kirimonline']=$this->klaim_garansi_model->getkirimonline($where)->row_array();
		$data['antarjemput']=$this->klaim_garansi_model->getantarjemput($where)->row_array();
		

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/klaimgaransi/show',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


		



	}

	public function get()
	{

		$data['data']=$this->klaim_garansi_model->get()->result_array();
		echo json_encode($data);
	}

	public function getdata()
	{
		echo $this->klaim_garansi_model->getdata();
	}

	public function getdataditerima()
	{
		echo $this->klaim_garansi_model->getdataditerima();
	}

	public function getdataselesai()
	{
		echo $this->klaim_garansi_model->getdataselesai();
	}

	public function setstatusperbaikan($kode)
	{	

		$where = array(

			'klaim_garansi.kode' => $kode
			
		);
		
		$data['data']=$this->klaim_garansi_model->get($where)->row_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/klaimgaransi/setstatusperbaikan',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function updatestatusperbaikan($kode)
	{	

		$where = array(

			'klaim_garansi.kode' => $kode
			
		);

		if($this->input->post('status')=='false'){

			$status=false;

		}else{

			$status=true;
		}
		$update=array(
			'tanggal_selesai_perbaikan'=>date("Y-m-d"),
			'status_perbaikan'=>$status
		);
		
		$update=$this->klaim_garansi_model->updatestatus($where,$update);

		$data['status']=array(

			'status'=>'success',
			'message'=>'Data berhasil diupdate'

		);
		$data['data']=$this->klaim_garansi_model->get($where)->row_array();

		if($this->input->post('status')=='true'){

			$message="

			<h3>Perbaikan Untuk Klaim Garansi Telah Selesai</h3>
			<p><small>Perbaikan Telah Selesai Kami Akan Mengirim Kembali Barang Ke Pemilik</small></p>
			
			";
			$subject="Perbaikan Untuk Klaim Garansi Telah Selesai";
			$klaim_garansi=$data['data'];
			$this->kirimemail($klaim_garansi['id'],$message,$subject);

		}

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/klaimgaransi/setstatusperbaikan',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function setstatuspembayaran($kode)
	{	

		$where = array(

			'klaim_garansi.kode' => $kode
			
		);
		
		$data['data']=$this->klaim_garansi_model->get($where)->row_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/klaimgaransi/setstatuspembayaran',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function updatestatuspembayaran($kode)
	{	

		$where = array(

			'klaim_garansi.kode' => $kode
			
		);

		if($this->input->post('status')=='false'){

			$status=false;

		}else{

			$status=true;
		}
		$update=array(

			'status_pembayaran'=>$status
		);
		
		$update=$this->klaim_garansi_model->updatestatus($where,$update);

		$data['status']=array(

			'status'=>'success',
			'message'=>'Data berhasil diupdate'

		);
		$data['data']=$this->klaim_garansi_model->get($where)->row_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/klaimgaransi/setstatuspembayaran',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}


	public function setstatuslokasibarang($kode)
	{	

		$where = array(

			'klaim_garansi.kode' => $kode
			
		);
		
		$klaim_garansi=$this->klaim_garansi_model->get($where)->row();
		$where = array(

			'klaim_garansi_id' => $klaim_garansi->id
			
		);
		$data['jenis_layanan']=$klaim_garansi->jenis_layanan;
		$data['kode']=$kode;
		$data['kirimonline']=$this->klaim_garansi_model->getkirimonline($where)->row_array();
		$data['antarjemput']=$this->klaim_garansi_model->getantarjemput($where)->row_array();
		$data['status_lokasi']=$this->klaim_garansi_model->getstatuslokasibarang()->result_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/klaimgaransi/setstatuslokasibarang',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function updatestatuslokasibarang($kode)
	{	

		$where = array(

			'klaim_garansi.kode' => $kode
			
		);

		$klaim_garansi=$this->klaim_garansi_model->get($where)->row();
		
		$where = array(

			'klaim_garansi_id' => $klaim_garansi->id
			
		);

		//antarjemput
		if($klaim_garansi->jenis_layanan==false){

			$update=array(
				
				'status'=>$this->input->post('status')
				
			);
						
			$update=$this->klaim_garansi_model->updatestatuslokasibarangantarjemput($where,$update);


		}else{

			$update=array(
				
				'status'=>$this->input->post('status'),
				'kurirkembali'=>$this->input->post('kurir'),
				'resikembali'=>$this->input->post('resi')
				
			);
			
			$update=$this->klaim_garansi_model->updatestatuslokasibarangkirimonline($where,$update);

		}

		
		//email
		if($this->input->post('status')==2){

			$message="

			<h3>Barang Telah Tiba Di Kantor BROSERVICE</h3>
			<p><small>Kami Akan Memperbaiki Barang Anda</small></p>
			
			";
			$subject="Barang Telah Tiba Di Kantor BROSERVICE";
			
			$this->kirimemail($klaim_garansi->id,$message,$subject);

		}

		//email
		if($this->input->post('status')==3){

			$message="

			<h3>Barang Telah Di Kirim Kembali Kepada Pemilik</h3>
			<p><small>Barang Telah Di kirim Kembali Oleh Kami</small></p>
			
			";
			$subject="Barang Telah Di Kirim Kembali Kepada Pemilik";
			
			$this->kirimemail($klaim_garansi->id,$message,$subject);

		}
			


		$data['status']=array(

			'status'=>'success',
			'message'=>'Data berhasil diupdate'

		);


		$where = array(

			'klaim_garansi_id' => $klaim_garansi->id
			
		);
		$data['jenis_layanan']=$klaim_garansi->jenis_layanan;
		$data['kode']=$kode;
		$data['kirimonline']=$this->klaim_garansi_model->getkirimonline($where)->row_array();
		$data['antarjemput']=$this->klaim_garansi_model->getantarjemput($where)->row_array();
		$data['status_lokasi']=$this->klaim_garansi_model->getstatuslokasibarang()->result_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/klaimgaransi/setstatuslokasibarang',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function setstatusklaimgaransi($kode)
	{	

		$where = array(

			'klaim_garansi.kode' => $kode
			
		);
		
		$data['data']=$this->klaim_garansi_model->get($where)->row_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/klaimgaransi/setstatusklaimgaransi',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function updatestatusklaimgaransi($kode)
	{	

		$where = array(

			'klaim_garansi.kode' => $kode
			
		);

		if($this->input->post('status')=='false'){

			$status=false;

		}else{

			$status=true;
		}
		$update=array(

			'status_klaim_garansi'=>$status
		);
		
		$update=$this->klaim_garansi_model->updatestatus($where,$update);

		$data['status']=array(

			'status'=>'success',
			'message'=>'Data berhasil diupdate'

		);
		$data['data']=$this->klaim_garansi_model->get($where)->row_array();

		if($this->input->post('status')=='true'){

			$message="

			<h3>Permintaan Klaim Garansi Disetujui BROSERVICE</h3>
			<p><small>Permintaan Klaim Garansi Disetujui BROSERVICE</small></p>
			
			";
			$subject="Permintaan Klaim Garansi Disetujui BROSERVICE";
			$klaim_garansi=$data['data'];
			$this->kirimemail($klaim_garansi['id'],$message,$subject);

		}
				

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/klaimgaransi/setstatusklaimgaransi',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}


	public function createresi($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        


            $data['kode']=$kode;
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/klaimgaransi/inputresi',$data);
            $this->load->view('includes/frontendfooter');

           

        }else{

            redirect('auth/login', 'refresh');
        }

    }

    public function updateresi($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        
           
            $where = array(

                'klaim_garansi.kode' => $kode
                
            );
            
            
            $klaim_garansi_id=$this->klaim_garansi_model->get($where)->row()->id;
    
            $where=array(
    
                'klaim_garansi_id'=>$klaim_garansi_id
    
            );


            
            $update=array(
    
                'kurir'=>$this->input->post('kurir'),
                'resi'=>$this->input->post('resi')            
            );
            


            if($this->klaim_garansi_model->updatekirimonline($where,$update)){

                redirect('perbaikan/histori-perbaikan', 'refresh');

            };
    
            
            
        }else{

            redirect('auth/login', 'refresh');
        }

    }

	public function destroy($id)
	{
		$where = array(

			'id' => $id
			
		);
		
		$data=$this->klaim_garansi_model->get($where)->result_array();
		

		$path='assets/uploads/klaimgaransi/';

		foreach($data as $value){
			$nama_gambar=$value['gambar'];
		}
		
		
		if(file_exists($path.$nama_gambar)){

			unlink($path.$nama_gambar);

		}


		$this->klaim_garansi_model->destroy($where);
		
		redirect('/broserviceadmin/klaimgaransi/', 'refresh');

	}

	function kirimemail($klaim_garansi_id,$message,$subject){

       
        $data['message']=$message;

       

        $where = array(

            'klaim_garansi.id' => $klaim_garansi_id
            
        );
        
        $klaimgaransi=$this->klaim_garansi_model->get($where)->row();

        $data['klaimgaransi']=$this->klaim_garansi_model->get($where)->row_array();

        $where = array(

            'id' => $klaimgaransi->users_id
            
        );
        $user=$this->klaim_garansi_model->getuser($where)->row();
        
        
        $where = array(

            'klaim_garansi_id' => $klaimgaransi->id
            
        );
        
        $data['kirimonline']=$this->klaim_garansi_model->getkirimonline($where)->row_array();
        $data['antarjemput']=$this->klaim_garansi_model->getantarjemput($where)->row_array();
        
    
    
        $html=$this->load->view('email/emailklaimgaransi',$data,true);

        $this->load->library('email');    
        $this->email->from('kambingimudth@gmail.com', 'Admin BROSERVICE.ID');   
        $this->email->to($user->email);   
        $this->email->subject($subject);   
        $this->email->message($html);  
        $this->email->send();

        

    }

}
