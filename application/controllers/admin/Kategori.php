<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
	{
		parent::__construct();

		$this->load->library('ion_auth');

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$this->load->model('kategori_model');

		$newdata = array(
			'menu'  => 'Kategori'
		);
		
		$this->session->set_userdata($newdata);
	}

	public function index()
	{
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/kategori/index');
		$this->load->view('admin/kategori/datatable');
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
	}

	public function create()
	{
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/kategori/create');
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
	}

	public function get()
	{

		$data['data']=$this->kategori_model->get()->result_array();
		echo json_encode($data);
	}

	public function getdata()
	{
		echo $this->kategori_model->getdata();
	}

	public function edit($id)
	{	

		$where = array(

			'id' => $id
			
		);
		
		$data['data']=$this->kategori_model->get($where)->result_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/kategori/edit',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function store()
	{

		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'required', 'Nama Harus Di Isi');
		
		if($this->input->post('tampilkan')=='true'){

			$tampilkan=true;

		}else{
			$tampilkan=false;
		}


		if ($this->form_validation->run() == FALSE)
		{
			
			$data['status']=array(

				'status'=>'error',
				'message'=>'Kesalahan input',

			);

		}
		else
		{
			
			
			


			//cek tidak input gambar
			if (empty($_FILES['gambar']['name']))
			{
				$data['status']=array(

					'status'=>'success',
					'message'=>'Data berhasil disimpan tanpa menggunakan gambar'
	
				);
				//array ke model insert
				$store=array(

					'nama'=>$this->input->post('nama'),
					'link'=>strtolower(str_replace(" ","-",$this->input->post('nama'))),
					'tampilkan'=>$tampilkan

				);

				$store=$this->kategori_model->store($store);
				

			}else{
				//array ke model
				$store=array(

					'nama'=>$this->input->post('nama'),
					'link'=>strtolower(str_replace(" ","-",$this->input->post('nama'))),
					'tampilkan'=>$tampilkan

				);
				//mendapatkan id yang kelak jadi nama file
				$id=$this->kategori_model->store($store);
				$file_name=$id;

				$config['upload_path']= 'assets/uploads/kategori/';
				$config['allowed_types']= 'gif|jpg|png';
				$config['file_name']= $file_name;
				$config['overwrite']= true;
				
				// // $config['max_size']             = 100;
				// $config['max_width']            = 1024;
				// $config['max_height']           = 768;

				$this->load->library('upload', $config);
				//proses upload
				if ( ! $this->upload->do_upload('gambar'))
				{
					
					$data['status']=array(

						'status'=>'success',
						'message'=>'Data berhasil disimpan , kesalahan pada saat upload foto'
		
					);
					
				}
				else
				{
					$this->load->library('image_lib');
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'assets/uploads/kategori/'.$this->upload->data('file_name');
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = false;
					$config['width']         = 400;
					$config['height']       = 400;

					$this->image_lib->clear();
					$this->image_lib->initialize($config);
					$this->image_lib->resize();

					$data['status']=array(

						'status'=>'success',
						'message'=>'Data berhasil disimpan'
		
					);

					$where=array(

						'id'=>$id
						
					);
					//array ke model update
					$update=array(

						'gambar'=>$this->upload->data('file_name')
						
					);

					
					$update=$this->kategori_model->update($where,$update);

				}

				
			}
			
			
			
		}
		
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/kategori/create',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
		
	}

	public function update($id)
	{

		$where=array(

			'id'=>$id

		);

		//memberi nilai true or false
		if($this->input->post('tampilkan')=='true'){

			$tampilkan=true;

		}else{
			$tampilkan=false;
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'required', 'Nama Harus Di Isi');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			
			$data['status']=array(

				'status'=>'error',
				'message'=>'Kesalahan input',

			);

		}
		else
		{
			
			
			//cek tidak input gambar
			if (empty($_FILES['gambar']['name']))
			{
				$data['status']=array(

					'status'=>'success',
					'message'=>'Data berhasil diupdate'
	
				);
				//array ke model insert
				$update=array(

					'nama'=>$this->input->post('nama'),
					'link'=>strtolower(str_replace(" ","-",$this->input->post('nama'))),
					'tampilkan'=>$tampilkan

				);
				$update=$this->kategori_model->update($where,$update);

			}else{
				
				$file_name=$id;

				$config['upload_path']= 'assets/uploads/kategori/';
				$config['allowed_types']= 'gif|jpg|png';
				$config['file_name']= $file_name;
				$config['overwrite']= true;
				
				// // $config['max_size']             = 100;
				// $config['max_width']            = 1024;
				// $config['max_height']           = 768;

				$this->load->library('upload', $config);
				//proses upload
				if ( ! $this->upload->do_upload('gambar'))
				{
					
					$data['status']=array(

						'status'=>'success',
						'message'=>'Data berhasil diupdate , kesalahan pada saat upload foto'
		
					);

					$update=array(

						'nama'=>$this->input->post('nama'),
						'link'=>strtolower(str_replace(" ","-",$this->input->post('nama'))),
						'tampilkan'=>$tampilkan
	
					);
					$update=$this->kategori_model->update($where,$update);
						
				}
				else
				{
					$this->load->library('image_lib');
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'assets/uploads/kategori/'.$this->upload->data('file_name');
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = false;
					$config['width']         = 400;
					$config['height']       = 400;

					$this->image_lib->clear();
					$this->image_lib->initialize($config);
					$this->image_lib->resize();

					$data['status']=array(

						'status'=>'success',
						'message'=>'Data berhasil diupdate'
		
					);
					//array ke model update
					$update=array(

						'nama'=>$this->input->post('nama'),
						'link'=>strtolower(str_replace(" ","-",$this->input->post('nama'))),
						'gambar'=>$this->upload->data('file_name'),
						'tampilkan'=>$tampilkan
	
					);
					

					
					$update=$this->kategori_model->update($where,$update);

				}


			}
			
			
			
		}
		
		$where = array(

			'id' => $id
			
		);
		
		$data['data']=$this->kategori_model->get($where)->result_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/kategori/edit',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
		
	}

	public function destroy($id)
	{
		$where = array(

			'id' => $id
			
		);
		
		$data=$this->kategori_model->get($where)->result_array();
		

		$path='assets/uploads/kategori/';

		foreach($data as $value){
			$nama_gambar=$value['gambar'];
		}
		
		
		if(file_exists($path.$nama_gambar)){

			unlink($path.$nama_gambar);

		}


		$this->kategori_model->destroy($where);
		
		redirect('/broserviceadmin/kategori/', 'refresh');

	}

}
