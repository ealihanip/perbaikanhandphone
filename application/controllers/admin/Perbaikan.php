<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perbaikan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
	{
		parent::__construct();

		$this->load->library('ion_auth');

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$this->load->model('perbaikan_model');
		$this->load->model('pengaturan_model');

		$newdata = array(
			'menu'  => 'Perbaikan'
		);
		
		$this->session->set_userdata($newdata);
	}

	public function index()
	{
		
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/index');
		$this->load->view('admin/perbaikan/datatable');
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
	}

	public function diterima()
	{
		
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/index');
		$this->load->view('admin/perbaikan/datatablebookingditerima');
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
	}

	public function ditolak()
	{
		
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/index');
		$this->load->view('admin/perbaikan/datatablebookingditolak');
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
	}

	public function selesai()
	{
		
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/index');
		$this->load->view('admin/perbaikan/datatableperbaikanselesai');
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');
	}

	public function cetakkwitansi($kode)
	{
		
		
		$where = array(

			'kode' => $kode
			
		);
		
		$perbaikan=$this->perbaikan_model->get($where)->row();

		$data['perbaikan']=$this->perbaikan_model->get($where)->row_array();

		$where = array(

			'id' => $perbaikan->users_id
			
		);
		$data['user']=$this->perbaikan_model->getuser($where)->row_array();
		
		
		$where = array(

			'perbaikan_id' => $perbaikan->id
			
		);
		$data['daftarkerusakan']=$this->perbaikan_model->getdaftarkerusakan($where)->result_array();
		$data['kirimonline']=$this->perbaikan_model->getkirimonline($where)->row_array();
		$data['antarjemput']=$this->perbaikan_model->getantarjemput($where)->row_array();
		$data['komfirmasipembayaran']=$this->perbaikan_model->getkomfirmasipembayaran($where)->result_array();
		
		
		$durasi=$this->pengaturan_model->get(array('id'=>1))->row()->nilai;
		$tanggalselesai= $perbaikan->tanggal_selesai_perbaikan;
		$akhir_garansi = date("Y-m-d", strtotime('+'.$durasi.' days', strtotime($tanggalselesai)));
		$data['akhir_garansi']=$akhir_garansi;
		
		$this->load->library('pdf');
		$title_page = 'Kwitansi';
		$html=$this->load->view('admin/perbaikan/cetakkwitansi',$data,true);
		$this->pdf->pdf_create($html,$title_page,'legal','portrait',"false");
		

		// $this->load->view('admin/perbaikan/cetakkwitansi',$data);
		 
	}

	public function show($kode){

		$where = array(

			'kode' => $kode
			
		);
		
		$perbaikan=$this->perbaikan_model->get($where)->row();

		$data['perbaikan']=$this->perbaikan_model->get($where)->row_array();

		$where = array(

			'id' => $perbaikan->users_id
			
		);
		$data['user']=$this->perbaikan_model->getuser($where)->row_array();
		
		
		$where = array(

			'perbaikan_id' => $perbaikan->id
			
		);
		$data['daftarkerusakan']=$this->perbaikan_model->getdaftarkerusakan($where)->result_array();
		$data['kirimonline']=$this->perbaikan_model->getkirimonline($where)->row_array();
		$data['antarjemput']=$this->perbaikan_model->getantarjemput($where)->row_array();
		$data['komfirmasipembayaran']=$this->perbaikan_model->getkomfirmasipembayaran($where)->result_array();
		$where = array(

			'perbaikan_id' => $perbaikan->id
			
		);

		//kondisi untuk kupon

		
		if($perbaikan->kupon!=null){

			$where=array(

				"id"=>$perbaikan->kupon
			);

			$data['kupon']=$this->perbaikan_model->getkupon($where)->result_array();

		}



		//get data pengaturan akhir garansi
		$durasi=$this->pengaturan_model->get(array('id'=>1))->row()->nilai;
		$tanggalselesai= $perbaikan->tanggal_selesai_perbaikan;
		$akhir_garansi = date("Y-m-d", strtotime('+'.$durasi.' days', strtotime($tanggalselesai)));
		$data['akhir_garansi']=$akhir_garansi;
		


		// $where = array(

		// 	'id' => $perbaikan->supersubkategori_id
			
		// );

		// $data['supersubkategori']=$this->perbaikan_model->getsupersubkategori($where)->row_array();


		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/show',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


		



	}

	public function get()
	{

		$data['data']=$this->perbaikan_model->get()->result_array();
		echo json_encode($data);
	}

	public function getdata()
	{
		echo $this->perbaikan_model->getdata();
	}

	public function getdataditerima()
	{
		echo $this->perbaikan_model->getdataditerima();
	}

	public function getdataditolak()
	{
		echo $this->perbaikan_model->getdataditolak();
	}

	public function getdataselesai()
	{
		echo $this->perbaikan_model->getdataselesai();
	}

	public function setstatusperbaikan($kode)
	{	

		$where = array(

			'kode' => $kode
			
		);
		
		$data['data']=$this->perbaikan_model->get($where)->row_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/setstatusperbaikan',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function updatestatusperbaikan($kode)
	{	

		$where = array(

			'kode' => $kode
			
		);

		if($this->input->post('status')=='false'){

			$status=false;

		}else{

			$status=true;
		}


		$update=array(
			'tanggal_selesai_perbaikan'=>date("Y-m-d"),
			'status_perbaikan'=>$status
		);
		
		$update=$this->perbaikan_model->updatestatus($where,$update);

		$data['status']=array(

			'status'=>'success',
			'message'=>'Data berhasil diupdate'

		);
		$data['data']=$this->perbaikan_model->get($where)->row_array();
		if($this->input->post('status')=='true'){
			$message="

                <h3>BARANG ANDA TELAH SELESAI DI PERBAIKI</h3>
				<p><small>SIlahkan Bayar Via Transfer atau Bayar Di tempat Jika Menggunakan Layanan Antar Jemput</small></p>
				
                ";
			$subject="Barang Telah Selesai Di Perbaiki BROSERVICE";
			$perbaikan=$data['data'];
			$this->kirimemail($perbaikan['id'],$message,$subject);
		}

		//email
		
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/setstatusperbaikan',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function setstatuspembayaran($kode)
	{	

		$where = array(

			'kode' => $kode
			
		);
		
		$data['data']=$this->perbaikan_model->get($where)->row_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/setstatuspembayaran',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function updatestatuspembayaran($kode)
	{	

		$where = array(

			'kode' => $kode
			
		);

		if($this->input->post('status')=='false'){

			$status=false;

		}else{

			$status=true;
		}
		$update=array(

			'status_pembayaran'=>$status
		);
		
		$update=$this->perbaikan_model->updatestatus($where,$update);

		$data['status']=array(

			'status'=>'success',
			'message'=>'Data berhasil diupdate'

		);
		$data['data']=$this->perbaikan_model->get($where)->row_array();

		//email
		if($this->input->post('status')=='true'){
			$message="

                <h3>PEMBAYARAN ANDA TELAH KAMI VERIFIKASI</h3>
				
                ";
			$subject="PEMBAYARAN TELAH DIVERIFIKASI BROSERVICE";
			$perbaikan=$data['data'];
			$this->kirimemail($perbaikan['id'],$message,$subject);
		}

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/setstatuspembayaran',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}


	public function setstatuslokasibarang($kode)
	{	

		$where = array(

			'kode' => $kode
			
		);
		
		$perbaikan=$this->perbaikan_model->get($where)->row();
		$where = array(

			'perbaikan_id' => $perbaikan->id
			
		);
		$data['jenis_layanan']=$perbaikan->jenis_layanan;
		$data['kode']=$kode;
		$data['kirimonline']=$this->perbaikan_model->getkirimonline($where)->row_array();
		$data['antarjemput']=$this->perbaikan_model->getantarjemput($where)->row_array();
		$data['status_lokasi']=$this->perbaikan_model->getstatuslokasibarang()->result_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/setstatuslokasibarang',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function updatestatuslokasibarang($kode)
	{	

		$where = array(

			'kode' => $kode
			
		);

		$perbaikan=$this->perbaikan_model->get($where)->row();
		
		$where = array(

			'perbaikan_id' => $perbaikan->id
			
		);

		//email
		if($this->input->post('status')==2){
			$message="

                <h3>Barang Telah Tiba Di Kantor BROSERVICE</h3>
				<p><small>Kami Akan Melakukan Perbaikan Pada Barang Anda</small></p>
				
                ";
			$subject="Barang Telah Tiba Di Kantor BROSERVICE";
			
			$this->kirimemail($perbaikan->id,$message,$subject);
		}

		//email
		if($this->input->post('status')==3){
			$message="

                <h3>Barang Telah Di Kirim Kembali Kepada Pemilik</h3>
				<p><small>Barang Telah Di kirim Kembali Oleh Kami</small></p>
				
                ";
			$subject="Barang Telah Di Kirim Kembali Kepada Pemilik";
			
			$this->kirimemail($perbaikan->id,$message,$subject);
		}

		//antarjemput
		if($perbaikan->jenis_layanan==false){

			$update=array(
				
				'status'=>$this->input->post('status')
				
			);
			
			$update=$this->perbaikan_model->updatestatuslokasibarangantarjemput($where,$update);


		}else{

			$update=array(
				
				'status'=>$this->input->post('status'),
				'kurirkembali'=>$this->input->post('kurir'),
				'resikembali'=>$this->input->post('resi')
				
			);
			
			$update=$this->perbaikan_model->updatestatuslokasibarangkirimonline($where,$update);

		}

		
		
		

		$data['status']=array(

			'status'=>'success',
			'message'=>'Data berhasil diupdate'

		);


		$where = array(

			'perbaikan_id' => $perbaikan->id
			
		);
		$data['jenis_layanan']=$perbaikan->jenis_layanan;
		$data['kode']=$kode;
		$data['kirimonline']=$this->perbaikan_model->getkirimonline($where)->row_array();
		$data['antarjemput']=$this->perbaikan_model->getantarjemput($where)->row_array();
		$data['status_lokasi']=$this->perbaikan_model->getstatuslokasibarang()->result_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/setstatuslokasibarang',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function setstatusbooking($kode)
	{	

		$where = array(

			'kode' => $kode
			
		);
		
		$data['data']=$this->perbaikan_model->get($where)->row_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/setstatusbooking',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function updatestatusbooking($kode)
	{	

		$where = array(

			'kode' => $kode
			
		);

		if($this->input->post('status')=='false'){

			$status=false;

		}else{

			$status=true;
			
		}
		$update=array(

			'status_booking'=>$status
		);
		
		$update=$this->perbaikan_model->updatestatus($where,$update);

		$data['status']=array(

			'status'=>'success',
			'message'=>'Data berhasil diupdate'

		);
		$data['data']=$this->perbaikan_model->get($where)->row_array();

			$message="

                <h3>Booking Perbaikan Telah Di Terima</h3>
				<p><small>Admin Telah Menerima Booking Anda</small></p>
				<p><small>Kami Akan Menjemput Barang Anda Atau Kami Akan Menunggu Barang Anda Tiba Di Kantor Kami Untuk Dilakukan Perbaikan</small></p>
                
                ";
			$subject="Booking Perbaikan Diterima BROSERVICE";
			$perbaikan=$data['data'];
			$this->kirimemail($perbaikan['id'],$message,$subject);
		

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/setstatusbooking',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function destroy($id)
	{
		$where = array(

			'id' => $id
			
		);
		
		$data=$this->perbaikan_model->get($where)->result_array();
		

		$path='assets/uploads/perbaikan/';

		foreach($data as $value){
			$nama_gambar=$value['gambar'];
		}
		
		
		if(file_exists($path.$nama_gambar)){

			unlink($path.$nama_gambar);

		}


		$this->perbaikan_model->destroy($where);
		
		redirect('/broserviceadmin/perbaikan/', 'refresh');

	}

	function kirimemail($perbaikan_id,$message,$subject){

		
        $data['message']=$message;
        $where=array(

            'perbaikan.id'=>$perbaikan_id

        );

		
		$perbaikan=$this->perbaikan_model->get($where)->row();

		$data['perbaikan']=$this->perbaikan_model->get($where)->row_array();

		$where = array(

			'id' => $perbaikan->users_id
			
		);
		$user=$this->perbaikan_model->getuser($where)->row();
		
		
		$where = array(

			'perbaikan_id' => $perbaikan->id
			
		);
		$data['daftarkerusakan']=$this->perbaikan_model->getdaftarkerusakan($where)->result_array();
		$data['kirimonline']=$this->perbaikan_model->getkirimonline($where)->row_array();
		$data['antarjemput']=$this->perbaikan_model->getantarjemput($where)->row_array();
		$data['komfirmasipembayaran']=$this->perbaikan_model->getkomfirmasipembayaran($where)->result_array();
		
		
		$tanggalselesai= $perbaikan->tanggal_selesai_perbaikan;
		$akhir_garansi = date("Y-m-d", strtotime('+30 days', strtotime($tanggalselesai)));
		$data['akhir_garansi']=$akhir_garansi;
        


        $html=$this->load->view('email/email',$data,true);

        $this->load->library('email');    
        $this->email->from('kambingimudth@gmail.com', 'Admin BROSERVICE.ID');   
        $this->email->to($user->email);   
        $this->email->subject($subject);   
        $this->email->message($html);  
        $this->email->send();

        

	}
	
	public function createtolakperbaikan($kode)
	{	

		$where = array(

			'kode' => $kode
			
		);
		
		$data['data']=$this->perbaikan_model->get($where)->row_array();

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('includes/startcontent');
		$this->load->view('admin/perbaikan/tolakperbaikan',$data);
		$this->load->view('includes/endcontent');
		$this->load->view('includes/footer');


	}

	public function storetolakperbaikan($kode)
	{	

		$where = array(

			'kode' => $kode
			
		);


		$update=array(
			'status_transaksi'=>8
		);
		
		$update=$this->perbaikan_model->updatestatus($where,$update);

		$data['data']=$this->perbaikan_model->get($where)->row_array();
		

		//email
		$message="

			<h3>BARANG ANDA DITOLAK UNTUK PERBAIKI</h3>
			<p><small>Barang Anda Di Tolak Untuk Di perbaiki Karena Barang Anda Tidak Bisa Di Perbaiki Oleh Kami, Kami Akan Mengirim Kembali Barang Anda Ke Lokasi Anda. Terima Kasih.</small></p>
			
			";
		$subject="BARANG ANDA DITOLAK UNTUK PERBAIKI BROSERVICE";
		$perbaikan=$data['data'];
		

		//email
		
		redirect('/broserviceadmin/perbaikan/', 'refresh');

	}


}
