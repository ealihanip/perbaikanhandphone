<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klaim_garansi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */




	public function __construct(){
		parent::__construct();

        $this->load->model('klaim_garansi_model');
        $this->load->model('perbaikan_model');
        

		$newdata = array(
			'menu'  => 'Garansi'
		);
		
		$this->session->set_userdata($newdata);
	}

	public function komfirmasiklaimgaransi($kode){



        $data['data']=array();
        $data['kode']=$kode;

        $this->load->view('includes/frontendheader');
		$this->load->view('includes/frontendnavbar');
		$this->load->view('frontend/klaimgaransi/komfirmasiklaimgaransi',$data);
		$this->load->view('includes/frontendfooter');
        
    }


    public function create($kode){



        $data['data']=array();
        $data['kode']=$kode;



        //mendapatkan informasi perbaikan

        $where=array(

            'kode'=>$kode
        );

        $perbaikan=$this->perbaikan_model->get($where)->row();


        //mendapatkan informasi layanan perbaikan

        if($perbaikan->jenis_layanan==false){
            //jika antar jemput

            redirect('klaim-garansi/antar-jemput/'.$kode, 'refresh');

        }else{
            //jika kirim online

            redirect('klaim-garansi/kirim-online/store/'.$kode, 'refresh');
        }
        
    }

    public function createantarjemput($kode){



        $data['data']=array();
        $data['kode']=$kode;


        
        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $data['kode']=$kode;
            
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/klaimgaransi/createantarjemput',$data);
            $this->load->view('includes/frontendfooter');

        }else{

            redirect('auth/login', 'refresh');
        }
        
    }

    public function storeantarjemput($kode){



        $data['data']=array();
        $data['kode']=$kode;


         //mendapatkan informasi perbaikan

        $where=array(

            'kode'=>$kode
        );

        $perbaikan=$this->perbaikan_model->get($where)->row();

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $this->load->library('form_validation');
		    $this->form_validation->set_rules('tanggal', 'Tanggal', 'required', 'Tanggal Harus Di Isi');
            $this->form_validation->set_rules('jam', 'Jam', 'required', 'Jam Harus Di Isi');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required', 'Alamat Harus Di Isi');
            

            if ($this->form_validation->run() == FALSE){


            

            }else{

                $user = $this->ion_auth->user()->row();
                $like=array(

                    'kode'=>'KG'
                );
                $kode=$this->klaim_garansi_model->getmaxkodeklaimgaransi($like)->row()->kode;
                
                if($kode!=null){   

                    $kode = (int) substr($kode, 2, 6);
                    $kode++;
                    $char = "KG";

                    $kode = $char . sprintf("%06s", $kode);
                    
                    
                }else{

                    $kode='KG000001';
                }


                
               

                $data=array(

                    'users_id'=>$user->id,
                    'perbaikan_id'=>$perbaikan->id,
                    'kode'=>$kode,
                    'tanggal_klaim_garansi'=>date("Y-m-d"),
                    'status_perbaikan'=>'0',
                    'jenis_layanan'=>'0'

                );

                $klaim_garansi_id=$this->klaim_garansi_model->storeklaimgaransi($data);

                $data=array(

                    'klaim_garansi_id'=>$klaim_garansi_id,
                    'tanggal'=>$this->input->post('tanggal'),
                    'jam'=>$this->input->post('jam'),
                    'alamat'=>$this->input->post('alamat')


                );

                $this->klaim_garansi_model->storeantarjemput($data);

                
                $message="

                <h3>Permintaan Klaim Garansi Diterima</h3>
                <p><small>Admin Akan Melakukan Pengecekan Untuk Klaim Garansi</small></p>
                
                ";
                $subject="Permintaan Klaim Garansi Diterima";
                $this->kirimemail($klaim_garansi_id,$message,$subject);
           
            }
            

            redirect('klaim-garansi/histori-klaim-garansi', 'refresh');

        }else{

            redirect('auth/login', 'refresh');
        }
        
    }

    public function storekirimonline($kode){



        $data['data']=array();
        $data['kode']=$kode;


         //mendapatkan informasi perbaikan

        $where=array(

            'kode'=>$kode
        );

        $perbaikan=$this->perbaikan_model->get($where)->row();

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            

                $user = $this->ion_auth->user()->row();
                $like=array(

                    'kode'=>'KG'
                );
                $kode=$this->klaim_garansi_model->getmaxkodeklaimgaransi($like)->row()->kode;
                
                if($kode!=null){   

                    $kode = (int) substr($kode, 2, 6);
                    $kode++;
                    $char = "KG";

                    $kode = $char . sprintf("%06s", $kode);
                    
                    
                }else{

                    $kode='KG000001';
                }


                
               

                $data=array(

                    'users_id'=>$user->id,
                    'perbaikan_id'=>$perbaikan->id,
                    'kode'=>$kode,
                    'tanggal_klaim_garansi'=>date("Y-m-d"),
                    'status_perbaikan'=>'0',
                    'jenis_layanan'=>'1'

                );

                $klaim_garansi_id=$this->klaim_garansi_model->storeklaimgaransi($data);

                
                
                $data=array(

                    'klaim_garansi_id'=>$klaim_garansi_id,
                    'status'=>'1'
    
    
                );
                $this->klaim_garansi_model->storekirimonline($data);
                
                
                $where=array(

                    'klaim_garansi.id'=>$klaim_garansi_id,
    
                );
                
                $message="

                <h3>Permintaan Klaim Garansi Diterima</h3>
                <p><small>Admin Akan Melakukan Pengecekan Untuk Klaim Garansi</small></p>
                
                ";
                $subject="Permintaan Klaim Garansi Diterima";
                
                $this->kirimemail($klaim_garansi_id,$message,$subject);
    
                $data['kode']=$this->klaim_garansi_model->get($where)->row()->kode;
                $data['finished']=true;
                $this->load->view('includes/frontendheader');
                $this->load->view('includes/frontendnavbar');
                $this->load->view('frontend/klaimgaransi/inputresi',$data);
                $this->load->view('includes/frontendfooter');

                

            redirect('perbaikan/histori-perbaikan', 'refresh');

        }else{

            redirect('auth/login', 'refresh');
        }
        
    }


    public function editantarjemput($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $data['kode']=$kode;
            
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/klaimgaransi/editantarjemput',$data);
            $this->load->view('includes/frontendfooter');

        }else{

            redirect('auth/login', 'refresh');
        }
    }

    public function updateantarjemput($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $data['kode']=$kode;
            
            $where = array(

                'klaim_garansi.kode' => $kode
                
            );
            
            
            $klaim_garansi_id=$this->klaim_garansi_model->get($where)->row()->id;
    
            $where=array(
    
                'klaim_garansi_id'=>$klaim_garansi_id
    
            );


            
            $update=array(
    
                'tanggal'=>$this->input->post('tanggal'),
                'jam'=>$this->input->post('jam'),
                'alamat'=>$this->input->post('alamat')            
            );
            


            if($this->klaim_garansi_model->updateantarjemput($where,$update)){

                redirect('klaim-garansi/histori-klaim-garansi', 'refresh');

            };
    

        }else{

            redirect('auth/login', 'refresh');
        }
    }


    public function createresi($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        


            $data['kode']=$kode;
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/klaimgaransi/inputresi',$data);
            $this->load->view('includes/frontendfooter');

           

        }else{

            redirect('auth/login', 'refresh');
        }

    }

    public function updateresi($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        
           
            $where = array(

                'klaim_garansi.kode' => $kode
                
            );
            
            
            $klaim_garansi_id=$this->klaim_garansi_model->get($where)->row()->id;
    
            $where=array(
    
                'klaim_garansi_id'=>$klaim_garansi_id
    
            );


            
            $update=array(
    
                'kurir'=>$this->input->post('kurir'),
                'resi'=>$this->input->post('resi')            
            );
            


            if($this->klaim_garansi_model->updatekirimonline($where,$update)){

                redirect('klaim-garansi/histori-klaim-garansi', 'refresh');

            };
    
            
            
        }else{

            redirect('auth/login', 'refresh');
        }

    }


    public function historiklaimgaransi(){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $user = $this->ion_auth->user()->row();

            $where=array(

                'klaim_garansi.users_id'=>$user->id

            );

            $klaimgaransi=$this->klaim_garansi_model->get($where)->result_array();
            // $data['daftar_kerusakan']=array();

            $temp_data=array();
            
            foreach($klaimgaransi as $value){

                

                $where=array(

                    'klaim_garansi_id'=>$value['id']
                );
                $kirimonline=$this->klaim_garansi_model->getkirimonline($where)->result_array();
                $antarjemput=$this->klaim_garansi_model->getantarjemput($where)->result_array();
                

                
                $array=array(

                    'id'=>$value['id'],
                    'kode_klaim_garansi'=>$value['kode_klaim_garansi'],
                    'kode_perbaikan'=>$value['kode_perbaikan'],
                    'tanggal_klaim_garansi'=>$value['tanggal_klaim_garansi'],
                    'users_id'=>$value['users_id'], 
                    'status_klaim_garansi'=>$value['status_klaim_garansi'],
                    'nama_status_klaim_garansi'=>$value['nama_status_klaim_garansi'],
                    'status_transaksi'=>$value['status_transaksi'],
                    'nama_status_perbaikan'=>$value['nama_status_perbaikan'],
                    'jenis_layanan'=>$value['jenis_layanan'],
                    'status_perbaikan'=>$value['status_perbaikan'],
                    'tanggal_selesai_perbaikan'=>$value['tanggal_selesai_perbaikan'],
                    
                    'kirim_online'=>$kirimonline,
                    'antar_jemput'=>$antarjemput

                );

                array_push($temp_data,$array);
                
            }
            

            // $data['antarjemput']=$this->klaim_garansi_model->getantarjemput($where)->result_array();
            // $data['kirimonline']=$this->klaim_garansi_model->getkirimonline($where)->result_array();

            
            $data['data']=$temp_data;


            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/klaimgaransi/historiklaimgaransi',$data);
            $this->load->view('includes/frontendfooter');
            



        }else{

            redirect('auth/login', 'refresh');

        }



    }

    public function komfirmasibarangditerima($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        


            $data['kode']=$kode;
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/klaimgaransi/komfirmasibarangditerima',$data);
            $this->load->view('includes/frontendfooter');

           

        }else{

            redirect('auth/login', 'refresh');
        }

    }

    public function updatekomfirmasibarangditerima($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $data['kode']=$kode;
            
            $where = array(

                'klaim_garansi.kode' => $kode
                
            );
            
            
            $klaim_garansi_id=$this->klaim_garansi_model->get($where)->row()->id;
    
            $where=array(
    
                'id'=>$klaim_garansi_id
    
            );


            
            $update=array(
    
                'status_transaksi'=>4,
                 
            );
            


            if($this->klaim_garansi_model->updatestatus($where,$update)){

               

            };


            $message="

            <h3>Klaim Garansi Telah Selesai</h3>
            <p><small>Terima Kasih Telah Menggunakan Layanan Kami</small></p>
            
            ";
            $subject="Klaim Garansi Telah Selesai";
            
            $this->kirimemail($klaim_garansi_id,$message,$subject);
            
            
            redirect('klaim-garansi/histori-klaim-garansi/', 'refresh');

        }else{

            redirect('auth/login', 'refresh');
        }
    }

    public function batalkanklaimgaransi($kode)
	{
		if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $data['kode']=$kode;
            

            
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/klaimgaransi/batalkanklaimgaransi',$data);
            $this->load->view('includes/frontendfooter');
            
          
            
    

        }else{

            redirect('auth/login', 'refresh');
        }

    }

    public function destroy($kode)
	{
		if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $data['kode']=$kode;
            
            $where = array(

                'klaim_garansi.kode' => $kode
                
            );
            
            
            $klaim_garansi=$this->klaim_garansi_model->get($where)->row();

    
            $where = array(

                'id' => $klaim_garansi->id
                
            );
            if($klaim_garansi->status_klaim_garansi==false){

                $this->klaim_garansi_model->destroy($where);

            }
            

            redirect('klaim-garansi/histori-klaim-garansi', 'refresh');
    

        }else{

            redirect('auth/login', 'refresh');
        }

    }

    function kirimemail($klaim_garansi_id,$message,$subject){

        $user = $this->ion_auth->user()->row();
        $data['message']=$message;

       

        $where = array(

            'klaim_garansi.id' => $klaim_garansi_id
            
        );
        
        $klaimgaransi=$this->klaim_garansi_model->get($where)->row();

        $data['klaimgaransi']=$this->klaim_garansi_model->get($where)->row_array();

        $where = array(

            'id' => $klaimgaransi->users_id
            
        );
        $data['user']=$this->klaim_garansi_model->getuser($where)->row_array();
        
        
        $where = array(

            'klaim_garansi_id' => $klaimgaransi->id
            
        );
        
        $data['kirimonline']=$this->klaim_garansi_model->getkirimonline($where)->row_array();
        $data['antarjemput']=$this->klaim_garansi_model->getantarjemput($where)->row_array();
        
    
    
        $html=$this->load->view('email/emailklaimgaransi',$data,true);

        $this->load->library('email');    
        $this->email->from('kambingimudth@gmail.com', 'Admin BROSERVICE.ID');   
        $this->email->to($user->email);   
        $this->email->subject($subject);   
        $this->email->message($html);  
        $this->email->send();

        

    }


    
}
