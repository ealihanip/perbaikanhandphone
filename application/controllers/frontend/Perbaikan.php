<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perbaikan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */




	public function __construct(){
		parent::__construct();

        $this->load->model('perbaikan_model');
        $this->load->model('pengaturan_model');
        

		$newdata = array(
			'menu'  => 'Booking'
		);
		
		$this->session->set_userdata($newdata);
	}

	public function index(){
        
        $data['data']=array();
        
        if ($this->uri->segment(4) != null)
        {
            $where=array(

                'supersubkategori.link'=>$this->uri->segment(4)
    
            );
    
            $data['data']=$this->perbaikan_model->getkerusakan($where)->result_array();

            $newdata = array(
                'pilih'  => 'kerusakan',
                'link'  => 'perbaikan/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/',
            );
            
            $this->session->set_userdata($newdata);
        }
        

        elseif ($this->uri->segment(3) != null)
        {    
         
            $where=array(

                'subkategori.link'=>$this->uri->segment(3)
    
            );
    
            $data['data']=$this->perbaikan_model->getsupersubkategori($where)->result_array();

            $newdata = array(
                'pilih'  => 'supersubkategori',
                'link'  => 'perbaikan/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/',
            );
            
            $this->session->set_userdata($newdata);
        }

        elseif ($this->uri->segment(2) != null)
        {
            $where=array(

                'kategori.link'=>$this->uri->segment(2)
    
            );
    
            $data['data']=$this->perbaikan_model->getsubkategori($where)->result_array();

            $newdata = array(
                'pilih'  => 'subkategori',
                'link'  => 'perbaikan/'.$this->uri->segment(2).'/',
            );
            
            $this->session->set_userdata($newdata);
        }
        
        

           

      
        $this->load->view('includes/frontendheader');
		$this->load->view('includes/frontendnavbar');
		$this->load->view('frontend/perbaikan/index',$data);
		$this->load->view('includes/frontendfooter');
                
        
        
		
	
    }
    
    public function detailkerusakan(){


        if ($this->uri->segment(6) != null)
        {
            $where=array(

                'nama'=>$this->uri->segment(6)
    
            );
    
            $data['kupon']=$this->perbaikan_model->getkupon($where)->row_array();
            
            if($data['kupon']!=null){
                // $newdata = array(
                //     'kupon'=>$this->uri->segment(6)
                
                // );
                
                // $this->session->set_userdata($newdata);

                // $data['status_kupon']=true;
                $data['kupon_ditemukan']=true;
               
            }else{
                // $data['status_kupon']=false;
                $data['kupon_ditemukan']=false;
            }
          
        }
        if ($this->uri->segment(5) != null)
        {
            $where=array(

                'kerusakan.link'=>$this->uri->segment(5)
    
            );
    
            $data['data']=$this->perbaikan_model->getkerusakan($where)->result_array();
            foreach($data['data'] as $value){

                $kerusakan=$value['id'];
                $supersubkategoriid=$value['supersubkategori_id'];

            }
            $newdata = array(
                'pilih'  => 'kerusakan',
                'kerusakan_id'  => $kerusakan,
                'supersubkategori_id'  => $supersubkategoriid,
                'link'  => 'perbaikan/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5).'/',
            );
            
            $this->session->set_userdata($newdata);
        }

        
        $this->load->view('includes/frontendheader');
		$this->load->view('includes/frontendnavbar');
		$this->load->view('frontend/perbaikan/detailkerusakan',$data);
		$this->load->view('includes/frontendfooter');

    }
 
    public function pilihlayanan(){


        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $where=array(

                'kerusakan.id'=>$this->session->kerusakan_id
    
            );
    
            $data['data']=$this->perbaikan_model->getkerusakan($where)->result_array();

            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/perbaikan/pilihlayanan',$data);
            $this->load->view('includes/frontendfooter');

           

        }else{

            redirect('auth/login', 'refresh');
        }

        
        

    }

    public function createantarjemput(){
        
        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin() && isset($this->session->kerusakan_id)){
            
        // echo $this->session->kerusakan_id;
        $this->load->view('includes/frontendheader');
        $this->load->view('includes/frontendnavbar');
        $this->load->view('frontend/perbaikan/createantarjemput');
        $this->load->view('includes/frontendfooter');

           

        }else{

            redirect('auth/login', 'refresh');
        }
    
    }

    public function storeantarjemput(){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin() && isset($this->session->kerusakan_id)){
            
            $this->load->library('form_validation');
		    $this->form_validation->set_rules('tanggal', 'Tanggal', 'required', 'Tanggal Harus Di Isi');
            $this->form_validation->set_rules('jam', 'Jam', 'required', 'Jam Harus Di Isi');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required', 'Alamat Harus Di Isi');
            

            if ($this->form_validation->run() == FALSE){


            

            }else{

                $user = $this->ion_auth->user()->row();
                $like=array(

                    'kode'=>'AJ'
                );
                $kode=$this->perbaikan_model->getmaxkodeperbaikan($like)->row()->kode;
                
                if($kode!=null){   

                    $kode = (int) substr($kode, 2, 6);
                    $kode++;
                    $char = "AJ";

                    $kode = $char . sprintf("%06s", $kode);
                    
                    
                }else{

                    $kode='AJ000001';
                }


                //kondisi untuk kupon

                $kupon=null;

                if(isset($this->session->kupon)){

                    $where=array(

                        "nama"=>$this->session->kupon
                    );

                    $datakupon=$this->perbaikan_model->getkupon($where)->result_array();

                    foreach($datakupon as $value){

                        $kupon=$value['id'];
                        
                    }

                }

               

                $data=array(

                    'users_id'=>$user->id,
                    'kode'=>$kode,
                    'supersubkategori_id'=>$this->session->supersubkategori_id,
                    'status_pembayaran'=>'0',
                    'tanggal_booking'=>date("Y-m-d"),
                    'status_perbaikan'=>'0',
                    'jenis_layanan'=>'0',
                    'kupon'=>$kupon

                );

                $perbaikan_id=$this->perbaikan_model->storeperbaikan($data);

                $data=array(

                    'perbaikan_id'=>$perbaikan_id,
                    'tanggal'=>$this->input->post('tanggal'),
                    'jam'=>$this->input->post('jam'),
                    'alamat'=>$this->input->post('alamat')


                );

                $this->perbaikan_model->storeantarjemput($data);
                
                $where=array(
                    'kerusakan.id'=>$this->session->kerusakan_id
                );

                
                $kerusakan=$this->perbaikan_model->getkerusakan($where)->row();

                $data=array(

                    'perbaikan_id'=>$perbaikan_id,
                    'kerusakan_id'=>$kerusakan->id,
                    'diskon'=>$kerusakan->diskon


                );
                $this->perbaikan_model->storedaftarkerusakan($data);


                $message="

                <h3>Booking Perbaikan Telah Di Terima</h3>
                <p><small>Admin Akan Mengkomfirmasi Booking Perbaikan Anda</small></p>
                
                ";
                $subject="Booking Layanan Antar Jemput BROSERVICE";
                $this->kirimemail($perbaikan_id,$message,$subject);
            }
            

            redirect('perbaikan/histori-perbaikan', 'refresh');

        }else{

            redirect('auth/login', 'refresh');
        }
       
    }

    public function storekirimonline(){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin() && isset($this->session->kerusakan_id)){
            
            
            $user = $this->ion_auth->user()->row();

            $like=array(

                'kode'=>'KO'
            );
            $kode=$this->perbaikan_model->getmaxkodeperbaikan($like)->row()->kode;
            if($kode!=null){
                
                
                $kode = (int) substr($kode, 2, 6);
                $kode++;
                $char = "KO";

                $kode = $char . sprintf("%06s", $kode);
                
                
            }else{

                $kode='KO000001';
            }

            //kondisi untuk kupon

            $kupon=null;

            if(isset($this->session->kupon)){

                $where=array(

                    "nama"=>$this->session->kupon
                );

                $datakupon=$this->perbaikan_model->getkupon($where)->result_array();

                foreach($datakupon as $value){

                    $kupon=$value['id'];
                    
                }

            }
            
            $data=array(

                'users_id'=>$user->id,
                'kode'=>$kode,
                'supersubkategori_id'=>$this->session->supersubkategori_id,
                'status_pembayaran'=>'0',
                'tanggal_booking'=>date("Y-m-d"),
                'status_perbaikan'=>'0',
                'jenis_layanan'=>'1',
                'kupon'=>$kupon

            );

            $perbaikan_id=$this->perbaikan_model->storeperbaikan($data);

            $data=array(

                'perbaikan_id'=>$perbaikan_id,
                'status'=>'1'


            );
            $this->perbaikan_model->storekirimonline($data);

            $where=array(
                'kerusakan.id'=>$this->session->kerusakan_id
            );

            
            $kerusakan=$this->perbaikan_model->getkerusakan($where)->row();

            $data=array(

                'perbaikan_id'=>$perbaikan_id,
                'kerusakan_id'=>$kerusakan->id,
                'diskon'=>$kerusakan->diskon


            );
            
            $this->perbaikan_model->storedaftarkerusakan($data);
            
            $where=array(

                'perbaikan.id'=>$perbaikan_id,

            );
            

            $data['kode']=$this->perbaikan_model->get($where)->row()->kode;
            $data['finished']=true;
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/perbaikan/inputresi',$data);
            $this->load->view('includes/frontendfooter');

            $message="

                <h3>Booking Perbaikan Telah Di Terima</h3>
                <p><small>Admin Akan Mengkomfirmasi Booking Perbaikan Anda</small></p>
                
                ";
            $subject="Booking Layanan Kirim Online BROSERVICE";
            $this->kirimemail($perbaikan_id,$message,$subject);
            

        }else{

            redirect('auth/login', 'refresh');
        }

    }
    
    public function createresi($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        


            $data['kode']=$kode;
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/perbaikan/inputresi',$data);
            $this->load->view('includes/frontendfooter');

           

        }else{

            redirect('auth/login', 'refresh');
        }

    }

    public function updateresi($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        
           
            $where = array(

                'kode' => $kode
                
            );
            
            
            $perbaikan_id=$this->perbaikan_model->get($where)->row()->id;
    
            $where=array(
    
                'perbaikan_id'=>$perbaikan_id
    
            );


            
            $update=array(
    
                'kurir'=>$this->input->post('kurir'),
                'resi'=>$this->input->post('resi')            
            );
            


            if($this->perbaikan_model->updatekirimonline($where,$update)){

                redirect('perbaikan/histori-perbaikan', 'refresh');

            };
    
            
            
        }else{

            redirect('auth/login', 'refresh');
        }

    }

    public function addkupon(){

        redirect($this->session->link.$this->input->post("kupon"), 'refresh');

    }

    public function storekupon(){


        $newdata = array(
            'kupon'=>$this->input->post('kupon'),
        
        );
        $this->session->set_userdata($newdata);

               
        redirect($this->session->link, 'refresh');

    }

    public function historiperbaikan(){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $user = $this->ion_auth->user()->row();

            //get data pengaturan durasi akhir garansi
            $durasi=$this->pengaturan_model->get(array('id'=>1))->row()->nilai;

            $where=array(

                'users_id'=>$user->id

            );

            $perbaikan=$this->perbaikan_model->get($where)->result_array();
            // $data['daftar_kerusakan']=array();

            $temp_data=array();
            
            foreach($perbaikan as $value){

                

                $where=array(

                    'perbaikan_id'=>$value['id']
                );

                $daftarkerusakan=$this->perbaikan_model->getdaftarkerusakan($where)->result_array();
                $kirimonline=$this->perbaikan_model->getkirimonline($where)->result_array();
                $antarjemput=$this->perbaikan_model->getantarjemput($where)->result_array();
                
                
                //kupon

                $kupon=array();
                if($value['kupon']!=null){

                    $where=array(

                        'id'=>$value['kupon']
                    );

                    $kupon=$this->perbaikan_model->getkupon($where)->row();

                }
                
                $array=array(

                    'id'=>$value['id'],
                    'kode'=>$value['kode'],
                    'nama_model'=>$value['nama_model'],
                    'nama_brand'=>$value['nama_brand'],
                    'nama_jenis'=>$value['nama_jenis'],
                    'tanggal_booking'=>$value['tanggal_booking'],
                    'total_harga'=>$value['total_harga'],
                    'users_id'=>$value['users_id'], 
                    'status_transaksi'=>$value['status_transaksi'],
                    'nama_status_booking'=>$value['nama_status_booking'],
                    'nama_status_pembayaran'=>$value['nama_status_pembayaran'],
                    'nama_status_perbaikan'=>$value['nama_status_perbaikan'],
                    'jenis_layanan'=>$value['jenis_layanan'],
                    'status_perbaikan'=>$value['status_perbaikan'],
                    'tanggal_selesai_perbaikan'=>$value['tanggal_selesai_perbaikan'],
                    'tanggal_akhir_garansi'=>date("Y-m-d", strtotime('+'.$durasi.' days', strtotime($value['tanggal_selesai_perbaikan']))),
                    'status_pembayaran'=>$value['status_pembayaran'],
                    'status_booking'=>$value['status_booking'],
                    'daftar_kerusakan'=>$daftarkerusakan,
                    'kirim_online'=>$kirimonline,
                    'antar_jemput'=>$antarjemput,
                    'kupon'=>$kupon

                );

                array_push($temp_data,$array);
                
            }
            

            // $data['antarjemput']=$this->perbaikan_model->getantarjemput($where)->result_array();
            // $data['kirimonline']=$this->perbaikan_model->getkirimonline($where)->result_array();

            
            $data['data']=$temp_data;


            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/perbaikan/historiperbaikan',$data);
            $this->load->view('includes/frontendfooter');
            



        }else{

            redirect('auth/login', 'refresh');

        }



    }

    public function createkerusakan($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        

            
            $where = array(

                'kode' => $kode
                
            );
            
            
            $supersubkategori_id=$this->perbaikan_model->get($where)->row()->supersubkategori_id;

            $data['kode']=$kode;
            $data['data']=$this->perbaikan_model->getkerusakan()->result_array();

            
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/perbaikan/createkerusakan',$data);
            $this->load->view('includes/frontendfooter');

           

        }else{

            redirect('auth/login', 'refresh');
        }
    }

    public function storekerusakan($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        

            
            $where = array(

                'kode' => $kode
                
            );
            
            
            $perbaikan_id=$this->perbaikan_model->get($where)->row()->id;

            $where=array(
                'kerusakan.id'=>$this->input->post('kerusakan')
            );

            
            $kerusakan=$this->perbaikan_model->getkerusakan($where)->row();

            $data=array(

                'perbaikan_id'=>$perbaikan_id,
                'kerusakan_id'=>$kerusakan->id,
                'diskon'=>$kerusakan->diskon


            );
            
            $this->perbaikan_model->storedaftarkerusakan($data);

            redirect('perbaikan/histori-perbaikan', 'refresh');

        }else{

            redirect('auth/login', 'refresh');
        }
    }

    public function editantarjemput($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $data['kode']=$kode;
            
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/perbaikan/editantarjemput',$data);
            $this->load->view('includes/frontendfooter');

        }else{

            redirect('auth/login', 'refresh');
        }
    }

    public function updateantarjemput($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $data['kode']=$kode;
            
            $where = array(

                'kode' => $kode
                
            );
            
            
            $perbaikan_id=$this->perbaikan_model->get($where)->row()->id;
    
            $where=array(
    
                'perbaikan_id'=>$perbaikan_id
    
            );


            
            $update=array(
    
                'tanggal'=>$this->input->post('tanggal'),
                'jam'=>$this->input->post('jam'),
                'alamat'=>$this->input->post('alamat')            
            );
            


            if($this->perbaikan_model->updateantarjemput($where,$update)){

                redirect('perbaikan/histori-perbaikan', 'refresh');

            };
    

        }else{

            redirect('auth/login', 'refresh');
        }
    }

    public function batalkanperbaikan($kode)
	{
		if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $data['kode']=$kode;
            

            
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/perbaikan/batalkanbooking',$data);
            $this->load->view('includes/frontendfooter');
            
          
            
    

        }else{

            redirect('auth/login', 'refresh');
        }

    }
    public function infopembayaran($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        
            $data['kode']=$kode;
            
            $where = array(

                'kode' => $kode
                
            );
            
            
            $data['data']=$this->perbaikan_model->get($where)->row_array();

            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/perbaikan/infopembayaran',$data);
            $this->load->view('includes/frontendfooter');

           

        }else{

            redirect('auth/login', 'refresh');
        }

    }

    public function createkomfirmasipembayaran($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        
            $data['kode']=$kode;
            

            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/perbaikan/createkomfirmasipembayaran.php',$data);
            $this->load->view('includes/frontendfooter');

           

        }else{

            redirect('auth/login', 'refresh');
        }

    }

    public function storekomfirmasipembayaran($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        
            $data['kode']=$kode;


            $where = array(

                'kode' => $kode
                
            );
            
            $idperbaikan=$this->perbaikan_model->get($where)->row()->id;


            $this->load->library('form_validation');
            $this->form_validation->set_rules('bank', 'Bank', 'required', 'Bank Harus Di Isi');
            $this->form_validation->set_rules('norek', 'No Rekening', 'required', 'No Rekening Harus Di Isi');
            $this->form_validation->set_rules('nama', 'Nama Pemilik Rekening', 'required', 'Nama Pemilik Rekening Harus Di Isi');
            $this->form_validation->set_rules('tanggal', 'Tanggal Transfer', 'required', 'anggal Transfer Harus Di Isi');
            if (empty($_FILES['foto']['name']))
            {
                $this->form_validation->set_rules('foto', 'Foto Bukti Transfer', 'required');
            }

            if ($this->form_validation->run() == FALSE){


                $data['status']=array(

                    'status'=>'error',
                    'message'=>'Kesalahan input',
    
                );

                $this->load->view('includes/frontendheader');
                $this->load->view('includes/frontendnavbar');
                $this->load->view('frontend/perbaikan/createkomfirmasipembayaran.php',$data);
                $this->load->view('includes/frontendfooter');

            }else{


                //array ke model
				$store=array(
                    'perbaikan_id'=>$idperbaikan,
                    'bank'=>$this->input->post('bank'),
                    'norek'=>$this->input->post('norek'),
                    'nama'=>$this->input->post('nama'),
                    'tanggal'=>$this->input->post('tanggal'),
                    'keterangan'=>$this->input->post('keterangan'),
					

				);
				//mendapatkan id yang kelak jadi nama file
				$id=$this->perbaikan_model->storekomfirmasipembayaran($store);
				$file_name=$id;

				$config['upload_path']= 'assets/uploads/komfirmasipembayaran/';
				$config['allowed_types']= 'gif|jpg|png';
				$config['file_name']= $file_name;
				$config['overwrite']= true;
				
				// // $config['max_size']             = 100;
				// $config['max_width']            = 1024;
				// $config['max_height']           = 768;

				$this->load->library('upload', $config);
                //proses upload
                
                if ( ! $this->upload->do_upload('foto'))
				{
					
					$data['status']=array(

						'status'=>'success',
						'message'=>'Data berhasil disimpan , kesalahan pada saat upload foto'
		
					);
					
				}else{

                    $this->load->library('image_lib');
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'assets/uploads/komfirmasipembayaran/'.$this->upload->data('file_name');
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = false;
					$config['width']         = 400;
					$config['height']       = 400;

					$this->image_lib->clear();
					$this->image_lib->initialize($config);
                    $this->image_lib->resize();
                    
                    $data['status']=array(

						'status'=>'success',
						'message'=>'Data berhasil disimpan'
		
					);

					$where=array(

						'id'=>$id
						
					);
					//array ke model update
					$update=array(

						'foto'=>$this->upload->data('file_name')
						
					);

                    $update=$this->perbaikan_model->updatekomfirmasipembayaran($where,$update);

                    $data['kode']=$kode;
                    
                    $this->load->view('includes/frontendheader');
                    $this->load->view('includes/frontendnavbar');
                    $this->load->view('frontend/perbaikan/finishkomfirmasipembayaran.php',$data);
                    $this->load->view('includes/frontendfooter');

                }


            }
            

        }else{

            redirect('auth/login', 'refresh');
        }

    }
    
    public function cetakkwitansi($kode)
	{
		
		
		$where = array(

			'kode' => $kode
			
		);
		
		$perbaikan=$this->perbaikan_model->get($where)->row();

		$data['perbaikan']=$this->perbaikan_model->get($where)->row_array();

		$where = array(

			'id' => $perbaikan->users_id
			
		);
		$data['user']=$this->perbaikan_model->getuser($where)->row_array();
		
		
		$where = array(

			'perbaikan_id' => $perbaikan->id
			
		);
		$data['daftarkerusakan']=$this->perbaikan_model->getdaftarkerusakan($where)->result_array();
		$data['kirimonline']=$this->perbaikan_model->getkirimonline($where)->row_array();
		$data['antarjemput']=$this->perbaikan_model->getantarjemput($where)->row_array();
		$data['komfirmasipembayaran']=$this->perbaikan_model->getkomfirmasipembayaran($where)->result_array();
		
		
		$durasi=$this->pengaturan_model->get(array('id'=>1))->row()->nilai;
		$tanggalselesai= $perbaikan->tanggal_selesai_perbaikan;
		$akhir_garansi = date("Y-m-d", strtotime('+'.$durasi.' days', strtotime($tanggalselesai)));
		$data['akhir_garansi']=$akhir_garansi;
		
		$this->load->library('pdf');
		$title_page = 'Kwitansi';
		$html=$this->load->view('admin/perbaikan/cetakkwitansi',$data,true);
		$this->pdf->pdf_create($html,$title_page,'legal','portrait',"false");
        
		
		
    }
    
    public function detailhadiah($nama){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        
            $data['nama']=$nama;
            
            $where = array(

                'nama' => $nama
                
            );
            
            
            $data['data']=$this->perbaikan_model->getkupon($where)->row_array();

            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/perbaikan/detailhadiah',$data);
            $this->load->view('includes/frontendfooter');

           

        }else{

            redirect('auth/login', 'refresh');
        }

    }


    public function komfirmasibarangditerima($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        


            $data['kode']=$kode;
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/perbaikan/komfirmasibarangditerima',$data);
            $this->load->view('includes/frontendfooter');

           

        }else{

            redirect('auth/login', 'refresh');
        }

    }

    public function updatekomfirmasibarangditerima($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $data['kode']=$kode;
            
            $where = array(

                'kode' => $kode
                
            );
            
            
            $perbaikan_id=$this->perbaikan_model->get($where)->row()->id;
    
            $where=array(
    
                'id'=>$perbaikan_id
    
            );


            
            $update=array(
    
                'status_transaksi'=>4,
                 
            );
            


            if($this->perbaikan_model->updatestatus($where,$update)){

               

            };
            
            $message="

                <h3>Barang Telah Selesai Di Perbaiki dan Telah Sampai Kepada Pemilik</h3>
                <p><small>Terima Kasih Telah Menggunakan Jasa Perbaikan Di BROSERVICE</small></p>
                
                ";
            $subject="Barang Telah Selesai Di Perbaiki dan Telah Sampai Kepada Pemilik";
            $this->kirimemail($perbaikan_id,$message,$subject);
            redirect('perbaikan/testimoni/create/'.$kode, 'refresh');

        }else{

            redirect('auth/login', 'refresh');
        }
    }


    public function createtestimoni($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        


            $data['kode']=$kode;
            $this->load->view('includes/frontendheader');
            $this->load->view('includes/frontendnavbar');
            $this->load->view('frontend/perbaikan/createtestimoni',$data);
            $this->load->view('includes/frontendfooter');

           

        }else{

            redirect('auth/login', 'refresh');
        }

    }


    public function storetestimoni($kode){

        if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
        


            $data['kode']=$kode;
            
            $where = array(

                'kode' => $kode
                
            );
            
            
            $perbaikan_id=$this->perbaikan_model->get($where)->row()->id;

            $data=array(

                'perbaikan_id'=>$perbaikan_id,
                'testimoni'=>$this->input->post('testimoni'),
                'tanggal'=>date('Y-m-d')
            );

            $this->perbaikan_model->storetestimoni($data);




            redirect('perbaikan/histori-perbaikan', 'refresh');

           

        }else{

            redirect('auth/login', 'refresh');
        }

    }

    public function destroy($kode)
	{
		if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
            
            $data['kode']=$kode;
            
            $where = array(

                'kode' => $kode
                
            );
            
            
            $perbaikan=$this->perbaikan_model->get($where)->row();

    
            $where = array(

                'id' => $perbaikan->id
                
            );
            if($perbaikan->status_booking==false){

                $this->perbaikan_model->destroy($where);

            }
            

            redirect('perbaikan/histori-perbaikan', 'refresh');
    

        }else{

            redirect('auth/login', 'refresh');
        }

    }
    

    function kirimemail($perbaikan_id,$message,$subject){

        $user = $this->ion_auth->user()->row();
        $data['message']=$message;
        $where=array(

            'perbaikan.id'=>$perbaikan_id

        );

		
		$perbaikan=$this->perbaikan_model->get($where)->row();

		$data['perbaikan']=$this->perbaikan_model->get($where)->row_array();

		$where = array(

			'id' => $perbaikan->users_id
			
		);
		$data['user']=$this->perbaikan_model->getuser($where)->row_array();
		
		
		$where = array(

			'perbaikan_id' => $perbaikan->id
			
		);
		$data['daftarkerusakan']=$this->perbaikan_model->getdaftarkerusakan($where)->result_array();
		$data['kirimonline']=$this->perbaikan_model->getkirimonline($where)->row_array();
		$data['antarjemput']=$this->perbaikan_model->getantarjemput($where)->row_array();
		$data['komfirmasipembayaran']=$this->perbaikan_model->getkomfirmasipembayaran($where)->result_array();
		
		
		$tanggalselesai= $perbaikan->tanggal_selesai_perbaikan;
		$akhir_garansi = date("Y-m-d", strtotime('+30 days', strtotime($tanggalselesai)));
		$data['akhir_garansi']=$akhir_garansi;
        


        $html=$this->load->view('email/email',$data,true);

        $this->load->library('email');    
        $this->email->from('kambingimudth@gmail.com', 'Admin BROSERVICE.ID');   
        $this->email->to($user->email);   
        $this->email->subject($subject);   
        $this->email->message($html);  
        $this->email->send();

        

    }

    
}
