<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */




	public function __construct()
	{
		parent::__construct();

		$this->load->model('kategori_model');
		$this->load->model('slider_model');
		$this->load->model('testimoni_model');
		$this->load->model('perbaikan_model');

		$newdata = array(
			'menu'  => 'Home'
		);
		
		$this->session->set_userdata($newdata);
	}


	public function index()
	{
		$where=array(

			'tampilkan'=>true

		);


		$data['kategori']=$this->kategori_model->get($where)->result_array();

		

		$data['slider']=$this->slider_model->get()->result_array();
		$data['testimoni']=$this->testimoni_model->get()->result_array();
		
		$this->load->view('includes/frontendheader');
		$this->load->view('includes/frontendnavbar');
		$this->load->view('includes/frontendslider',$data);
		
		$this->load->view('frontend/home/index',$data);
		$this->load->view('includes/frontendsupportservice');
		$this->load->view('frontend/home/testimoni',$data);
		$this->load->view('includes/frontendfooter');
		
		
	
	}
}
