-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2018 at 04:21 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `broservice`
--

-- --------------------------------------------------------

--
-- Table structure for table `antar_jemput`
--

CREATE TABLE `antar_jemput` (
  `id` int(12) NOT NULL,
  `perbaikan_id` int(12) NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `alamat` text NOT NULL,
  `status` int(12) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `antar_jemput`
--

INSERT INTO `antar_jemput` (`id`, `perbaikan_id`, `tanggal`, `jam`, `alamat`, `status`) VALUES
(88, 124, '2018-09-14', '11:00:00', 'test', 2),
(89, 125, '2018-09-14', '11:00:00', 'INDOMART', 2),
(90, 127, '2018-09-15', '11:00:00', 'bandung', 2);

-- --------------------------------------------------------

--
-- Table structure for table `daftar_kerusakan`
--

CREATE TABLE `daftar_kerusakan` (
  `id` int(12) NOT NULL,
  `perbaikan_id` int(12) NOT NULL,
  `kerusakan_id` int(12) NOT NULL,
  `diskon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daftar_kerusakan`
--

INSERT INTO `daftar_kerusakan` (`id`, `perbaikan_id`, `kerusakan_id`, `diskon`) VALUES
(89, 124, 3, 20),
(90, 125, 3, 20),
(91, 126, 3, 20),
(92, 126, 4, 3),
(93, 127, 4, 3),
(94, 128, 3, 20),
(95, 128, 4, 3),
(96, 129, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `gambar` varchar(255) NOT NULL DEFAULT 'none.png',
  `tampilkan` tinyint(4) NOT NULL DEFAULT '0',
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `gambar`, `tampilkan`, `link`) VALUES
(32, 'Smartphone', '32.jpg', 1, 'smartphone'),
(33, 'Laptop', '33.jpg', 1, 'laptop');

-- --------------------------------------------------------

--
-- Table structure for table `kerusakan`
--

CREATE TABLE `kerusakan` (
  `id` int(12) NOT NULL,
  `supersubkategori_id` int(12) NOT NULL,
  `nama` varchar(266) NOT NULL,
  `harga` int(12) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(255) NOT NULL DEFAULT 'none.png',
  `link` varchar(255) NOT NULL,
  `diskon` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kerusakan`
--

INSERT INTO `kerusakan` (`id`, `supersubkategori_id`, `nama`, `harga`, `deskripsi`, `gambar`, `link`, `diskon`) VALUES
(3, 14, 'Rusak LCD', 460000, 'test', '3.jpg', 'rusak-lcd', 20),
(4, 14, 'LCD ORI', 650000, 'LCD ORI', '4.png', 'lcd-ori', 3),
(5, 14, 'Rusak Baterai', 100000, 'test', 'none.png', 'rusak-baterai', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kg_antar_jemput`
--

CREATE TABLE `kg_antar_jemput` (
  `id` int(12) NOT NULL,
  `klaim_garansi_id` int(12) NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `alamat` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kg_antar_jemput`
--

INSERT INTO `kg_antar_jemput` (`id`, `klaim_garansi_id`, `tanggal`, `jam`, `alamat`, `status`) VALUES
(5, 24, '2018-09-08', '11:00:00', '1', 0),
(6, 25, '2018-09-15', '11:00:00', '123123', 2),
(7, 26, '2018-09-15', '11:00:00', 'KNN', 1),
(8, 27, '2018-09-15', '11:00:00', 'bdg', 1),
(9, 28, '2018-09-15', '11:00:00', 'bdg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `kg_kirim_online`
--

CREATE TABLE `kg_kirim_online` (
  `id` int(12) NOT NULL,
  `klaim_garansi_id` int(12) NOT NULL,
  `kurir` varchar(20) NOT NULL,
  `resi` varchar(50) NOT NULL,
  `resikembali` varchar(255) NOT NULL,
  `kurirkembali` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kg_kirim_online`
--

INSERT INTO `kg_kirim_online` (`id`, `klaim_garansi_id`, `kurir`, `resi`, `resikembali`, `kurirkembali`, `status`) VALUES
(9, 16, 'JNE', '123123', '', '', 1),
(10, 17, 'JNE', '123123123', '', '', 1),
(11, 18, 'JNE', '123123', '', '', 1),
(12, 19, 'JNE', '13123', '', '', 1),
(13, 20, 'JNE', '123123', '', '', 1),
(14, 21, 'JNE', '123123', '', '', 1),
(15, 22, '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kirim_online`
--

CREATE TABLE `kirim_online` (
  `id` int(12) NOT NULL,
  `perbaikan_id` int(12) NOT NULL,
  `kurir` varchar(20) NOT NULL,
  `resi` varchar(50) NOT NULL,
  `resikembali` varchar(255) NOT NULL,
  `kurirkembali` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kirim_online`
--

INSERT INTO `kirim_online` (`id`, `perbaikan_id`, `kurir`, `resi`, `resikembali`, `kurirkembali`, `status`) VALUES
(4, 126, 'JNE', '57856868576', '', 'JNE', 2),
(5, 128, '', '', '', '', 1),
(6, 129, 'JNE', '232323', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `klaim_garansi`
--

CREATE TABLE `klaim_garansi` (
  `id` int(12) NOT NULL,
  `users_id` int(11) NOT NULL,
  `perbaikan_id` int(12) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `jenis_layanan` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal_klaim_garansi` date NOT NULL,
  `status_perbaikan` int(11) NOT NULL,
  `tanggal_selesai_perbaikan` date DEFAULT NULL,
  `status_klaim_garansi` tinyint(4) NOT NULL DEFAULT '0',
  `status_transaksi` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `klaim_garansi`
--

INSERT INTO `klaim_garansi` (`id`, `users_id`, `perbaikan_id`, `kode`, `jenis_layanan`, `tanggal_klaim_garansi`, `status_perbaikan`, `tanggal_selesai_perbaikan`, `status_klaim_garansi`, `status_transaksi`) VALUES
(25, 5, 64, 'KG000001', 0, '2018-09-08', 0, NULL, 0, 0),
(26, 17, 125, 'KG000002', 0, '2018-09-14', 1, '2018-09-14', 1, 0),
(28, 16, 127, 'KG000003', 0, '2018-09-15', 1, '2018-09-15', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `komfirmasi_pembayaran`
--

CREATE TABLE `komfirmasi_pembayaran` (
  `id` int(12) NOT NULL,
  `perbaikan_id` int(12) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `norek` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tanggal` date NOT NULL,
  `foto` varchar(255) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kupon`
--

CREATE TABLE `kupon` (
  `id` int(12) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `min_transaksi` int(12) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(255) NOT NULL DEFAULT 'none.png'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kupon`
--

INSERT INTO `kupon` (`id`, `nama`, `min_transaksi`, `deskripsi`, `gambar`) VALUES
(2, 'KAOSBROSERVICE', 100000, 'test', '2.jpg'),
(3, 'mug', 1000000, 'mug', '3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(12) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nilai` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `nama`, `nilai`) VALUES
(1, 'Durasi Garansi', '10');

-- --------------------------------------------------------

--
-- Table structure for table `perbaikan`
--

CREATE TABLE `perbaikan` (
  `id` int(12) NOT NULL,
  `users_id` int(11) UNSIGNED NOT NULL,
  `supersubkategori_id` int(11) NOT NULL,
  `tanggal_booking` date NOT NULL,
  `kode` varchar(20) NOT NULL,
  `jenis_layanan` tinyint(4) NOT NULL,
  `status_perbaikan` tinyint(4) NOT NULL,
  `tanggal_selesai_perbaikan` date DEFAULT NULL,
  `status_pembayaran` tinyint(4) NOT NULL DEFAULT '0',
  `status_booking` tinyint(4) NOT NULL,
  `status_transaksi` int(4) NOT NULL DEFAULT '1',
  `kupon` int(12) DEFAULT NULL,
  `update_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perbaikan`
--

INSERT INTO `perbaikan` (`id`, `users_id`, `supersubkategori_id`, `tanggal_booking`, `kode`, `jenis_layanan`, `status_perbaikan`, `tanggal_selesai_perbaikan`, `status_pembayaran`, `status_booking`, `status_transaksi`, `kupon`, `update_at`) VALUES
(124, 16, 14, '2018-09-14', 'AJ000001', 0, 1, '2018-09-15', 1, 1, 8, NULL, NULL),
(125, 17, 14, '2018-09-14', 'AJ000002', 0, 1, '2018-09-14', 1, 1, 4, 2, NULL),
(126, 17, 14, '2018-09-14', 'KO000001', 1, 1, '2018-09-15', 1, 1, 4, 2, NULL),
(127, 16, 14, '2018-09-15', 'AJ000003', 0, 1, '2018-09-15', 1, 1, 8, NULL, NULL),
(128, 17, 14, '2018-09-15', 'KO000002', 1, 1, '2018-09-16', 0, 0, 1, 2, NULL),
(129, 17, 14, '2018-09-16', 'KO000003', 1, 0, NULL, 0, 1, 8, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `poin`
--

CREATE TABLE `poin` (
  `id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `point` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(12) NOT NULL,
  `gambar` varchar(255) NOT NULL DEFAULT 'none.png'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_lokasi_barang`
--

CREATE TABLE `status_lokasi_barang` (
  `id` int(12) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_lokasi_barang`
--

INSERT INTO `status_lokasi_barang` (`id`, `nama`) VALUES
(1, 'Menunggu Barang'),
(2, 'Kantor BROSERVICE'),
(3, 'Di Kirim Kembali Ke Costumer');

-- --------------------------------------------------------

--
-- Table structure for table `status_transaksi`
--

CREATE TABLE `status_transaksi` (
  `id` int(12) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_transaksi`
--

INSERT INTO `status_transaksi` (`id`, `nama`) VALUES
(1, 'Menunggu Komfimasi Admin'),
(2, 'Menunggu Barang Datang'),
(3, 'perbaikan'),
(4, 'Selesai'),
(5, 'Klaim Garansi'),
(6, 'Perbaikan Garansi'),
(7, 'Garansi Selesai'),
(8, 'Dibatalkan Karena Barang Tidak Bisa Di Perbaiki');

-- --------------------------------------------------------

--
-- Table structure for table `subkategori`
--

CREATE TABLE `subkategori` (
  `id` int(12) NOT NULL,
  `kategori_id` int(12) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL DEFAULT 'none.png',
  `tampilkan` tinyint(4) NOT NULL DEFAULT '0',
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subkategori`
--

INSERT INTO `subkategori` (`id`, `kategori_id`, `nama`, `gambar`, `tampilkan`, `link`) VALUES
(16, 32, 'Lenovo', '16.jpg', 1, 'lenovo');

-- --------------------------------------------------------

--
-- Table structure for table `supersubkategori`
--

CREATE TABLE `supersubkategori` (
  `id` int(12) NOT NULL,
  `subkategori_id` int(12) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL DEFAULT 'none.png',
  `tampilkan` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supersubkategori`
--

INSERT INTO `supersubkategori` (`id`, `subkategori_id`, `nama`, `gambar`, `tampilkan`, `link`) VALUES
(14, 16, 'lenovo a6000', '14.jpg', '1', 'lenovo-a6000');

-- --------------------------------------------------------

--
-- Table structure for table `testimoni`
--

CREATE TABLE `testimoni` (
  `id` int(11) NOT NULL,
  `perbaikan_id` int(11) NOT NULL,
  `testimoni` text NOT NULL,
  `tampilkan` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimoni`
--

INSERT INTO `testimoni` (`id`, `perbaikan_id`, `testimoni`, `tampilkan`, `tanggal`) VALUES
(1, 64, 'Terima Kasih Bos', 1, '2018-09-08'),
(2, 65, 'nanti saya order lagi mas , pelayanan memuaskan, terima kasih', 0, '2018-09-08'),
(3, 125, 'TEST 1', 1, '2018-09-14'),
(4, 126, '1111MNNJKKHKHKHY', 0, '2018-09-14'),
(5, 124, 'terima kasih broservice', 0, '2018-09-15'),
(6, 127, 'Terima Kasih Lagi', 1, '2018-09-15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `address`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1537122579, 1, 'Admin', 'istrator', 'ADMIN', '0', '0'),
(5, '::1', 'nepnep@nep.com', '$2y$08$Qz0OnpOsm0tmk7ez7v9i8OUOf6X7MlIjXguJ16TJT1LkPhs9SxIcG', NULL, 'nepnep@nep.com', NULL, NULL, NULL, NULL, 1536088167, 1536898683, 1, 'Nep Nepz', 'hanep', NULL, '09896897869', 'bandung'),
(16, '::1', 'kambingimudth@gmail.com', '$2y$08$wRSaeLDvMzQUS1sYCvBxduJYGfObURMzRyMIGifTjpQ7mJzEorIxW', NULL, 'kambingimudth@gmail.com', NULL, '5dxolfZkuEsrEA2bj2dUZu52b102ec866d908a6c', 1536984794, NULL, 1536410937, 1537122546, 1, 'kanep nep', 'Nepz', NULL, '123123', 'hanep'),
(17, '180.245.192.13', 'widy.secret@gmail.com', '$2y$08$8fYXS7fni6SwTleFO/Yh0uodGnoH4dTTlvL.31vV42ldNb5BSqwhm', NULL, 'widy.secret@gmail.com', NULL, 'D34jl6OokqJC7muBsjiTOe9a1795b6a931e6273c', 1536990195, NULL, 1536909793, 1537114531, 1, 'widy', 'nurahman', NULL, '087724456289', 'KNG');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(3, 1, 1),
(7, 5, 2),
(18, 16, 2),
(19, 17, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `antar_jemput`
--
ALTER TABLE `antar_jemput`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perbaikan_to_antar_jemput` (`perbaikan_id`);

--
-- Indexes for table `daftar_kerusakan`
--
ALTER TABLE `daftar_kerusakan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Perbaikan_to_daftar_kerusakan` (`perbaikan_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kerusakan`
--
ALTER TABLE `kerusakan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supersubkategori` (`supersubkategori_id`);

--
-- Indexes for table `kg_antar_jemput`
--
ALTER TABLE `kg_antar_jemput`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perbaikan_to_antar_jemput` (`klaim_garansi_id`);

--
-- Indexes for table `kg_kirim_online`
--
ALTER TABLE `kg_kirim_online`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perbaikan_to_kirim_online` (`klaim_garansi_id`);

--
-- Indexes for table `kirim_online`
--
ALTER TABLE `kirim_online`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perbaikan_to_kirim_online` (`perbaikan_id`);

--
-- Indexes for table `klaim_garansi`
--
ALTER TABLE `klaim_garansi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komfirmasi_pembayaran`
--
ALTER TABLE `komfirmasi_pembayaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perbaikan_to_komfirmasi_pembayaran` (`perbaikan_id`);

--
-- Indexes for table `kupon`
--
ALTER TABLE `kupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perbaikan`
--
ALTER TABLE `perbaikan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users` (`users_id`),
  ADD KEY `status_transaksi` (`status_transaksi`),
  ADD KEY `kupon` (`kupon`);

--
-- Indexes for table `poin`
--
ALTER TABLE `poin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_lokasi_barang`
--
ALTER TABLE `status_lokasi_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_transaksi`
--
ALTER TABLE `status_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subkategori`
--
ALTER TABLE `subkategori`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategori` (`kategori_id`);

--
-- Indexes for table `supersubkategori`
--
ALTER TABLE `supersubkategori`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subkategori` (`subkategori_id`);

--
-- Indexes for table `testimoni`
--
ALTER TABLE `testimoni`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `antar_jemput`
--
ALTER TABLE `antar_jemput`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `daftar_kerusakan`
--
ALTER TABLE `daftar_kerusakan`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `kerusakan`
--
ALTER TABLE `kerusakan`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kg_antar_jemput`
--
ALTER TABLE `kg_antar_jemput`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `kg_kirim_online`
--
ALTER TABLE `kg_kirim_online`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `kirim_online`
--
ALTER TABLE `kirim_online`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `klaim_garansi`
--
ALTER TABLE `klaim_garansi`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `komfirmasi_pembayaran`
--
ALTER TABLE `komfirmasi_pembayaran`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kupon`
--
ALTER TABLE `kupon`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `perbaikan`
--
ALTER TABLE `perbaikan`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `poin`
--
ALTER TABLE `poin`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subkategori`
--
ALTER TABLE `subkategori`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `supersubkategori`
--
ALTER TABLE `supersubkategori`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `testimoni`
--
ALTER TABLE `testimoni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `antar_jemput`
--
ALTER TABLE `antar_jemput`
  ADD CONSTRAINT `perbaikan_to_antar_jemput` FOREIGN KEY (`perbaikan_id`) REFERENCES `perbaikan` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `daftar_kerusakan`
--
ALTER TABLE `daftar_kerusakan`
  ADD CONSTRAINT `Perbaikan_to_daftar_kerusakan` FOREIGN KEY (`perbaikan_id`) REFERENCES `perbaikan` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kerusakan`
--
ALTER TABLE `kerusakan`
  ADD CONSTRAINT `supersubkategori` FOREIGN KEY (`supersubkategori_id`) REFERENCES `supersubkategori` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kirim_online`
--
ALTER TABLE `kirim_online`
  ADD CONSTRAINT `perbaikan_to_kirim_online` FOREIGN KEY (`perbaikan_id`) REFERENCES `perbaikan` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `komfirmasi_pembayaran`
--
ALTER TABLE `komfirmasi_pembayaran`
  ADD CONSTRAINT `perbaikan_to_komfirmasi_pembayaran` FOREIGN KEY (`perbaikan_id`) REFERENCES `perbaikan` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `perbaikan`
--
ALTER TABLE `perbaikan`
  ADD CONSTRAINT `kupon` FOREIGN KEY (`kupon`) REFERENCES `kupon` (`id`),
  ADD CONSTRAINT `status_transaksi` FOREIGN KEY (`status_transaksi`) REFERENCES `status_transaksi` (`id`),
  ADD CONSTRAINT `users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `subkategori`
--
ALTER TABLE `subkategori`
  ADD CONSTRAINT `kategori` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `supersubkategori`
--
ALTER TABLE `supersubkategori`
  ADD CONSTRAINT `subkategori` FOREIGN KEY (`subkategori_id`) REFERENCES `subkategori` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
